---
title: Privacy Policy
---

# Privacy Policy
At TalentBrick, user privacy is our priority. This Privacy Policy document contains types of information that is collected by TalentBrick websites and how we use it. 
This privacy documentation applies to all websites except our forum, Forum privacy policy can be found [here](https://ask.talentbrick.com/p/privacy).

## What information do we collect?
We collect information from you when you submit your email address for a newsletter, you may be asked to enter your name and email address. Though it is voluntary and you can still keep visiting or using our website without subscribing to our newsletter.

## What do we use your information for?
We use your information to send periodic emails — The email address you provide will be used to send you information and notifications that you request.

## How do we protect your information?
We implement a variety of security measures to maintain the safety of your personal information when you enter, submit, or access your personal information.

## Log Files
We use Cloudflare pages as our website host, so Cloudflare may use log files. Log files contain data recorded by a web server. It contains information like visitors' IP addresses, your user agent, and URL accessed.

## Do we use cookies?
Like any other website, TalentBrick uses 'cookies'. These cookies are used to store information including visitors' preferences (eg. dark mode/light mode toggle). The information is used to optimize the users' experience by customizing our web page content based on visitors' browser type and/or other information.

## Do we disclose any information to outside parties?
We do not sell, trade, or otherwise transfer to outside parties your personally identifiable information. We may also release your information when we believe release is appropriate to comply with the law, enforce our site policies, or protect our or others' rights, property, or safety.

## Google ADS
- Third party vendors, including Google, use cookies to serve ads based on a user's prior visits to your website or other websites.
- Google's use of advertising cookies enables it and its partners to serve ads to your users based on their visit to your sites and/or other sites on the Internet.
- Users may opt out of personalized advertising by visiting [Ads Settings](https://www.google.com/settings/ads).

## Third-party links
Occasionally, at our discretion, we may include or offer third-party products or services on our site. These third-party sites have separate and independent privacy policies. We therefore have no responsibility or liability for the content and activities of these linked sites. Nonetheless, we seek to protect the integrity of our site and welcome any feedback about these sites.

## Affiliate Disclosure
To keep this open-source project alive we use amazon affiliate links in our articles, we link books suggested by the author to amazon affiliate links.

TalentBrick is a participant in the Amazon Services LLC Associates Program, an affiliate advertising program designed to provide a means for website owners to earn fees by promoting and linking to amazon.com and any other website that may be affiliated with Amazon Service LLC Associates Program.

## Online Privacy Policy
This online privacy policy applies only to information collected through our site and not to information collected offline. 

## Your Consent
By using our site, you consent to our website's privacy policy.

## Changes to our Privacy Policy
If we decide to change our privacy policy, we will post those changes on this page.

## Contact
In the event of a critical issue or urgent matter affecting this site, please contact us at contact@talentbrick.com.