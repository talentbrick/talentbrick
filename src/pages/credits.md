---
title: Contributors
description: TalentBrick has been the work of a lot of people. A big thanks to all the people who have contributed to this platform. We wouldn't have come this far without your support.
---
# Thanks to our Contributors
[TalentBrick](/) has been the work of a lot of people. We take pleasure in expressing our gratitude to all the people who have contributed or will contribute in the future on this platform. We wouldn't have come this far without your support.

### Forum Members
[Click here](https://ask.talentbrick.com/users?q=group%3A10) to view.

### Gitlab Contributors
[Click here](https://gitlab.com/talentbrick/talentbrick/-/graphs/main) to see the graph.