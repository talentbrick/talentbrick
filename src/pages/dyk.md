---
title: Did You Know Facts | NCERT Bites
description: Revise NCERT concepts in a minute with Did You Know facts, It's very useful for competitive exam preparation like NEET JEE.
hide_table_of_contents: true
---


<head>
  <meta property="og:image" content="https://icdn.talentbrick.com/main/dyk.png" />
  <link rel="stylesheet" href="https://www.talentbrick.com/homepage.css" />
</head>
<div id="start" className="container">
  <h1 id="h1">Did You Know Facts | NCERT Bites</h1>
  <p>They say NCERT is THE book for NEET preparation. But how do you remember the great number of lines with their respective contexts for your exam? Well, we have you covered with NCERT Bites!</p>

 <p>Here we provide you with lines from your very own books which you might have missed, along with the pagewise references so you can go read them up if that's what you want.</p>
  <h2 id="h1">Select Class to Get Started</h2>
  <section id="category">
    <ul>
      <li>
        <a href="/class-11/dyk">
          <i className="category-img category-img3" />
          <h6>Class 11</h6>
        </a>
      </li>
      <li>
        <a href="/class-12/dyk">
          <i className="category-img category-img4" />
          <h6>Class 12</h6>
        </a>
      </li>
    </ul>
  </section>
</div>
