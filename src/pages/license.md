---
title: License
---
<head>
<meta name="robots" content="noindex"/>
</head>

# License
## About
[TalentBrick](/) is licensed under [CC BY-NC-ND 4.0](https://gitlab.com/talentbrick/talentbrick/-/blob/main/LICENSE).

## You are free to:
**Share** — copy and redistribute the material in any medium or format

## Under the following terms:

- Attribution — You must give appropriate credit to TalentBrick that this content is owned by us, provide a link to the license (this page), and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.

- NonCommercial — You are not allowed to use the material for commercial purposes.

- NoDerivatives — If you remix, transform or build upon the material, you are not allowed to distribute the modified material.

:::tip
If you are sharing a link to the official website or post from the official social page then you don't need to attribute. These terms applies when someone is sharing PDFs or in any other way that isn't managed by TalentBrick.
:::

Source: [Creative Commons](https://creativecommons.org/licenses/by-nc-nd/4.0/)