import React from "react";
import clsx from "clsx";
import Layout from "@theme/Layout";
import Link from "@docusaurus/Link";
import useDocusaurusContext from "@docusaurus/useDocusaurusContext";
import styles from "./index.module.css";
import HomepageFeatures from "../components/HomepageFeatures";
import Head from "@docusaurus/Head";

function HomepageHeader() {
  const { siteConfig } = useDocusaurusContext();
  return (
    <header className={clsx("hero hero--primary", styles.heroBanner)}>
      <div className="container">
        <div className="container2">
          <p className="hero__subtitle">{siteConfig.tagline}</p>
        </div>
        <div>
          <Link
            className="button button--secondary button--lg hmcl2"
            to="#start"
          >
            Start Learning Now ⏱
          </Link>
          <Link
            className="button button--secondary button--lg hmcl2 hmcl3"
            to="/blog"
          >
            Read Blog
          </Link>
        </div>
      </div>
    </header>
  );
}

export default function Home() {
  const { siteConfig } = useDocusaurusContext();
  return (
    <Layout
      title={`Home`}
      description="Say hello to the Open-Source education model. Learning made easy learn without any disturbance, Clear concepts at a glance, and Get access to quality study materials only on TalentBrick."
    >
      <Head>
        <link rel="stylesheet" href="homepage.css" />
        <meta
          name="og:image"
          content="https://icdn.talentbrick.com/Static/TalentBrick-img.png"
        />
      </Head>
      <HomepageHeader />
      <main>
        <HomepageFeatures />
      </main>
      <div id="start" className="container">
        <h1 id="h1">Browse by Class</h1>
        <section id="category">
          <ul>
            <li>
              <Link href="/class-11">
                <i className="category-img category-img3" />
                <h6>Class 11</h6>
              </Link>
            </li>
            <li>
              <Link href="/class-12">
                <i className="category-img category-img4" />
                <h6>Class 12</h6>
              </Link>
            </li>
          </ul>
        </section>
        <h1 id="h1">Exam Preparation</h1>
        <section id="category">
          <ul>
            <li>
              <Link href="/blog/tags/inbo">
                <i className="category-img category-img5" />
                <h6>INBO</h6>
              </Link>
            </li>
            <li>
              <Link href="/blog/tags/kvpy">
                <i className="category-img category-img2" />
                <h6>KVPY</h6>
              </Link>
            </li>
            <li>
              <Link href="/blog/tags/neet">
                <i className="category-img category-img6" />
                <h6>NEET</h6>
              </Link>
            </li>
            <li>
              <Link href="/blog/tags/jee">
                <i className="category-img category-img7" />
                <h6>JEE</h6>
              </Link>
            </li>
          </ul>
        </section>
        <h1 id="h1">Resources</h1>
        <section id="category">
          <ul>
            <li>
              <Link href="/blog">
                <i className="category-img" />
                <h6>Awesome Articles</h6>
              </Link>
            </li>
            <li>
              <Link href="/mcq">
                <i className="category-img category-img1" />
                <h6>MCQ Questions</h6>
              </Link>
            </li>
            <li>
              <Link href="/dyk">
                <i className="category-img category-img8" />
                <h6>Did You Know Facts</h6>
              </Link>
            </li>
          </ul>
        </section>
      </div>
    </Layout>
  );
}
