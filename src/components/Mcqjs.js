import { useLayoutEffect } from "react";
import { useLocation } from "@docusaurus/router";

function LocationMcqTracker() {
  const { pathname } = useLocation();
  useLayoutEffect(() => {
    var labels = document.querySelectorAll(".tb-mcq .opt45");
    for (let a = 0; a < labels.length; a++)
      labels[a].addEventListener("click", addclass);
    function addclass(a) {
      a.target.parentNode.classList.add("show-crctbox");
      a.target.classList.add("tb-wrng");
    }

    document.querySelector(".show-ans-7").addEventListener("click", () => {
      document.querySelector(".show-ans-7").style.display = "none";
      document.querySelector(".hide-ans-5").style.display = "block";
      var xm, im;
      xm = document.querySelectorAll(".tb-mcq");
      for (im = 0; im < xm.length; im++) {
        xm[im].classList.add("showall-true");
      }
    });
    document.querySelector(".hide-ans-5").addEventListener("click", () => {
      document.querySelector(".hide-ans-5").style.display = "none";
      document.querySelector(".show-ans-7").style.display = "block";
      var xm, im;
      xm = document.querySelectorAll(".tb-mcq");
      for (im = 0; im < xm.length; im++) {
        xm[im].classList.remove("showall-true");
      }
      var xms, ims;
      xms = document.querySelectorAll(".tb-mcq .opt45");
      for (ims = 0; ims < xms.length; ims++) {
        xms[ims].classList.remove("tb-wrng");
        xms[ims].classList.remove("tb-crct");
      }
      var xmss, imss;
      xmss = document.querySelectorAll(".tb-mcq");
      for (imss = 0; imss < xmss.length; imss++) {
        xmss[imss].classList.remove("show-crctbox");
      }
    });
  }, [pathname]);
  return null;
}

export default LocationMcqTracker;
