import React, { useState, useEffect } from "react";
import axios from "axios";

export default function DidYouKnow(Props) {
  const [books, setBooks] = useState(false);

  useEffect(() => {
    const fetchData = async () => {
      const response = await axios.get(Props.api);
      setBooks(response.data);
    };
    fetchData();
  }, []);
  return (
    <>
      <div className="row">
        {books &&
          books.map((book, index) => {
            return (
              <div className="col col--4 margin-bottom--lg" key={index}>
                <div className="card">
                  <div className="card__image">
                    <img
                      src={"https://icdn.talentbrick.com" + book.url}
                      alt={book.alt}
                      title={book.alt}
                    />
                  </div>
                  <div className="card__footer button-group button-group--block">
                    <a
                      className="button button--primary"
                      href={"https://icdn.talentbrick.com" + book.url}
                      target="_blank"
                    >
                      View
                    </a>
                    <a
                      className="button button--primary"
                      href={"https://icdn-down.talentbrick.com" + book.url}
                      download
                    >
                      Download
                    </a>
                  </div>
                </div>
              </div>
            );
          })}
      </div>
    </>
  );
}
