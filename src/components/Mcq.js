import React from "react";
import Admonition from "@theme/Admonition";
import LocationMcqTracker from "./Mcqjs";

export default function Mcq({ children }) {
  return (
    <>
      <div className="tb-mcq">{children}</div>
      <hr />
    </>
  );
}
export function Mcqw({ children }) {
  return <p className="opt45">{children}</p>;
}
export function Mcqc({ children }) {
  return <p className="opt45 correctmcq">{children}</p>;
}
export function Mcqans(props) {
  return (
    <div className="crctbox" style={{ display: "none" }}>
      <Admonition type="tip" title={props.title}>
        <p>
          {props.kans(props.ans)}
          {props.children}
        </p>
      </Admonition>
    </div>
  );
}
export function Mcqinfo() {
  return (
    <>
      <link rel="stylesheet" href="/assets/mcq-ques.css" />
      <div className="hide-print-56">
        <Admonition type="info">
          <p>
            Scroll down and select an option in the questions and know the
            answer or click on the Show answers button below to toggle all the
            answers.
          </p>
          <button className="button button--success show-ans-7">
            Show Answers
          </button>
          <button
            style={{ display: "none" }}
            className="button button--warning hide-ans-5"
          >
            Hide Answers
          </button>
        </Admonition>
      </div>
      <LocationMcqTracker />
    </>
  );
}
Mcqans.defaultProps = {
  kans: (ans) => (
    <>
      Correct Answer: <b>{ans}</b>
      <br />
    </>
  ),
  title: "Answer and Explanation",
};
