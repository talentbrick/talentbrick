import React from "react";
import clsx from "clsx";
import styles from "./HomepageFeatures.module.css";

const FeatureList = [
  {
    title: "Content from Toppers",
    Svg: require("../../static/img/undraw_docusaurus_react.svg").default,
    description: (
      <>Directy learn from toppers and learn new concepts at first sight.</>
    ),
  },
  {
    title: "Quality and Specific Content",
    Svg: require("../../static/img/undraw_docusaurus_mountain.svg").default,
    description: (
      <>
        "Don't study hard but study Smart!" We give you specific guidance for
        each topic from different authors.
      </>
    ),
  },
  {
    title: "Power of Open Source",
    Svg: require("../../static/img/open-source.svg").default,
    description: (
      <>
        TalentBrick is an open source project built on other open source
        technologies. We love the community and will always{" "}
        <a href="https://gitlab.com/talentbrick/talentbrick">
          welcome contributions
        </a>
        !
      </>
    ),
  },
];

function Feature({ Svg, title, description }) {
  return (
    <div className={clsx("col col--4")}>
      <div className="text--center">
        <Svg className={styles.featureSvg} alt={title} />
      </div>
      <div className="text--center padding-horiz--md">
        <h3>{title}</h3>
        <p>{description}</p>
      </div>
    </div>
  );
}

export default function HomepageFeatures() {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
