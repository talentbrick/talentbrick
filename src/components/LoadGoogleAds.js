import React from "react";
import Head from "@docusaurus/Head";

const LoadGoogleAds = () => {
  const ads = process.env.NODE_ENV === "production" && (
    <script
      async
      src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"
    />
  );
  return <Head>{ads && ads}</Head>;
};

export default LoadGoogleAds;
