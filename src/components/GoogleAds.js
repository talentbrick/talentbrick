import React, { Component } from "react";
import { Adsense } from "@ctrl/react-adsense";

class GoogleAds extends Component {
  render() {
    return (
      <Adsense
        client="ca-pub-1520766169486436"
        slot={this.props.slot}
        style={{ display: "block" }}
        layout="in-article"
        format="fluid"
      />
    );
  }
}

export default GoogleAds;
