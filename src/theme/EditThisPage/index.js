import React from 'react';
import Translate from '@docusaurus/Translate';
import IconEdit from '@theme/Icon/Edit';
import { ThemeClassNames } from '@docusaurus/theme-common';
import BrowserOnly from '@docusaurus/BrowserOnly';
import Admonition from '@theme/Admonition';
import Link from '@docusaurus/Link';
export default function EditThisPage({ editUrl }) {
  return (
    <div>
      <a
        href={editUrl}
        target="_blank"
        rel="noreferrer noopener"
        title="Caught a mistake or want to improve this page? Edit this on GitLab!"
        className={ThemeClassNames.common.editThisPage}>
        <IconEdit />
        <Translate
          id="theme.common.editThisPage"
          description="The link label to edit the current page">
          Edit this on GitLab!
        </Translate>
      </a><BrowserOnly>
        {() => <a rel="noreferrer noopener" className={ThemeClassNames.common.editThisPage} title="Print this Page" onClick={window.print}>📄 Save as PDF</a>}
      </BrowserOnly>
      <br /><br />
      <Admonition type="caution">
        <p>This page is licensed under <Link rel="noopener noreferrer" to="/license">CC BY-NC-ND 4.0</Link>. You are strictly instructed to follow license rules. <Link rel="noopener noreferrer" to="/license">Click here</Link> to learn more before sharing.</p>
      </Admonition>
    </div>
  );
}
