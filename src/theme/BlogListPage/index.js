import React from "react";
import clsx from "clsx";
import useDocusaurusContext from "@docusaurus/useDocusaurusContext";
import {
  PageMetadata,
  HtmlClassNameProvider,
  ThemeClassNames,
} from "@docusaurus/theme-common";
import BlogLayout from "@theme/BlogLayout";
import BlogListPaginator from "@theme/BlogListPaginator";
import SearchMetadata from "@theme/SearchMetadata";
import BlogPostItems from "@theme/BlogPostItems";
import Head from "@docusaurus/Head";
import Link from "@docusaurus/Link";
function BlogListPageMetadata(props) {
  const { metadata } = props;
  const {
    siteConfig: { title: siteTitle },
  } = useDocusaurusContext();
  const { blogDescription, blogTitle, permalink } = metadata;
  const isBlogOnlyMode = permalink === "/";
  const title = isBlogOnlyMode ? siteTitle : blogTitle;
  return (
    <>
      <PageMetadata title={title} description={blogDescription} />
      <SearchMetadata tag="blog_posts_list" />
      <Head>
        <meta
          name="og:image"
          content="https://icdn.talentbrick.com/main/blog-og-image.png"
        />
      </Head>
    </>
  );
}
function BlogListPageContent(props) {
  const { metadata, items, sidebar } = props;
  return (
    <BlogLayout sidebar={sidebar}>
      <div style={{ textAlign: "center", paddingBottom: "25px" }}>
        <h1>TalentBrick Blog</h1>
        <p>
          Are you confused about preparing for exams like NEET, JEE, KVPY, and
          INBO? We have got you covered, with blogs that have advice from
          students who have excelled in these exams.
        </p>
        <a
          className="button button--lg"
          style={{
            color: "#2196f2",
            border: "3px solid",
            borderRadius: "20px",
            margin: "10px",
            fontSize: "20px",
          }}
          href="https://ask.talentbrick.com/contact-us"
        >
          Write Blog 📝
        </a>
        <Link
          className="button button--lg"
          title="Filter By Tags"
          style={{
            color: "#2196f2",
            border: "3px solid",
            borderRadius: "20px",
            margin: "10px",
            fontSize: "20px",
          }}
          href="/blog/tags"
        >
          Filter By Tags
        </Link>
        <hr />
      </div>
      <BlogPostItems items={items} />
      <BlogListPaginator metadata={metadata} />
    </BlogLayout>
  );
}
export default function BlogListPage(props) {
  return (
    <HtmlClassNameProvider
      className={clsx(
        ThemeClassNames.wrapper.blogPages,
        ThemeClassNames.page.blogListPage
      )}
    >
      <BlogListPageMetadata {...props} />
      <BlogListPageContent {...props} />
    </HtmlClassNameProvider>
  );
}
