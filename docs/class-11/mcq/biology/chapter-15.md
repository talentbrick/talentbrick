---
sidebar_position: 15
title: MCQ Plant Growth and Development Chapter 15 Biology Class 11
description: Plant Growth and Development Chapter 15 Biology Class 11, Boost your performance and score great marks in NEET and Class 11 Exam by practicing these MCQs.
image: https://icdn.talentbrick.com/mcq/class-11-bio-chapter-15.jpg
sidebar_label: Chapter 15
---

import Mcq, {Mcqinfo, Mcqw, Mcqc, Mcqans} from "@site/src/components/Mcq";

import LoadGoogleAds from "@site/src/components/LoadGoogleAds";

import GoogleAds from "@site/src/components/GoogleAds";

NCERT Biology Class 11th Ch.15 Plant Growth and Development MCQ Practice Questions for NEET | Class 11

Prepare these important MCQ Questions of Biology Class 11 Plant Growth and Development Chapter 15, Latest questions to expected to come in NEET | School Exams.

<Mcqinfo/>
<LoadGoogleAds/>

### 1. First step in process of plant growth is:
  <Mcq>
    <Mcqw>1. Zygote formation</Mcqw>
    <Mcqc>2. Seed germination</Mcqc>
    <Mcqw>3. Periclinal division</Mcqw>
    <Mcqw>4. Anticlinal division</Mcqw>
  </Mcq>

### 2. Choose the incorrect statement.
  <Mcq>
    <Mcqw>1. Development is sum of two processes growth and differentiation.</Mcqw>
    <Mcqw>2. Both growth and differentiation are open in higher plants.</Mcqw>
    <Mcqc>3. Increase in length denotes the growth in dorsiventral leaf.</Mcqc>
    <Mcqw>4. Cork cambium appear later in life in gymnosperms.</Mcqw>
  </Mcq>

<GoogleAds slot="1911400781" />

### 3. Choose the incorrect match.
  <Mcq>
    <Mcqw>1. F.W. Went – Auxin</Mcqw>
    <Mcqc>2. Kurosawa – Cytokinin</Mcqc>
    <Mcqw>3. H.H. Cousins- Ethylene</Mcqw>
    <Mcqw>4. F. Skoog- Cytokinin</Mcqw>
  </Mcq>

### 4. Germination in which of the following plants is epigeal?
  <Mcq>
    <Mcqw>1. Maize</Mcqw>
    <Mcqw>2. Mango</Mcqw>
    <Mcqw>3. Radish</Mcqw>
    <Mcqc>4. Bean</Mcqc>
  </Mcq>

### 5. Which is most conspicuous event in any living organism?
  <Mcq>
    <Mcqw>1. Reproduction</Mcqw>
    <Mcqw>2. Maturation</Mcqw>
    <Mcqc>3. Growth</Mcqc>
    <Mcqw>4. Differentiation</Mcqw>
  </Mcq>

### 6. Maize apical meristem gives rise to:
  <Mcq>
    <Mcqw>1. 3,50,000 cells per hour</Mcqw>
    <Mcqc>2. 17,500 cells per hour</Mcqc>
    <Mcqw>3. 3,500 cells per day</Mcqw>
    <Mcqw>4. 17,500 cells per day</Mcqw>
  </Mcq>

### 7. Cells in meristematic phase of growth have which of following characteristics?
A. Rich in protoplasm.  
B. Large nucleus.  
C. Abundant cytoplasmic connections.  
D. Thin cell wall.
  <Mcq>
    <Mcqw>1. Only A and C</Mcqw>
    <Mcqw>2. A, C, D only</Mcqw>
    <Mcqc>3. A,B,C,D</Mcqc>
    <Mcqw>4. Only A and B</Mcqw>
  </Mcq>

### 8. Choose the incorrect option:
  <Mcq>
    <Mcqw>1. Embryo development shows both arithmetic and geometric phases of growth.</Mcqw>
    <Mcqw>2. Increased vacuolation is seen during phase of elongation.</Mcqw>
    <Mcqc>3. In most systems, initial growth if fast and it decreases rapidly thereafter.</Mcqc>
    <Mcqw>4. Relative growth rate refers to efficiency index.</Mcqw>
  </Mcq>

  <GoogleAds slot="1911400781" />

### 9. During which process cells undergo few to major structural changes in their cell walls?
  <Mcq>
    <Mcqw>1. Growth</Mcqw>
    <Mcqw>2. Maturation</Mcqw>
    <Mcqc>3. Differentiation</Mcqc>
    <Mcqw>4. Dedifferentiation</Mcqw>
  </Mcq>

### 10. Choose the incorrect option with respect to development of tracheary element.
  <Mcq>
    <Mcqw>1. Loss of protoplasm</Mcqw>
    <Mcqc>2. Inelastic cell wall</Mcqc>
    <Mcqw>3. Lignin deposition</Mcqw>
    <Mcqw>4. Cell wall becomes very strong</Mcqw>
  </Mcq>

### 11. Odd one out with respect to dedifferentiation
  <Mcq>
    <Mcqw>1. Interfascicular cambium</Mcqw>
    <Mcqc>2. Intrafascicular cambium</Mcqc>
    <Mcqw>3. Cork cambium</Mcqw>
    <Mcqw>4. Root vascular cambium</Mcqw>
  </Mcq>

### 12. Extrinsic factor among the following which controls development.
  <Mcq>
    <Mcqw>1. Intercellular factor</Mcqw>
    <Mcqw>2. Intracellular factor</Mcqw>
    <Mcqc>3. Nutrition</Mcqc>
    <Mcqw>4. Both 1 and 3</Mcqw>
  </Mcq>

### 13. Choose the incorrect match:
  <Mcq>
    <Mcqw>1. Auxin – Indole acetic acid</Mcqw>
    <Mcqc>2. Zeatin – N⁶ fufurylamino purine</Mcqc>
    <Mcqw>3. Abscissic acid – Carotenoids</Mcqw>
    <Mcqw>4. Gibberellic acid – Terpene</Mcqw>
  </Mcq>

### 14. Which PGR plays important role in response to wounds?
  <Mcq>
    <Mcqc>1. ABA</Mcqc>
    <Mcqw>2. GAs</Mcqw>
    <Mcqw>3. IAA</Mcqw>
    <Mcqw>4. Cytokinin</Mcqw>
  </Mcq>

### 15. Choose the correct statement.
  <Mcq>
    <Mcqw>1. Kinetin is obtained for coconut milk.</Mcqw>
    <Mcqw>2. Auxin was first isolated from oat seedlings.</Mcqw>
    <Mcqc>3. Ethylene is largely an inhibitor of growth activities.</Mcqc>
    <Mcqw>4. Dormin was chemically different from ABA.</Mcqw>
  </Mcq>

### 16. Growth regulator used to induce rooting in twig is:
  <Mcq>
    <Mcqw>1. Ethylene</Mcqw>
    <Mcqc>2. Auxin</Mcqc>
    <Mcqw>3. Cytokinin</Mcqw>
    <Mcqw>4. Gibberellic acids</Mcqw>
  </Mcq>

### 17. H.H. cousins confirmed release of volatile substance from P that hastened ripening of stored Q. P and Q are:
  <Mcq>
    <Mcqw>1. Banana and apple</Mcqw>
    <Mcqc>2. Orange and banana</Mcqc>
    <Mcqw>3. Banana and orange</Mcqw>
    <Mcqw>4. Apple and banana</Mcqw>
  </Mcq>

<GoogleAds slot="1911400781" />

### 18. ABA plays an important role in
  <Mcq>
    <Mcqw>1. Seed development</Mcqw>
    <Mcqw>2. Seed maturation</Mcqw>
    <Mcqw>3. Seed dormancy</Mcqw>
    <Mcqc>4. All of these</Mcqc>
  </Mcq>

### 19. State True or False:
A. Any PGR has diverse physiological effects on plants.  
B. Diverse PGR manifest similar effects.
  <Mcq>
    <Mcqw>1. T,F</Mcqw>
    <Mcqw>2. F,T</Mcqw>
    <Mcqc>3. T,T</Mcqc>
    <Mcqw>4. F,T</Mcqw>
  </Mcq>

### 20. Odd one with respect to chemical inhibitors for seed dormancy.
  <Mcq>
    <Mcqw>1. Ascorbic acid</Mcqw>
    <Mcqc>2. Nitrates</Mcqc>
    <Mcqw>3. Phenolic acid</Mcqw>
    <Mcqw>4. Abscissic acids</Mcqw>
  </Mcq>