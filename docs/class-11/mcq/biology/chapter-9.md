---
sidebar_position: 9
title: MCQ Questions Biomolecules Biology Class 11 Chapter 9
description: Biomolecules Chapter 9 Biology Class 11, Boost your performance and score great marks in NEET and Class 11 Exam by practicing these MCQs.
image: https://icdn.talentbrick.com/mcq/class-11-bio-chapter-9.png
sidebar_label: Chapter 9
---

import Mcq, {Mcqinfo, Mcqw, Mcqc, Mcqans} from "@site/src/components/Mcq";

import LoadGoogleAds from "@site/src/components/LoadGoogleAds";

import GoogleAds from "@site/src/components/GoogleAds";

MCQ Quiz Biomolecules Chapter 9 Biology Class 11 MCQ Questions for NEET |  School Exams.

Prepare these important MCQ Questions of Biomolecules (Biology), These are Latest questions to expect in NEET | School Exams.

<Mcqinfo/>
<LoadGoogleAds/>

### 1. To perform chemical analysis acid used is X and we obtain Y slurry. X and Y are:
  <Mcq>
    <Mcqw>1. Trifluoroacetic acid, thin</Mcqw>
    <Mcqc>2. Trichloroacetic acid, thick</Mcqc>
    <Mcqw>3. Trichloroacetic acid, thin</Mcqw>
    <Mcqw>4. Trifluoroacetic acid, thick</Mcqw>
    <Mcqans ans="2. Trichloroacetic acid, thick">Reference: NCERT Page 142</Mcqans>
  </Mcq>

  <GoogleAds slot="1911400781" />

### 2. Analytical techniques give us an idea of:
  <Mcq>
    <Mcqw>1. Molecular formula</Mcqw>
    <Mcqw>2. Probable structure</Mcqw>
    <Mcqw>3. Reactivity with water</Mcqw>
    <Mcqc>4. Both 1 and 2</Mcqc>
    <Mcqans ans="4. Both 1 and 2">Reference: NCERT Page 143</Mcqans>
  </Mcq>

### 3. Choose the incorrect statement:
  <Mcq>
    <Mcqw>1. Silicon is the second most abundant on the earth crust.</Mcqw>
    <Mcqw>2. Destructive experiment is to be done to detect inorganic compounds.</Mcqw>
    <Mcqc>3. Most abundant organic compound is water.</Mcqc>
    <Mcqw>4. All the carbon compounds that we get from living beings can be called biomolecules.</Mcqw>
    <Mcqans ans="3. Most abundant organic compound is water.">Reference: NCERT Page 143</Mcqans>
  </Mcq>

  ### 4. Analysis of compounds gives an idea of:
  <Mcq>
    <Mcqw>1. Carbon</Mcqw>
    <Mcqw>2. Hydrogen</Mcqw>
    <Mcqc>3. Organic constituents</Mcqc>
    <Mcqw>4. Both 1 and 2</Mcqw>
    <Mcqans ans="3. Organic constituents">Reference: NCERT Page 143</Mcqans>
  </Mcq>

### 5. Based on the nature of variable group R there are how many amino acids?
  <Mcq>
    <Mcqw>1. 20</Mcqw>
    <Mcqw>2. 24</Mcqw>
    <Mcqw>3. 19</Mcqw>
    <Mcqc>4. Many</Mcqc>
    <Mcqans ans="4. Many">Reference: NCERT Page 143</Mcqans>
  </Mcq>

### 6. Palmitic acid has:
  <Mcq>
    <Mcqc>1. 16 carbon including carboxyl carbon</Mcqc>
    <Mcqw>2. 15 carbon including carboxyl carbon</Mcqw>
    <Mcqw>3. 17 carbon including carboxyl carbon</Mcqw>
    <Mcqw>4. 16 carbon excluding carboxyl carbon</Mcqw>
    <Mcqans ans="1. 16 carbon including carboxyl carbon">Reference: NCERT Page 144</Mcqans>
  </Mcq>

### 7. Lipids are classified as fats and oils based on
  <Mcq>
    <Mcqw>1. Boiling point</Mcqw>
    <Mcqc>2. Melting point</Mcqc>
    <Mcqw>3. Saturation or unsaturation</Mcqw>
    <Mcqw>4. Presence or absence of phosphorous</Mcqw>
    <Mcqans ans="2. Melting point">Reference: NCERT Page 144</Mcqans>
  </Mcq>

### 8. Which tissue especially has lipids with complex structures:
  <Mcq>
    <Mcqc>1. Neural tissue</Mcqc>
    <Mcqw>2. Cardiac tissue</Mcqw>
    <Mcqw>3. Connective tissue</Mcqw>
    <Mcqw>4. Epithelial tissue</Mcqw>
    <Mcqans ans="1. Neural tissue">Reference: NCERT Page 144</Mcqans>
  </Mcq>

<GoogleAds slot="1911400781" />

### 9. There are how many types of nucleotides?
  <Mcq>
    <Mcqw>a. 4</Mcqw>
    <Mcqw>b. 2</Mcqw>
    <Mcqw>c. 1</Mcqw>
    <Mcqc>d. 5</Mcqc>
    <Mcqans ans="d. 5">Reference: NCERT Page 160</Mcqans>
  </Mcq>

### 10. Choose the correct statement:
  <Mcq>
    <Mcqw>1. Adenine has only one oxygen atom in its structure.</Mcqw>
    <Mcqw>2. Cholesterol has 25 carbon in its structure.</Mcqw>
    <Mcqc>3. Nucleic acids like DNA and RNA consists of nucleotides only.</Mcqc>
    <Mcqw>4. All of the above.</Mcqw>
    <Mcqans ans="3. Nucleic acids like DNA and RNA consists of nucleotides only.">Reference: Options 1 and 2: Pg 145<br/>Option 3: Pg 144</Mcqans>
  </Mcq>

### 11. Choose the incorrect statement:
  <Mcq>
    <Mcqw>1. All compounds found in the acid-soluble pool have their, molecular weights ranging from 18 to 800 Da.</Mcqw>
    <Mcqw>2. Polysaccharides, nucleic acids, Proteins comprise the macromolecular fraction of any living tissue.</Mcqw>
    <Mcqc>3. When we grind tissue we do not disturb the cell structure.</Mcqc>
    <Mcqw>4. Percentage of protein is about 10 – 15 in cellular mass.</Mcqw>
    <Mcqans ans="3. When we grind tissue we do not disturb the cell structure.">Reference: Option 1: Pg 146<br/>Option 2: Pg 149<br/>Options 3 and 4: Pg 147</Mcqans>
  </Mcq>

### 12. Polysaccharides are found in:
  <Mcq>
    <Mcqw>1. Acid soluble pellet</Mcqw>
    <Mcqw>2. Acid insoluble supernatant</Mcqw>
    <Mcqw>3. Acid soluble supernatant</Mcqw>
    <Mcqc>4. Acid insoluble pellet</Mcqc>
    <Mcqans ans="4. Acid insoluble pellet">Reference: NCERT Page 148</Mcqans>
  </Mcq>

### 13. Complex polysaccharides are mostly:
  <Mcq>
    <Mcqc>1. Homopolymers</Mcqc>
    <Mcqw>2. Heteropolymers</Mcqw>
    <Mcqw>3. Copolymers</Mcqw>
    <Mcqw>4. Branched-chain polymers</Mcqw>
    <Mcqans ans="1. Homopolymers">Reference: NCERT Page 149</Mcqans>
  </Mcq>

### 14. A nucleotide has how many chemically distinct components:
  <Mcq>
    <Mcqw>a. 2</Mcqw>
    <Mcqw>b. 4</Mcqw>
    <Mcqw>c. 1</Mcqw>
    <Mcqc>d. 3</Mcqc>
    <Mcqans ans="d. 3">Reference: NCERT Page 149</Mcqans>
  </Mcq>

### 15. In proteins:
  <Mcq>
    <Mcqw>1. Only left-handed helices are observed</Mcqw>
    <Mcqw>2. Helices nature change with temperature</Mcqw>
    <Mcqc>3. Only right-handed helices are observed</Mcqc>
    <Mcqw>4. Both left and right handed helices are observed</Mcqw>
    <Mcqans ans="3. Only right-handed helices are observed">Reference: NCERT Page 150</Mcqans>
  </Mcq>

### 16. Nucleic acids exhibit ___ variety of secondary structures:
  <Mcq>
    <Mcqw>1. Little</Mcqw>
    <Mcqc>2. Wide</Mcqc>
    <Mcqw>3. No variety</Mcqw>
    <Mcqw>4. Almost all</Mcqw>
    <Mcqans ans="2. Wide">Reference: NCERT Page 151</Mcqans>
  </Mcq>

### 17. One of the greatest discovery ever made was:
  <Mcq>
    <Mcqw>1. 10 metabolic steps in Glycolysis</Mcqw>
    <Mcqw>2. Principle of Bioenergetics</Mcqw>
    <Mcqc>3. All biomolecules have a turnover</Mcqc>
    <Mcqw>4. Structure of alpha-helix</Mcqw>
    <Mcqans ans="3. All biomolecules have a turnover">Reference: NCERT Page 152</Mcqans>
  </Mcq>

<GoogleAds slot="1911400781" />

### 18. Choose the correct statement:
  <Mcq>
    <Mcqw>1. Majority of metabolic reactions occur in isolation.</Mcqw>
    <Mcqw>2. Glucose in a normal individual is 4.2mol/L – 6.1 mol/L.</Mcqw>
    <Mcqc>3. Every biochemical reaction is a catalyzed reaction.</Mcqc>
    <Mcqw>4. Metabolic pathways are circular only.</Mcqw>
    <Mcqans ans="3. Every biochemical reaction is a catalyzed reaction.">Reference: Options 1,2 and 4: Pg 152<br/>Option 3: Pg 153</Mcqans>
  </Mcq>

### 19. Choose the correct statement:
A. Metabolic reactions are always linked to some other reactions.  
B. Flow of metabolite through metabolic pathway is at definite rate.
  <Mcq>
    <Mcqw>1. Both A and B are incorrect</Mcqw>
    <Mcqc>2.Both A and B are correct</Mcqc>
    <Mcqw>3. Only A is correct</Mcqw>
    <Mcqw>4. Only B is correct</Mcqw>
    <Mcqans ans="2.Both A and B are correct">Reference: NCERT Page 152</Mcqans>
  </Mcq>

### 20. Cholesterol is derived from:
  <Mcq>
    <Mcqw>1. Butryric acid</Mcqw>
    <Mcqw>2. Ciric acid</Mcqw>
    <Mcqc>3. Acetic acid</Mcqc>
    <Mcqw>4. Amino acids</Mcqw>
    <Mcqans ans="3. Acetic acid">Reference: NCERT Page 153</Mcqans>
  </Mcq>

### 21. Metabolism is synonymous to:
  <Mcq>
    <Mcqw>1. Equilibrium state</Mcqw>
    <Mcqc>2. Living state</Mcqc>
    <Mcqw>3.Non steady state</Mcqw>
    <Mcqw>4. Transition state</Mcqw>
    <Mcqans ans="2. Living state">Reference: NCERT Page 153</Mcqans>
  </Mcq>

### 22. In absence of any enzyme rate of formation of carbonic acid is about
  <Mcq>
    <Mcqw>1. 200 molecules per second</Mcqw>
    <Mcqw>2. 600000 molecules per hour</Mcqw>
    <Mcqc>3. 200 molecules per hour</Mcqc>
    <Mcqw>4. 600000 molecules per second</Mcqw>
    <Mcqans ans="3. 200 molecules per hour">Reference: NCERT Page 155</Mcqans>
  </Mcq>

### 23. Glycolysis is __ step reaction and catalysed by ___ different enzymes.
  <Mcq>
    <Mcqw>1. 10, 5</Mcqw>
    <Mcqw>2. 12,10</Mcqw>
    <Mcqw>3. 10,12</Mcqw>
    <Mcqc>4. 10,10</Mcqc>
    <Mcqans ans="4. 10,10">Reference: NCERT Page 153</Mcqans>
  </Mcq>

### 24. Intermediate formed in Enzyme action is:
  <Mcq>
    <Mcqw>1. ES complex</Mcqw>
    <Mcqc>2. EP complex</Mcqc>
    <Mcqw>3. PS complex</Mcqw>
    <Mcqw>4. Both 2 and 3</Mcqw>
  </Mcq>

### 25. Succinate dehydrogenase is inhibited by:
  <Mcq>
    <Mcqc>1. Malonate</Mcqc>
    <Mcqw>2. Malate</Mcqw>
    <Mcqw>3. Aspartate</Mcqw>
    <Mcqw>4. Oxalosuccinate</Mcqw>
    <Mcqans ans="1. Malonate">Reference: NCERT Page 158</Mcqans>
  </Mcq>

<GoogleAds slot="1911400781" />

### 26. In enzyme classification there are how many subclasses for each class?
  <Mcq>
    <Mcqw>a. 2-6</Mcqw>
    <Mcqc>b. 4-13</Mcqc>
    <Mcqw>c. 6</Mcqw>
    <Mcqw>d. 13-20</Mcqw>
    <Mcqans ans="b. 4-13">Reference: NCERT Page 158</Mcqans>
  </Mcq>

### 27. Enzymes are named according to how many no. of digits?
  <Mcq>
    <Mcqc>a. 4</Mcqc>
    <Mcqw>b. 6</Mcqw>
    <Mcqw>c. 2</Mcqw>
    <Mcqw>d. 9</Mcqw>
    <Mcqans ans="a. 4">Reference: NCERT Page 158</Mcqans>
  </Mcq>

### 28. Enzymes catalysing transfer of group H between a pair of substrate is:
  <Mcq>
    <Mcqw>1. Transferases</Mcqw>
    <Mcqw>2. Isomerases</Mcqw>
    <Mcqc>3. Dehydrogenase</Mcqc>
    <Mcqw>4. Lyases</Mcqw>
    <Mcqans ans="3. Dehydrogenase">Reference: NCERT Page 158</Mcqans>
  </Mcq>

### 29. Choose the incorrect statement:
  <Mcq>
    <Mcqw>1. Co-enzymes are organic compounds.</Mcqw>
    <Mcqw>2. Metal ions forms coordinate bonds with side chains.</Mcqw>
    <Mcqc>3. Prosthetic group haem is part of site other than active site of enzyme.</Mcqc>
    <Mcqw>4. NAD has two phosphate group in its structure.</Mcqw>
    <Mcqans ans="3. Prosthetic group haem is part of site other than active site of enzyme.">Reference: Options 1,2 and 3: Pg 159<br/>Option 4: See full form of NAD and you’ll understand 😊</Mcqans>
  </Mcq>

### 30. Choose the incorrect statement:
  <Mcq>
    <Mcqc>1. Cellulose is a primary metabolite.</Mcqc>
    <Mcqw>2. Some secondary metabolites have ecological importance.</Mcqw>
    <Mcqw>3. Gums are polymeric in nature.</Mcqw>
    <Mcqw>4. Curcumin is categorised as a drug among secondary metabolites.</Mcqw>
    <Mcqans ans="1. Cellulose is a primary metabolite.">Reference: NCERT Page 146</Mcqans>
  </Mcq>

  <GoogleAds slot="1911400781" />