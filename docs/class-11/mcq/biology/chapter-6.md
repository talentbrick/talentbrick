---
sidebar_position: 6
title: MCQ Questions Anatomy of Flowering Plants Chapter 6 Class 11 Biology
description: Anatomy of Flowering Plants Chapter 6 Biology Class 11, Boost your performance and score great marks in NEET and Class 11 Exam by practicing these MCQs.
image: https://icdn.talentbrick.com/mcq/class-11-bio-chapter-6.png
sidebar_label: Chapter 6
---

import Mcq, {Mcqinfo, Mcqw, Mcqc, Mcqans} from "@site/src/components/Mcq";

import LoadGoogleAds from "@site/src/components/LoadGoogleAds";

import GoogleAds from "@site/src/components/GoogleAds";

**NCERT** Biology Class 11th Ch.6 Anatomy of Flowering Plants MCQ Practice Questions for NEET | Class 11.

Prepare these **important MCQ Questions** of Biology Class 11 Anatomy of Flowering Plants Chapter 6, Latest questions to expected to come in **NEET | School Exams**.

<Mcqinfo/>
<LoadGoogleAds/>

### 1. During the formation of primary plant body specific regions of apical meristem produce __
  <Mcq>
    <Mcqw>1. Dermal tissues</Mcqw>
    <Mcqw>2.Ground tissues</Mcqw>
    <Mcqw>3. Vascular tissues</Mcqw>
    <Mcqc>4. All of the these</Mcqc>
    <Mcqans ans="4. All of the these">Reference: NCERT Page 85</Mcqans>
  </Mcq>

### 2. Choose the incorrect match with respect to the following diagram.
![Choose the incorrect match with respect to the following diagram.](https://icdn.talentbrick.com/Static/Choose%20the%20incorrect%20match%20with%20respect%20to%20the%20following%20diagram.png)
  <Mcq>
    <Mcqw>1. A: Central cylinder</Mcqw>
    <Mcqc>2. B: Protoderm</Mcqc>
    <Mcqw>3. C: Root cap</Mcqw>
    <Mcqw>4. D: Root Apical meristem</Mcqw>
    <Mcqans ans="2. B: Protoderm">Reference: NCERT Page 85</Mcqans>
  </Mcq>

<GoogleAds slot="1911400781" />

 ### 3. Choose the Odd one out with respect to cylindrical meristems.
  <Mcq>
    <Mcqw>1. Intrafascicular meristem</Mcqw>
    <Mcqc>2. Intercalary meristem</Mcqc>
    <Mcqw>3. Interfascicular meristem</Mcqw>
    <Mcqw>4. Cork cambium</Mcqw>
    <Mcqans ans="2. Intercalary meristem">Reference: NCERT Page 85</Mcqans>
  </Mcq>

### 4. Choose the correct statement with respect to collenchyma.
  <Mcq>
    <Mcqw>1. They occur below endodermis in most of the dicots.</Mcqw>
    <Mcqw>2. They are either closely packed or have small intercellular spaces.</Mcqw>
    <Mcqc>3. They often contain chloroplasts.</Mcqc>
    <Mcqw>4. Corners of these cells are thickened due to deposition of cutin.</Mcqw>
    <Mcqans ans="3. They often contain chloroplasts.">Reference: NCERT Page 86</Mcqans>
  </Mcq>

 ### 5. Choose the correct statement for sclerenchyma.
  <Mcq>
    <Mcqc>1. On the basis of structure, origin and development scelerchyma may be either sclereids or fibres.</Mcqc>
    <Mcqw>2. They are commonly found in fruit wall of guava.</Mcqw>
    <Mcqw>3. Fibres do not occur in groups, in various parts of plants.</Mcqw>
    <Mcqw>4. They provide mechanical support to young stems and petiole.</Mcqw>
    <Mcqans ans="1. On the basis of structure, origin and development scelerchyma may be either sclereids or fibres.">Reference: Options 1,3 and 4: Pg 86<br/>Option 2: Pg 87</Mcqans>
  </Mcq>

### 6. Thick walled bundle sheath cells are found in:
  <Mcq>
    <Mcqw>1. Monocot leaf</Mcqw>
    <Mcqc>2. Dicot leaf</Mcqc>
    <Mcqw>3. Monocot roots</Mcqw>
    <Mcqw>4. Dicot roots</Mcqw>
    <Mcqans ans="2. Dicot leaf">Reference: NCERT Page 93</Mcqans>
  </Mcq>

### 7. Thick walled parenchyma cells are found in:
  <Mcq>
    <Mcqw>1. Monocot root </Mcqw>
    <Mcqw>2. Dicot stem pericycle</Mcqw>
    <Mcqc>3. Dicot root pericycle</Mcqc>
    <Mcqw>4. Dicot stem cortex</Mcqw>
    <Mcqans ans="3. Dicot root pericycle">Reference: NCERT Page 91</Mcqans>
  </Mcq>

<GoogleAds slot="1911400781" />

### 8. Intiation of vascular cambium during secondary growth in roots takes place in which cells?
  <Mcq>
    <Mcqw>1. Conjuctive tissue</Mcqw>
    <Mcqc>2. Pericycle</Mcqc>
    <Mcqw>3. Phloem</Mcqw>
    <Mcqw>4. Cortex</Mcqw>
    <Mcqans ans="2. Pericycle">Reference: NCERT Page 91</Mcqans>
  </Mcq>

### 9. Correcy description about phellogen.
  <Mcq>
    <Mcqw>1. It is single layered,thin walled with oval cells.</Mcqw>
    <Mcqc>2. It is rectangular, and made up of two layers.</Mcqc>
    <Mcqw>3. It cuts off cells only on inner side.</Mcqw>
    <Mcqw>4. It develops in stelar region of dicot stem.</Mcqw>
    <Mcqans ans="2. It is rectangular, and made up of two layers.">Reference: NCERT Page 96</Mcqans>
  </Mcq>

### 10. Choose the incorrect match among the following.
  <Mcq>
    <Mcqw>1. Lenticels: Lens shaped</Mcqw>
    <Mcqc>2. Radical conduction: Conjuctive tissue</Mcqc>
    <Mcqw>3. Xylem parenchyma: Fats and Starch</Mcqw>
    <Mcqw>4. Sieve tube elements: Peripheral cytoplasm</Mcqw>
    <Mcqans ans="2. Radical conduction: Conjuctive tissue">Reference: Option 1: Pg 96<br/>Option 2: Pg 87 and Pg 91<br/>Option 3: Pg 87<br/>Option 4: Pg 88</Mcqans>
  </Mcq>

### 11. Identify the tissue/group of cells from the following information.
i. Elongated, tapering, cylindrical.  
ii. It has pits through which plasmodesmatal connection exist between cells.
  <Mcq>
    <Mcqw>1. Xylem parenchyma</Mcqw>
    <Mcqc>2. Phloem parenchyma</Mcqc>
    <Mcqw>3. Xylem fibres</Mcqw>
    <Mcqw>4. Phloem fibres</Mcqw>
    <Mcqans ans="2. Phloem parenchyma">Reference: NCERT Page 88</Mcqans>
  </Mcq>

### 12. Spring wood and autumn wood are seen in:
  <Mcq>
    <Mcqw>1. Dicot stem, tropical region</Mcqw>
    <Mcqw>2. Dicot root, temperate region</Mcqw>
    <Mcqc>3. Dicot stem, temperate region</Mcqc>
    <Mcqw>4. Monocot stem, temperate region</Mcqw>
    <Mcqans ans="3. Dicot stem, temperate region">Reference: NCERT Page 96</Mcqans>
  </Mcq>

<GoogleAds slot="1911400781" />

### 13. State True or False
i. Monocots and dicots differ in type, number and location of vascular bundles.  
ii. There are different type of wood on the basis of their time of production.  
iii. Ground tissue is divided into three zones namely hypodermis, cortex and endodermis.
  <Mcq>
    <Mcqw>1. T,T,T</Mcqw>
    <Mcqc>2. T,T,F</Mcqc>
    <Mcqw>3. F,T,F</Mcqw>
    <Mcqw>4. F,F,T</Mcqw>
    <Mcqans ans="2. Pericycle">Reference: NCERT Pg 96, Pg 98 and Pg 99</Mcqans>
  </Mcq>

### 14. Choose the incorrect option for bast fibres.
  <Mcq>
    <Mcqc>1. They are generally absent in primary phloem.</Mcqc>
    <Mcqw>2. They are much elongated and branched.</Mcqw>
    <Mcqw>3. Phloem fibres of flax and hemp are used commercailly.</Mcqw>
    <Mcqw>4. At maturity, they loose their protoplasm.</Mcqw>
    <Mcqans ans="1. They are generally absent in primary phloem.">Reference: NCERT Page 88</Mcqans>
  </Mcq>

### 15. Choose the incorrect statement.
  <Mcq>
    <Mcqw>1. Trichome is usually multicellular.</Mcqw>
    <Mcqw>2. Subsidiary cells are specialised in their shape and size.</Mcqw>
    <Mcqc>3. Stomatal appartus is comprises of guard cells, stomatal aperture and complimentary cells.</Mcqc>
    <Mcqw>4. Epidermal cells have small amount of cytoplasm lining the cell wall.</Mcqw>
    <Mcqans ans="3. Stomatal appartus is comprises of guard cells, stomatal aperture and complimentary cells.">Reference: NCERT Page 89</Mcqans>
  </Mcq>

<GoogleAds slot="1911400781" />