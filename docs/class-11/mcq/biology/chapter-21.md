---
sidebar_position: 21
title: MCQ Questions Neural Control and Coordination Biology Class 11 Ch 21
description: Neural Control and Coordination Biology, MCQs for Class 11, These are the latest questions to expect in NEET | School Exams | Competitive Exams.
image: https://icdn.talentbrick.com/mcq/class-11-bio-chapter-21.png
sidebar_label: Chapter 21
---

import Mcq, {Mcqinfo, Mcqw, Mcqc, Mcqans} from "@site/src/components/Mcq";

import LoadGoogleAds from "@site/src/components/LoadGoogleAds";

import GoogleAds from "@site/src/components/GoogleAds";

NCERT Biology Class 11 MCQ Practice Questions Neural Control and Coordination Chapter 21 for NEET | Class 11

Prepare these important MCQ Questions of Neural control and coordination in Biology, Latest questions to expect in NEET | School Exams.

<Mcqinfo/>
<LoadGoogleAds/>

### 1. Which of the following is not performed by neuron?
  <Mcq>
    <Mcqw>1. Detect stimuli</Mcqw>
    <Mcqw>2. Transmit stimuli</Mcqw>
    <Mcqc>3. Generate stimuli</Mcqc>
    <Mcqw>4. Receive stimuli</Mcqw>
  </Mcq>

  <GoogleAds slot="1911400781" />

### 2. Choose the incorrect statement.
  <Mcq>
    <Mcqw>1. The neural system coordinates and integrates functions as well as metabolism of all the organs.</Mcqw>
    <Mcqw>2. Very important part of forebrain is called Hypothalamus</Mcqw>
    <Mcqw>3. Neural system of all animals is composed of highly specialised cells.</Mcqw>
    <Mcqc>4. Invertebrates have a more developed neural system.</Mcqc>
  </Mcq>

### 3. Choose the incorrect match.
  <Mcq>
    <Mcqw>1. Insects – Brain</Mcqw>
    <Mcqw>2. Human – Central nervous system</Mcqw>
    <Mcqc>3. Porifera- Neuron</Mcqc>
    <Mcqw>4. Hydra- Network of neurons</Mcqw>
  </Mcq>

### 4. Nerve fibres of PNS are of __ types.
  <Mcq>
    <Mcqw>a. 3</Mcqw>
    <Mcqc>b. 2</Mcqc>
    <Mcqw>c. 4</Mcqw>
    <Mcqw>d. 5</Mcqw>
  </Mcq>

### 5. Autonomic neural system transmits impulses from CNS to:
  <Mcq>
    <Mcqw>1. Involuntary organs</Mcqw>
    <Mcqw>2. Smooth muscles</Mcqw>
    <Mcqw>3. Skeletal muscles</Mcqw>
    <Mcqc>4. Both 1 and 2</Mcqc>
  </Mcq>

### 6. Neurons in hydra are of which type?
  <Mcq>
    <Mcqw>1. Unipolar</Mcqw>
    <Mcqc>2. Apolar</Mcqc>
    <Mcqw>3. Bipolar</Mcqw>
    <Mcqw>4. Mutipolar</Mcqw>
  </Mcq>

### 7. Visceral nervous system comprises of
A. Plexuses  
B. Ganglia  
C. Nerves  
D. Fibres
  <Mcq>
    <Mcqw>1. Only B and C</Mcqw>
    <Mcqw>2. A, B, C only</Mcqw>
    <Mcqc>3. A,B,C,D</Mcqc>
    <Mcqw>4. Only A and B</Mcqw>
  </Mcq>

### 8. Choose the incorrect option:
  <Mcq>
    <Mcqw>1. Neuron is microscopic structure</Mcqw>
    <Mcqw>2. Axon hillock is part of Cyton</Mcqw>
    <Mcqw>3. Axon is a long fibre</Mcqw>
    <Mcqc>4. Based on number of axon and dendrites neurons are divided into 5 types.</Mcqc>
  </Mcq>

### 9. Which of the following neuron have one axon and one dendrite
  <Mcq>
    <Mcqw>1. Bipolar neuron</Mcqw>
    <Mcqw>2. Unipolar neuron</Mcqw>
    <Mcqw>3. Pseudounipolar neuron</Mcqw>
    <Mcqc>4. Both 1 and 3</Mcqc>
  </Mcq>

<GoogleAds slot="1911400781" />

### 10. Choose the correct statement with respect to myelinated nerve fibres.
  <Mcq>
    <Mcqw>1. Enclosed by schwann cells.</Mcqw>
    <Mcqc>2. Enveloped by schwann cells.</Mcqc>
    <Mcqw>3. Schwann do not enclose nor envelop them.</Mcqw>
    <Mcqw>4. Commonly found in autonomous neural system.</Mcqw>
  </Mcq>

### 11. Membrane is impermeable to __ present in axoplasm.
  <Mcq>
    <Mcqw>1. Na⁺</Mcqw>
    <Mcqw>2. Lipids</Mcqw>
    <Mcqc>3. Negatively charges proteins</Mcqc>
    <Mcqw>4. Both 1 and 3</Mcqw>
  </Mcq>

### 12. Ionic gradients across membrane are restored by:
  <Mcq>
    <Mcqw>1. ATP pump</Mcqw>
    <Mcqw>2. Voltage gated Na⁺ channels</Mcqw>
    <Mcqc>3. Voltage gated K⁺ channels</Mcqc>
    <Mcqw>4. Leaky channels</Mcqw>
  </Mcq>

### 13. Incorrect with respect to electrical synapse:
  <Mcq>
    <Mcqw>1. Impulse flow is bidirectional.</Mcqw>
    <Mcqw>2. Impulse transmission across an electrical synapse is always faster than chemical synapse.</Mcqw>
    <Mcqw>3. Electrical synapse are rare in humans.</Mcqw>
    <Mcqc>4. Transmission across them is similar to conduction along two axons.</Mcqc>
  </Mcq>

### 14. Which part forms major part of human brain.
  <Mcq>
    <Mcqw>1. Thalamus</Mcqw>
    <Mcqw>2. Hypothalamus</Mcqw>
    <Mcqc>3. Cerebrum</Mcqc>
    <Mcqw>4. Medulla</Mcqw>
  </Mcq>

### 15. Cerebral cortex contains:
  <Mcq>
    <Mcqw>1. Small regions , neither sensory nor motor in function.</Mcqw>
    <Mcqw>2. Large regions , either sensory nor motor in function.</Mcqw>
    <Mcqc>3. Large regions , neither sensory nor motor in function.</Mcqc>
    <Mcqw>4. Small regions , both sensory and motor in function.</Mcqw>
  </Mcq>

<GoogleAds slot="1911400781" />

### 16. Which of the following is not the function of association area?
  <Mcq>
    <Mcqw>1. Intersensory association</Mcqw>
    <Mcqw>2. Memory</Mcqw>
    <Mcqw>3. Communication</Mcqw>
    <Mcqc>4. Expression of emotions</Mcqc>
  </Mcq>

### 17. Major coordinating centre for motor and sensory signalling is:
  <Mcq>
    <Mcqw>1. Hypothalamus</Mcqw>
    <Mcqw>2. Cerebellum</Mcqw>
    <Mcqc>3. Thalamus</Mcqc>
    <Mcqw>4. Cerebrum</Mcqw>
  </Mcq>

### 18. Choose the correct statement:
  <Mcq>
    <Mcqw>1. Excitor transmits impulse to CNS.</Mcqw>
    <Mcqc>2. Motor end plate receives impulse from CNS.</Mcqc>
    <Mcqw>3. Pons connect brain and spinal cord</Mcqw>
    <Mcqw>4. Interneuron is present in white matter of spinal cord.</Mcqw>
  </Mcq>

<GoogleAds slot="1911400781" />

### 19. State True or False:
A. Sensory organs detect all types of changes in environment.  
B. Olfactory receptors are made of epithelium.
  <Mcq>
    <Mcqw>1. T,F</Mcqw>
    <Mcqw>2. F,T</Mcqw>
    <Mcqc>3. T,T</Mcqc>
    <Mcqw>4. F,T</Mcqw>
  </Mcq>

### 20. Which is extension of brain’s limbic system.
  <Mcq>
    <Mcqw>1. Olfactory epithelium</Mcqw>
    <Mcqc>2. Olfactory bulb</Mcqc>
    <Mcqw>3. Olfactory bipolar cells</Mcqw>
    <Mcqw>4. Bowmann’s gland</Mcqw>
  </Mcq>

### 21. External layer of eye is composed of:
  <Mcq>
    <Mcqw>1. Loose connective tissue</Mcqw>
    <Mcqw>2. Specialised connective tissue</Mcqw>
    <Mcqc>3. Dense connective tissue</Mcqc>
    <Mcqw>4. Epithelial tissue</Mcqw>
  </Mcq>

### 22. Lens is held in place by:
  <Mcq>
    <Mcqw>1. Ciliary muscles</Mcqw>
    <Mcqc>2. Ligaments</Mcqc>
    <Mcqw>3. Iris</Mcqw>
    <Mcqw>4. Cornea</Mcqw>
  </Mcq>

### 23. Choose the correct with respect to location.
  <Mcq>
    <Mcqw>1. Blind spot is medial to and slightly below posterior pole of eye ball.</Mcqw>
    <Mcqw>2. Lateral to blind spot there is pigmented spot called Fovea.</Mcqw>
    <Mcqw>3. Blind spot is lateral to and slightly below posterior pole of eye ball.</Mcqw>
    <Mcqc>4. Lateral to blind spot there is pigmented spot called Macula lutea.</Mcqc>
  </Mcq>

### 24. Tympanic membrane is part of:
  <Mcq>
    <Mcqc>1. External ear</Mcqc>
    <Mcqw>2. Middle ear</Mcqw>
    <Mcqw>3. Cochlea</Mcqw>
    <Mcqw>4. Vestibular apparatus</Mcqw>
  </Mcq>

### 25. Auditory sense is perceived in which lobe of brain?
  <Mcq>
    <Mcqw>1. Parietal</Mcqw>
    <Mcqc>2. Temporal</Mcqc>
    <Mcqw>3. Frontal</Mcqw>
    <Mcqw>4. Occipital</Mcqw>
  </Mcq>

### 26. Tympanic membrane consists of:
  <Mcq>
    <Mcqw>1. Connective tissue covered with skin on both the sides.</Mcqw>
    <Mcqw>2. Connective tissue covered with skin on inner side and mucus membrane on outer side.</Mcqw>
    <Mcqc>3. Connective tissue covered with skin on outer side and mucus membrane on inner side.</Mcqc>
    <Mcqw>4. Mucus membrane on both the sides.</Mcqw>
  </Mcq>

### 27. Scala tympani terminates at the X which opens to Y. X and Y are respectively:
  <Mcq>
    <Mcqc>1. Round window , Middle ear</Mcqc>
    <Mcqw>2. Oval window , Middle ear</Mcqw>
    <Mcqw>3. Round window , Inner ear</Mcqw>
    <Mcqw>4. Oval window , Inner ear</Mcqw>
  </Mcq>

  <GoogleAds slot="1911400781" />

### 28. Above the rows of hair cells there is:
  <Mcq>
    <Mcqw>1. Thick elastic membrane , Tectorial membrane.</Mcqw>
    <Mcqc>2. Thin elastic membrane , Tectorial membrane.</Mcqc>
    <Mcqw>3. Thick elastic membrane , Basilar membrane.</Mcqw>
    <Mcqw>4. Thin elastic membrane , Reissner’s membrane.</Mcqw>
  </Mcq>

### 29. State true or false.
A. Nerve impulses are generated and transmitted by afferent fibres to auditory cortex of brain.  
B. Vestibular apparatus is located below Cochlea.
  <Mcq>
    <Mcqc>1. T,F</Mcqc>
    <Mcqw>2. T,T</Mcqw>
    <Mcqw>3. F,T</Mcqw>
    <Mcqw>4. F,F</Mcqw>
  </Mcq>

### 30. Which of the following integrates information received from semicircular canals and the auditory system.
  <Mcq>
    <Mcqw>1. Temporal lobe</Mcqw>
    <Mcqc>2. Cerebellum</Mcqc>
    <Mcqw>3. Medulla</Mcqw>
    <Mcqw>4. Thalamus</Mcqw>
  </Mcq>