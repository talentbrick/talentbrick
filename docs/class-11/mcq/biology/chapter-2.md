---
sidebar_position: 2
title: MCQ Questions Biological Classification Class 11 Chapter 2
description: Biological Classification MCQ Questions Class 11 Biology, These are the latest questions to expect in NEET | School Exams | Competitive Exams.
image: https://icdn.talentbrick.com/mcq/class-11-bio-chapter-2.png
sidebar_label: Chapter 2
---

import Mcq, {Mcqinfo, Mcqw, Mcqc, Mcqans} from "@site/src/components/Mcq";

import LoadGoogleAds from "@site/src/components/LoadGoogleAds";

import GoogleAds from "@site/src/components/GoogleAds";

**NCERT MCQ Quiz** for Class 11 Biology Chapter 2 Biological Classification for NEET | School Exams | Class 11.

Prepare these important MCQ Questions of Class 11 Biology Chapter 2 Biological Classification with Answers, These are Latest questions to expect in **NEET | School Exams**.

<Mcqinfo/>
<LoadGoogleAds/>

### 1. Biological classification of plants and animals was first proposed by:
  <Mcq>
    <Mcqw>1. Linnaeus</Mcqw>
    <Mcqc>2. Aristotle</Mcqc>
    <Mcqw>3. R.H. Whittaker</Mcqw>
    <Mcqw>4. Theophrastus</Mcqw>
    <Mcqans ans="2. Aristotle">Reference: NCERT Page 27</Mcqans>
  </Mcq>

### 2. Choose the correct statement.
  <Mcq>
    <Mcqw>1. Aristotle used simple morphological characters to classify animals.</Mcqw>
    <Mcqw>2. Monera have cellulosic cell wall.</Mcqw>
    <Mcqw>3. Loose tissue body organization is seen in kingdom Plantae.</Mcqw>
    <Mcqc>4. In two kingdom classification, kingdoms included all plants and animals.</Mcqc>
    <Mcqans ans="4. In two kingdom classification, kingdoms included all plants and animals.">Reference: Option 1: Pg 16<br/>Options 2 and 3: Pg 17<br/>Option 4: Pg 16</Mcqans>
  </Mcq>

<GoogleAds slot="1911400781" />

### 3. How many main criteria were used by RH whittaker?
  <Mcq>
    <Mcqw>a. 3</Mcqw>
    <Mcqw>b. 4</Mcqw>
    <Mcqc>c. 5</Mcqc>
    <Mcqw>d. 6</Mcqw>
    <Mcqans ans="c. 5">Reference: NCERT Page 17</Mcqans>
  </Mcq>

### 4. Comma shaped bacteria are called:
  <Mcq>
    <Mcqw>1. Bacillus</Mcqw>
    <Mcqc>2. Vibrio</Mcqc>
    <Mcqw>3. Cocci</Mcqw>
    <Mcqw>4. Both 1 and 2</Mcqw>
    <Mcqans ans="2. Vibrio">Reference: NCERT Page 18</Mcqans>
  </Mcq>

### 5. Majority of bacteria are:
  <Mcq>
    <Mcqw>1. Photosynthetic</Mcqw>
    <Mcqw>2. Chemosynthetic</Mcqw>
    <Mcqc>3. Decomposers</Mcqc>
    <Mcqw>4. Parasitic</Mcqw>
    <Mcqans ans="3. Decomposers">Reference: NCERT Page 19</Mcqans>
  </Mcq>

### 6. Which of the following have pigments similar to green plants?
  <Mcq>
    <Mcqc>1. Cyanobacteria</Mcqc>
    <Mcqw>2. Euglenoids</Mcqw>
    <Mcqw>3. Fungi</Mcqw>
    <Mcqw>4. Archaebacteria</Mcqw>
    <Mcqans ans="1. Cyanobacteria">Reference: NCERT Page 19</Mcqans>
  </Mcq>

### 7. Which of the following are oxidised by chemosynthetic bacteria ?
A. Nitrates  
B. Nitrites  
C. Ammonia
  <Mcq>
    <Mcqw>1. Only B and C</Mcqw>
    <Mcqc>2. A, B, C</Mcqc>
    <Mcqw>3. Only C</Mcqw>
    <Mcqw>4. Only B</Mcqw>
    <Mcqans ans="2. A, B, C">Reference: NCERT Page 21</Mcqans>
  </Mcq>

 ### 8. Choose the incorrect option.
  <Mcq>
    <Mcqw>1. Amoboid protozoans are found in moist soil.</Mcqw>
    <Mcqc>2. In diatoms cell walls form two thick overlapping shell which fit together.</Mcqc>
    <Mcqw>3. Dinoflagellates are mostly marine.</Mcqw>
    <Mcqw>4. Dinoflagellates appear yellow, brown and blue.</Mcqw>
    <Mcqans ans="2. In diatoms cell walls form two thick overlapping shell which fit together.">Reference: Options 1,3 and 4: Pg 21<br/>Option 2: Pg 20</Mcqans>
  </Mcq>

### 9. Pigments of which organisms are identical to those of higher plants?
  <Mcq>
    <Mcqw>1. Diatoms</Mcqw>
    <Mcqw>2. Slime moulds</Mcqw>
    <Mcqw>3. Cyanobacteria</Mcqw>
    <Mcqc>4. Euglenoids</Mcqc>
    <Mcqans ans="4. Euglenoids">Reference: NCERT Page 21</Mcqans>
  </Mcq>

### 10. Spores of slime moulds are dispersed by:
  <Mcq>
    <Mcqw>1. Water</Mcqw>
    <Mcqw>2. Soil</Mcqw>
    <Mcqc>3. Wind</Mcqc>
    <Mcqw>4. Insects</Mcqw>
    <Mcqans ans="3. Wind">Reference: NCERT Page 21</Mcqans>
  </Mcq>

### 11. Silica shells are found in:
  <Mcq>
    <Mcqw>1. Freshwater amoeba</Mcqw>
    <Mcqw>2. Sporozoans</Mcqw>
    <Mcqc>3. Marine amoeba</Mcqc>
    <Mcqw>4. Ciliates</Mcqw>
    <Mcqans ans="3. Marine amoeba">Reference: NCERT Page 22</Mcqans>
  </Mcq>

### 12. Most notorious sporozoan organism is:
  <Mcq>
    <Mcqw>1. Mycoplasma</Mcqw>
    <Mcqw>2. Cyanobacteria</Mcqw>
    <Mcqc>3. Plasmodium</Mcqc>
    <Mcqw>4. Mushroom</Mcqw>
    <Mcqans ans="3. Plasmodium">Reference: NCERT Page 22</Mcqans>
  </Mcq>

<GoogleAds slot="1911400781" />

### 13. Which of the following statement is correct?
  <Mcq>
    <Mcqw>1. All fungi are filamentous.</Mcqw>
    <Mcqc>2. Fusion of protoplasm is called plamogamy.</Mcqc>
    <Mcqw>3. The network of mycelium is called hyphae.</Mcqw>
    <Mcqw>4. Dikaryotic stage has ploidy (2n).</Mcqw>
    <Mcqans ans="2. Fusion of protoplasm is called plamogamy.">Reference: Options 1 and 3: Pg 22<br/>Options 2 and 4: Pg 23</Mcqans>
  </Mcq>

### 14. Mushroom and toadstools both have which character in common?
  <Mcq>
    <Mcqw>1. Edible fungi</Mcqw>
    <Mcqw>2. Ascomycetes</Mcqw>
    <Mcqw>3. Parasitic fungi</Mcqw>
    <Mcqc>4. Reproduce by fragmentation</Mcqc>
    <Mcqans ans="4. Reproduce by fragmentation">Reference: NCERT Page 24</Mcqans>
  </Mcq>

### 15. Ascomycetes are:
  <Mcq>
    <Mcqw>1. Coprophilous</Mcqw>
    <Mcqw>2. Decomposers</Mcqw>
    <Mcqw>3. Parasitic</Mcqw>
    <Mcqc>4. All of these</Mcqc>
    <Mcqans ans="4. All of these">Reference: NCERT Page 23</Mcqans>
  </Mcq>

### 16. Identify the below given fungi
![Identify the fungi](https://icdn.talentbrick.com/mcq/Question-16.jpg)
  <Mcq>
    <Mcqw>1. Mucor</Mcqw>
    <Mcqw>2. Rhizopus</Mcqw>
    <Mcqc>3. Aspergillus</Mcqc>
    <Mcqw>4. Agaricus</Mcqw>
    <Mcqans ans="3. Aspergillus">Reference: NCERT Page 23</Mcqans>
  </Mcq>

### 17. In basidiomycetes karyogamy occurs in
  <Mcq>
    <Mcqw>1. Basidiocarp</Mcqw>
    <Mcqc>2. Basidium</Mcqc>
    <Mcqw>3. Basidiospore</Mcqw>
    <Mcqw>4. Both 2 and 3</Mcqw>
    <Mcqans ans="2. Basidium">Reference: NCERT Page 24</Mcqans>
  </Mcq>

### 18. Choose the Correct for deuteromycetes:
  <Mcq>
    <Mcqw>1. Some members are decomposers or parasite while most are saprophytes.</Mcqw>
    <Mcqc>2. Some members are parasitic or saprophytes while most are decomposers.</Mcqc>
    <Mcqw>3. Some members are parasites while most of them are decomposers or saprophytes.</Mcqw>
    <Mcqw>4. Some members are saprophytes while most are parasitic and decomposers.</Mcqw>
    <Mcqans ans="2. Some members are parasitic or saprophytes while most are decomposers.">Reference: NCERT Page 24</Mcqans>
  </Mcq>

### 19. State true or false:
A. Plantae includes all eukaryotic chlorophyll containing organisms.  
B. The viruses are non cellular organisms.
  <Mcq>
    <Mcqw>1. T, F</Mcqw>
    <Mcqw>2. F, T</Mcqw>
    <Mcqc>3. T, T</Mcqc>
    <Mcqw>4. F, T</Mcqw>
    <Mcqans ans="3. T, T">Reference: NCERT Page 25, 27</Mcqans>
  </Mcq>

### 20. The name virus was given by:
  <Mcq>
    <Mcqw>1. Ivnowsky</Mcqw>
    <Mcqw>2. Pasteur</Mcqw>
    <Mcqc>3. M.W Biejerinek</Mcqc>
    <Mcqw>4. T.O. Diener</Mcqw>
    <Mcqans ans="3. M.W Biejerinek">Reference: NCERT Page 26</Mcqans>
  </Mcq>

<GoogleAds slot="1911400781" />
