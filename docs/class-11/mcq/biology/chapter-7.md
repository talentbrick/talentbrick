---
sidebar_position: 7
title: MCQ Questions Structural Organization in Animals Class 11 Chapter 7
description: Structural Organization in Animals Chapter 7 Biology Class 11, Boost your performance and score great marks in NEET and Class 11 Exam by practicing these MCQs.
image: https://icdn.talentbrick.com/mcq/class-11-bio-chapter-7.png
sidebar_label: Chapter 7
---

import Mcq, {Mcqinfo, Mcqw, Mcqc, Mcqans} from "@site/src/components/Mcq";

import LoadGoogleAds from "@site/src/components/LoadGoogleAds";

import GoogleAds from "@site/src/components/GoogleAds";

**NCERT** Biology Class 11th Ch.7 Structural Organization in Animals MCQ Practice Questions for NEET | Class 11.

Prepare these **important MCQ Questions** of Biology Class 11 Structural Organization in Animals Chapter 7, Latest questions to expected to come in **NEET | School Exams**.

<Mcqinfo/>
<LoadGoogleAds/>

### 1. Cockroach size ranges from:

  <Mcq>
    <Mcqw>1. 34 - 53 cm</Mcqw>
    <Mcqc>2. 0.6 – 7.6 cm</Mcqc>
    <Mcqw>3. 0.6 – 7.6 inches</Mcqw>
    <Mcqw>4. 34-53 inches</Mcqw>
     <Mcqans ans="2. 0.6 – 7.6 cm">Reference: NCERT Page 111</Mcqans>
  </Mcq>

### 2. Neck of cockroach is extension of:

  <Mcq>
    <Mcqw>1. Head</Mcqw>
    <Mcqc>2. Prothorax</Mcqc>
    <Mcqw>3. Abdomen</Mcqw>
    <Mcqw>4. Metathorax</Mcqw>
     <Mcqans ans="2. Prothorax">Reference: NCERT Page 112</Mcqans>
  </Mcq>

<GoogleAds slot="1911400781" />

### 3. Head of cockroach is formed by fusion of:

  <Mcq>
    <Mcqc>1. Six segments</Mcqc>
    <Mcqw>2. Seven segments</Mcqw>
    <Mcqw>3. Ten segments</Mcqw>
    <Mcqw>4. Two segments</Mcqw>
     <Mcqans ans="1. Six segments">Reference: NCERT Page 112</Mcqans>
  </Mcq>

### 4. Which of the following feature can be used to distinguish male cockroach from female cockroach?

  <Mcq>
    <Mcqw>1. Presence of anal cerci</Mcqw>
    <Mcqw>2. Absence of anal cerci</Mcqw>
    <Mcqc>3. Wings extend beyond the tip of abdomen</Mcqc>
    <Mcqw>4. Presence of arthrodial membrane</Mcqw>
     <Mcqans ans="3. Wings extend beyond the tip of abdomen">Reference: NCERT Page 111</Mcqans>
  </Mcq>

### 5. Choose the correct statement.

  <Mcq>
    <Mcqw>1. Hypopharynx is median inflexible lobe that act as tongue.</Mcqw>
    <Mcqw>2. In females abdomen has 9 segments only.</Mcqw>
    <Mcqw>3. Prothorax does not bears pair of walking legs.</Mcqw>
    <Mcqc>4. Neck shows great mobility in all directions.</Mcqc>
     <Mcqans ans="4. Neck shows great mobility in all directions.">Reference: NCERT Page 112</Mcqans>
  </Mcq>

### 6. Correct description of Tegmina is

  <Mcq>
    <Mcqw>1. Opaque, membranous, mesothoracic.</Mcqw>
    <Mcqw>2. Transparent, membranous, metathoracic.</Mcqw>
    <Mcqc>3.Opaque, Leathery, mesothoracic.</Mcqc>
    <Mcqw>4. Transparent, Leathery, metathoracic.</Mcqw>
     <Mcqans ans="3.Opaque, Leathery, mesothoracic.">Reference: NCERT Page 112</Mcqans>
  </Mcq>

### 7. How many of the following are part of female reproductive system in cockroach?

| Phallic gland, Collaterial gland, Spermathecal pores, Spermatheca, Vestibulum, Titillator. |
| ------------------------------------------------------------------------------------------ |

  <Mcq>
    <Mcqw>a. 2</Mcqw>
    <Mcqc>b. 4</Mcqc>
    <Mcqw>c. 5</Mcqw>
    <Mcqw>d. 3</Mcqw>
     <Mcqans ans="b. 4">Reference: NCERT Page 114 and 115</Mcqans>
  </Mcq>

### 8. Male bears thread like anal styles on which segment:

  <Mcq>
    <Mcqw>1. 9th terga</Mcqw>
    <Mcqw>2. 10th sterna</Mcqw>
    <Mcqw>3. 10th terga</Mcqw>
    <Mcqc>4. 9th sterna</Mcqc>
     <Mcqans ans="4. 9th sterna">Reference: NCERT Page 112</Mcqans>
  </Mcq>

<GoogleAds slot="1911400781" />

### 9. Compound eyes are situated on:

  <Mcq>
    <Mcqw>1. Ventral surface</Mcqw>
    <Mcqc>2. Dorsal surface</Mcqc>
    <Mcqw>3. Ventro-lateral surface</Mcqw>
    <Mcqw>4. Terminal surface</Mcqw>
     <Mcqans ans="2. Dorsal surface">Reference: NCERT Page 114</Mcqans>
  </Mcq>

### 10. Choose the correct statement with respect to cockroach.

  <Mcq>
    <Mcqw>a. In females 7th tergite is boat shaped.</Mcqw>
    <Mcqc>b. Blood from sinuses enter heart through ostia.</Mcqc>
    <Mcqw>c. 10 spiracles are present on lateral surface of the body.</Mcqw>
    <Mcqw>d. All of these.</Mcqw>
     <Mcqans ans="b. Blood from sinuses enter heart through ostia.">Reference: Option 1: Pg 112<br/>Options 2 and 3: Pg 113</Mcqans>
  </Mcq>

### 11. Grinding and incising region are located on which mouth part of cockroach.

  <Mcq>
    <Mcqc>1. Mandible</Mcqc>
    <Mcqw>2. Maxillae</Mcqw>
    <Mcqw>3. Hypopharynx</Mcqw>
    <Mcqw>4. Labium</Mcqw>
     <Mcqans ans="1. Mandible">Reference: NCERT Page 112</Mcqans>
  </Mcq>

### 12. Choose the incorrect statement with respect to cockroach.

  <Mcq>
    <Mcqw>1. Female cockroach has a pair of spermatheca.</Mcqw>
    <Mcqw>2. On average female lays 9-10 ootheca.</Mcqw>
    <Mcqc>3. Foregut is not lined by cuticle.</Mcqc>
    <Mcqw>4. Hindgut is broader than midgut.</Mcqw>
     <Mcqans ans="3. Foregut is not lined by cuticle.">Reference: Option 1: Pg 114<br/>Option 2: Pg 115<br/>Options 3 and 4: Pg 113</Mcqans>
  </Mcq>

### 13. Correct description regarding salivary gland in cockroach.

  <Mcq>
    <Mcqw>1. A pair present near gizzard.</Mcqw>
    <Mcqc>2. A pair present near crop.</Mcqc>
    <Mcqw>3. Two pairs present near crop.</Mcqw>
    <Mcqw>4. Two pairs present near gizzard.</Mcqw>
     <Mcqans ans="2. A pair present near crop.">Reference: NCERT Page 113 and 121</Mcqans>
  </Mcq>

### 14. Choose the correct statement for digestive system of cockroach.

  <Mcq>
    <Mcqw>1. At the junction of midgut and foregut 100-150 tubules of gastric caeca are present.</Mcqw>
    <Mcqw>2. At the junction of midgut and hindgut 6-8 tubules of gastric caeca are present.</Mcqw>
    <Mcqc>3. At the junction of midgut and hindgut 100-150 yellow filamentous tubules are present.</Mcqc>
    <Mcqw>4. At the junction of midgut and hindgut 6-8 yellow filamentous tubules are present.</Mcqw>
     <Mcqans ans="3. At the junction of midgut and hindgut 100-150 yellow filamentous tubules are present.">Reference: NCERT Page 113</Mcqans>
  </Mcq>

### 15. Blood flow in cockroach is:

  <Mcq>
    <Mcqw>1. Always from anterior to posterior.</Mcqw>
    <Mcqw>2. Always towards abdomen.</Mcqw>
    <Mcqw>3. Sometimes posterior to anterior and sometimes anterior to posterior.</Mcqw>
    <Mcqc>4. Always from posterior to anterior.</Mcqc>
     <Mcqans ans="4. Always from posterior to anterior.">Blood from sinuses enter heart through ostia and is pumped anteriorly to sinuses again.<br/>Reference: NCERT Page 113</Mcqans>
  </Mcq>

### 16. Malpighian tubules consist of:

  <Mcq>
    <Mcqw>1. Glandular cells</Mcqw>
    <Mcqw>2. Cuboidal cells</Mcqw>
    <Mcqw>3. Ciliated cells</Mcqw>
    <Mcqc>4. Both 1 and 3</Mcqc>
     <Mcqans ans="4. Both 1 and 3">Reference: NCERT Page 114</Mcqans>
  </Mcq>

### 17. In earthworm, which condition is seen to prevent self fertilizations?

  <Mcq>
    <Mcqc>1. Protandrous</Mcqc>
    <Mcqw>2. Progynous</Mcqw>
    <Mcqw>3. Different position of reproductive structures</Mcqw>
    <Mcqw>4. May be 1 or 2</Mcqw>
     <Mcqans ans="1. Protandrous">It is a protandrous animal with crossfertilisation. Fertilisation and development take place in cocoon secreted by the glands of clitellum.<br/>Reference: NCERT Page 121</Mcqans>
  </Mcq>

### 18. How many spermathecae are found in earthworm?

  <Mcq>
    <Mcqw>a. 4</Mcqw>
    <Mcqc>b. 8</Mcqc>
    <Mcqw>c. 2</Mcqw>
    <Mcqw>d. 10</Mcqw>
     <Mcqans ans="b. 8">Reference: NCERT Page 110</Mcqans>
  </Mcq>

<GoogleAds slot="1911400781" />

### 19. If head of cockroach is cut off, it will survive for as long as:

  <Mcq>
    <Mcqw>1. One minute</Mcqw>
    <Mcqw>2. One day</Mcqw>
    <Mcqc>3. One week</Mcqc>
    <Mcqw>4. One month</Mcqw>
     <Mcqans ans="3. One week">Reference: NCERT Page 114</Mcqans>
  </Mcq>

### 20. Choose the correct statement:

A. Male gonapophysis is chitinous symmetrical structure.  
B. Each ovary formed of 8 ovarioles
<Mcq>
<Mcqw>1. Both A and B are incorrect</Mcqw>
<Mcqw>2.Both A and B are correct</Mcqw>
<Mcqw>3. Only A is correct</Mcqw>
<Mcqc>4. Only B is correct</Mcqc>
 <Mcqans ans="4. Only B is correct">Reference: NCERT Page 114</Mcqans>
</Mcq>

### 21. Correct statement with respect to frog.

  <Mcq>
    <Mcqw>1.Out of sense organs eye and internal ears are cellular aggregation around nerve endings and rest are well organised structure.</Mcqw>
    <Mcqw>2. Female lays 250-300 eggs at a time.</Mcqw>
    <Mcqc>3. Midbrain is characterized by a pair of optic lobes.</Mcqc>
    <Mcqw>4. The ventricle opens into sac like conus arteriosus on dorsal side of heart.</Mcqw>
     <Mcqans ans="3. Midbrain is characterized by a pair of optic lobes.">Reference: Options 1 and 3: Pg 119<br/>Option 2: Pg 120<br/>Option 4: Pg 118</Mcqans>
  </Mcq>

### 22. In earthworm on average each cocoon produces \_\_ baby worms.

  <Mcq>
    <Mcqw>a. 20</Mcqw>
    <Mcqw>b. 2</Mcqw>
    <Mcqc>c. 4</Mcqc>
    <Mcqw>d. 9</Mcqw>
     <Mcqans ans="c. 4">Reference: NCERT Page 110</Mcqans>
  </Mcq>

### 23. Earthworm does not have:

  <Mcq>
    <Mcqw>1. Eyes</Mcqw>
    <Mcqw>2. Larval stages</Mcqw>
    <Mcqw>3. Specialized breathing devices</Mcqw>
    <Mcqc>4. All of these</Mcqc>
     <Mcqans ans="4. All of these">Reference: Options 1 and 2: Pg 110<br/>Option 3: Pg 108</Mcqans>
  </Mcq>

### 24. Choose the correct with respect to morphology of earthworm.

  <Mcq>
    <Mcqw>1. Setae are absent on first and last segment only.</Mcqw>
    <Mcqw>2. Four spermathecal aperture are present on ventro lateral side.</Mcqw>
    <Mcqc>3. Integumentary nephridia opens on body surface.</Mcqc>
    <Mcqw>4. Peristomium acts as a wedge to force open crack in soil.</Mcqw>
     <Mcqans ans="3. Integumentary nephridia opens on body surface.">Reference: Options 1 and 2: Pg 107<br/>Option 3: Pg 109<br/>Option 4: Pg 106</Mcqans>
  </Mcq>

<GoogleAds slot="1911400781" />

### 25. Choose the incorrect match for earthworm.
  <Mcq>
    <Mcqw>1. Typholosole – begins from 26th segment</Mcqw>
    <Mcqc>2. Gizzard – 5-7 segments</Mcqc>
    <Mcqw>3. Blood glands – 4,5,6 segments</Mcqw>
    <Mcqw>4. Buccal segment – 1st segment</Mcqw>
     <Mcqans ans="2. Gizzard – 5-7 segments">Reference: NCERT Page 108</Mcqans>
  </Mcq>