---
sidebar_position: 1
title: MCQ Questions The Living World Biology Chapter 1 Class 11
description: The Living World Biology MCQ Questions Class 11, These are the latest questions to expect in NEET | School Exams | Competitive Exams.
image: https://icdn.talentbrick.com/mcq/MCQ-Questions-Class-11-Biology-Chapter-1-The-Living-World-www.talentbrick.com_.png
sidebar_label: Chapter 1
---

import Mcq, {Mcqinfo, Mcqw, Mcqc, Mcqans} from "@site/src/components/Mcq";

import LoadGoogleAds from "@site/src/components/LoadGoogleAds";

import GoogleAds from "@site/src/components/GoogleAds";

**NCERT MCQ Questions** for Class 11 Biology Chapter 1 The Living World for NEET | School Exams | Class 11.

Prepare these important MCQ Questions of Class 11 Biology Chapter 1 The Living World with Answers, These are Latest questions to expect in **NEET | School Exams**.

<Mcqinfo/>
<LoadGoogleAds/>

### 1. Systematics is a \_\_ word.
  <Mcq>
    <Mcqw>1. Greek</Mcqw>
    <Mcqc>2. Latin</Mcqc>
    <Mcqw>3. German</Mcqw>
    <Mcqw>4. French</Mcqw>
    <Mcqans ans="2. Latin">Reference: NCERT Page 8</Mcqans>
  </Mcq>

<GoogleAds slot="1911400781" />

### 2. Who is known as Darwin of 20ᵗʰ century.
  <Mcq>
    <Mcqw>1. Norman Mayr</Mcqw>
    <Mcqw>2. Thomas Malthus</Mcqw>
    <Mcqc>3. Ernst Mayr</Mcqc>
    <Mcqw>4. GN Ramachandran</Mcqw>
    <Mcqans ans="3. Ernst Mayr">Reference: NCERT Page 2</Mcqans>
  </Mcq>

### 3. Choose the correct statement.
  <Mcq>
    <Mcqw>1. Photoperiod affects reproduction in plants only.</Mcqw>
    <Mcqw>2. Most obvious feature of all living organisms is growth.</Mcqw>
    <Mcqw>3. Technically complicated feature of all living organisms is metabolism.</Mcqw>
    <Mcqc>4. Emergence is feature of living organism.</Mcqc>
    <Mcqans ans="4. Emergence is feature of living organism.">Reference: First 3 options: Pg 5<br/>4th option: Pg 3</Mcqans>
  </Mcq>

### 4. Which of the following multiply by fragmentation?
  <Mcq>
    <Mcqc>1. Protonema of mosses</Mcqc>
    <Mcqw>2. Protonema of pteridophytes</Mcqw>
    <Mcqw>3. Unicellular Algae</Mcqw>
    <Mcqw>4. Amoeba</Mcqw>
    <Mcqans ans="1. Protonema of mosses">Reference: NCERT Page 4</Mcqans>
  </Mcq>

### 5. For plants principles and criteria for scientific naming is given by
  <Mcq>
    <Mcqw>1. ICNB</Mcqw>
    <Mcqc>2. ICBN</Mcqc>
    <Mcqw>3. ICZN</Mcqw>
    <Mcqw>4. Both 1 and 2</Mcqw>
    <Mcqans ans="2. ICBN">Reference: NCERT Page 6</Mcqans>
  </Mcq>

### 6. Biological name has how many components?
  <Mcq>
    <Mcqw>a. 6</Mcqw>
    <Mcqc>b. 2</Mcqc>
    <Mcqw>c. 3</Mcqw>
    <Mcqw>d. 5</Mcqw>
    <Mcqans ans="b. 2">Reference: NCERT Page 6</Mcqans>
  </Mcq>

<GoogleAds slot="1911400781" />

### 7. Find the incorrect statement.
  <Mcq>
    <Mcqw>1. Zoo enables us to learn about food habits and behaviour of animals.</Mcqw>
    <Mcqw>2. All living organisms are linked to one another by the sharing of the common genetic material, but to varying degrees.</Mcqw>
    <Mcqc>3. Primates have the property of self-consciousness.</Mcqc>
    <Mcqw>4. The herbarium sheet carry a label providing information about collector’s name.</Mcqw>
    <Mcqans ans="3. Primates have the property of self-consciousness.">Reference: Option 1: Pg 13<br/>Options 2 and 3: Pg 5<br/>Option 4: Pg 12</Mcqans>
  </Mcq>

### 8. Index to plant species found in particular area is given by
  <Mcq>
    <Mcqw>1. Catalogues</Mcqw>
    <Mcqw>2. Monographs</Mcqw>
    <Mcqw>3. Manuals</Mcqw>
    <Mcqc>4. Flora</Mcqc>
    <Mcqans ans="4. Flora">Reference: NCERT Page 14</Mcqans>
  </Mcq>

### 9. State True/False
a. All animals in a zoo are provided, as far as possible, the conditions similar to their natural habitats.  
b. The keys are based on the contrasting characters generally in a pair called couplet.
  <Mcq>
    <Mcqw>1. Only 2nd is false</Mcqw>
    <Mcqw>2. Both are false</Mcqw>
    <Mcqw>3. Only 1st is false</Mcqw>
    <Mcqc>4. Both are true</Mcqc>
    <Mcqans ans="4. Both are true">Reference: NCERT Page 13</Mcqans>
  </Mcq>

### 10. Indian botanical garden situated in **\_**
  <Mcq>
    <Mcqw>1. Delhi</Mcqw>
    <Mcqc>2. Howrah</Mcqc>
    <Mcqw>3. Lucknow</Mcqw>
    <Mcqw>4. Kew</Mcqw>
    <Mcqans ans="2. Howrah">Reference: NCERT Page 12</Mcqans>
  </Mcq>

### 11. Mango belong to which order
  <Mcq>
    <Mcqc>1. Sapindales</Mcqc>
    <Mcqw>2. Anacardiaceae</Mcqw>
    <Mcqw>3. Poales</Mcqw>
    <Mcqw>4. Polymoniales</Mcqw>
    <Mcqans ans="1. Sapindales">Reference: NCERT Page 11</Mcqans>    
  </Mcq>

### 12. Solanaceae are included in the order Polymoniales mainly based on:
  <Mcq>
    <Mcqw>1. Vegetative features</Mcqw>
    <Mcqc>2. Floral Characters</Mcqc>
    <Mcqw>3. Root Structures</Mcqw>
    <Mcqw>4. Both 1 and 2</Mcqw>
    <Mcqans ans="2. Floral Characters">Reference: NCERT Page 10</Mcqans>
  </Mcq>

### 13. No. of species have the descriped range between
  <Mcq>
    <Mcqw>1. 1 – 1.5 Million</Mcqw>
    <Mcqw>2. 2.7 – 2.8 Million</Mcqw>
    <Mcqw>3. 1.7 – 2.7 Million</Mcqw>
    <Mcqc>4. 1.7 – 1.8 Million</Mcqc>
    <Mcqans ans="4. 1.7 – 1.8 Million">Reference: NCERT Page 16</Mcqans>
  </Mcq>

### 14. Which of the following can be considered as Taxa?
  <Mcq>
    <Mcqw>1. Dog</Mcqw>
    <Mcqw>2. Wheat</Mcqw>
    <Mcqw>3. Mammals</Mcqw>
    <Mcqc>4. All of these</Mcqc>
    <Mcqans ans="4. All of these">Reference: NCERT Page 7</Mcqans>
  </Mcq>

### 15. What is the common name of Panthera pardus?
  <Mcq>
    <Mcqw>1. Cheetah</Mcqw>
    <Mcqc>2. Leopard</Mcqc>
    <Mcqw>3. Wolf</Mcqw>
    <Mcqw>4. Lion</Mcqw>
    <Mcqans ans="2. Leopard">Reference: NCERT Page 9</Mcqans> 
  </Mcq>

<GoogleAds slot="1911400781" />