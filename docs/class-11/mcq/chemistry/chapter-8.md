---
sidebar_position: 8
title: MCQ Questions Redox Reactions Inorganic Chemistry Class 11 Chapter 8
description: Redox Reactions Inorganic Chemistry Chapter 8 NCERT MCQ questions for Class 11. Latest questions to expect in Jee Mains | NEET | School Exams.
image: https://icdn.talentbrick.com/mcq/MCQ-Questions-Redox-Reactions-Inorganic-Chemistry-Class-11-Chapter-8.png
sidebar_label: Chapter 8
---

import Mcq, {Mcqinfo, Mcqw, Mcqc, Mcqans} from "@site/src/components/Mcq";
import LoadGoogleAds from "@site/src/components/LoadGoogleAds";
import GoogleAds from "@site/src/components/GoogleAds";

**NCERT** *Inorganic Chemistry* Redox Reactions Class 11 Chapter 8, **Important MCQ Questions** for Jee Mains | NEET | Class 11.

Prepare these important Questions of Redox Reactions in Inorganic Chemistry, Latest questions to expect in Jee Mains | NEET | School Exams. 

<Mcqinfo/>
<LoadGoogleAds/>
<GoogleAds slot="1911400781" />

### 1. Which of the following is correct Stock notation?
  <Mcq>
    <Mcqw>1. H(I)AuCl₃</Mcqw>
    <Mcqw>2. HAu(I)Cl₃</Mcqw>
    <Mcqw>3. HAuCl₃(III)</Mcqw>
    <Mcqc>4. HAu(III)Cl₃</Mcqc>
  </Mcq>

### 2. Indicator used in the titration of Potassium dichromate is:
  <Mcq>
    <Mcqw>1. Starch solution</Mcqw>
    <Mcqw>2. Hydrogen peroxide</Mcqw>
    <Mcqc>3. Diphenylamine</Mcqc>
    <Mcqw>4. Biphenyl</Mcqw>
  </Mcq>

### 3. Layer test is used for the detection of:
  <Mcq>
    <Mcqc>1. Bromide ion</Mcqc>
    <Mcqw>2. Sulfide ion</Mcqw>
    <Mcqw>3. Chloride ion</Mcqw>
    <Mcqw>4. Fluoride ion</Mcqw>
  </Mcq>

### 4. The substance to be used as a primary standard should satisfy which of the following requirements:
  <Mcq>
    <Mcqw>1. It should not be hygroscopic.</Mcqw>
    <Mcqw>2. It should have a high relative molecular mass.</Mcqw>
    <Mcqw>3. Its reaction with another substance should be instantaneous and stoichiometric.</Mcqw>
    <Mcqc>4. All of these.</Mcqc>
  </Mcq>

### 5. Phenolphthalein is a:
  <Mcq>
    <Mcqw>1. Weak base</Mcqw>
    <Mcqc>2. Weak acid</Mcqc>
    <Mcqw>3. Amphiprotic ion</Mcqw>
    <Mcqw>4. Strong acid</Mcqw>
  </Mcq>

### 6. ClO⁻ disproportionate to give:
  <Mcq>
    <Mcqw>1. Cl⁻ and ClO⁻ </Mcqw>
    <Mcqw>2. Cl₂ and ClO₄⁻</Mcqw>
    <Mcqw>3. Cl⁻ and ClO₂⁻ </Mcqw>
    <Mcqc>4. Cl⁻ and ClO₃⁻</Mcqc>
  </Mcq>

<GoogleAds slot="1911400781" />

### 7. In the reaction of metallic cobalt and nickel sulphate solution, the equilibrium test reveals the presence of:
  <Mcq>
    <Mcqc>a. Ni⁺² and Co⁺²</Mcqc>
    <Mcqw>b. Ni⁺² only</Mcqw>
    <Mcqw>c. Co⁺² only</Mcqw>
    <Mcqw>d. Can’t be predicted</Mcqw>
  </Mcq>

### 8. The non-metal displacement redox reaction include:
  <Mcq>
    <Mcqw>1. Hydrogen and oxygen displacement.</Mcqw>
    <Mcqw>2. Rarely hydrogen displacement, no oxygen displacement.</Mcqw>
    <Mcqc>3. Hydrogen displacement and rarely oxygen displacement.</Mcqc>
    <Mcqw>4. No oxygen displacement and rarely hydrogen displacement.</Mcqw>
  </Mcq>

### 9. AgF₂ is a:
  <Mcq>
    <Mcqw>1. Very strong reducing agent</Mcqw>
    <Mcqc>2. Very strong oxidising agent</Mcqc>
    <Mcqw>3. Weakly reducing agent</Mcqw>
    <Mcqw>4. Weakly oxidising agent</Mcqw>
  </Mcq>

### 10. Which of the following metals produce dihydrogen from acids even though they could not produce it by reaction with steam:
  <Mcq>
    <Mcqw>1. Mg</Mcqw>
    <Mcqw>2. Fe</Mcqw>
    <Mcqc>3. Cd</Mcqc>
    <Mcqw>4. Ca</Mcqw>
  </Mcq>

### 11. Methyl orange is a:
  <Mcq>
    <Mcqc>1. Weak base</Mcqc>
    <Mcqw>2. Weak acid</Mcqw>
    <Mcqw>3. Amphiprotic ion</Mcqw>
    <Mcqw>4. Strong acid</Mcqw>
  </Mcq>

### 12. Sodium hydroxide and potassium permanganate are examples of:
  <Mcq>
    <Mcqw>1. Tertiary standard solution</Mcqw>
    <Mcqw>2. They do not form standard solution</Mcqw>
    <Mcqw>3. Primary standard solution</Mcqw>
    <Mcqc>4. Secondary standard solution</Mcqc>
  </Mcq>

### 13. Iodide ion can be oxidized to iodine by:
  <Mcq>
    <Mcqw>1. Hypo solution</Mcqw>
    <Mcqc>2. CuSO₄</Mcqc>
    <Mcqw>3. Alkaline KMnO₄</Mcqw>
    <Mcqw>4. KCl</Mcqw>
  </Mcq>

### 14. Iron on reaction with steam forms:
  <Mcq>
    <Mcqc>1. Fe₂O₃</Mcqc>
    <Mcqw>2. Fe₃O₄</Mcqw>
    <Mcqw>3. FeO₄⁻²</Mcqw>
    <Mcqw>4. FeO</Mcqw>
  </Mcq>

### 15. Choose the correct statement:
  <Mcq>
    <Mcqw>1. All alkali metals except Li will displace hydrogen from cold water.</Mcqw>
    <Mcqw>2. All decomposition reactions are redox reactions.</Mcqw>
    <Mcqw>3. Rhombic sulphur ongoing disproportionation forms sulphur in -2 and +3 oxidation state.</Mcqw>
    <Mcqc>4. There is no way to convert F⁻ to F₂ by chemical means.</Mcqc>
  </Mcq>

### 16. In the reaction between metallic zinc and the copper nitrate solution, using H₂S (a very sensitive test) gas, Cu⁺²:
  <Mcq>
    <Mcqw>1. Can be detected satisfactorily</Mcqw>
    <Mcqw>2. Depend on concentration </Mcqw>
    <Mcqc>3. Cannot be detected</Mcqc>
    <Mcqw>4. Give black precipitate on reacting</Mcqw>
  </Mcq>

### 17. Rate of hydrogen gas evolution is fastest for:
  <Mcq>
    <Mcqw>1. Na</Mcqw>
    <Mcqc>2. Mg</Mcqc>
    <Mcqw>3. Ca</Mcqw>
    <Mcqw>4. Fe</Mcqw>
  </Mcq>

### 18. Thiosulphate on reaction with bromine liquid forms:
  <Mcq>
    <Mcqc>1. SO₄⁻²</Mcqc>
    <Mcqw>2. S₄O₆⁻²</Mcqw>
    <Mcqw>3. S₂O₈⁻²</Mcqw>
    <Mcqw>4. SO₃⁻²</Mcqw>
  </Mcq>