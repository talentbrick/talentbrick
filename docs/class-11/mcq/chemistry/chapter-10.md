---
sidebar_position: 10
title: MCQ Questions S Block Elements Class 11 Chemistry Chapter 10
description: S Block Elements Chemistry Chapter 10 NCERT MCQ questions for Class 11. Latest questions to expect in Jee Mains | NEET | School Exams.
image: https://icdn.talentbrick.com/mcq/MCQ-Questions-S-Block-Elements-Class-11-Chemistry-Chapter-10.png
sidebar_label: Chapter 10
---

import Mcq, {Mcqinfo, Mcqw, Mcqc, Mcqans} from "@site/src/components/Mcq";
import LoadGoogleAds from "@site/src/components/LoadGoogleAds";
import GoogleAds from "@site/src/components/GoogleAds";

MCQ Quiz s-Block Elements [Chemistry Class 11 MCQ Questions](/class-11/mcq/chemistry) for NEET | School Exams | Class 11

Prepare these important MCQ Questions of s-Block Elements, These are Latest questions to expect in Jee | NEET | School Exams.

<Mcqinfo/>
<LoadGoogleAds/>
<GoogleAds slot="1911400781" />

### 1. MgCl₂.6H₂O on strong heating gives:

  <Mcq>
    <Mcqw>1. [Mg(OH)₆]Cl₂</Mcqw>
    <Mcqc>2. MgO</Mcqc>
    <Mcqw>3. Mg(OH)₂</Mcqw>
    <Mcqw>4. MgCl₂ anhydrous</Mcqw>
  </Mcq>

### 2. With respect to abundance in earth crust rank of magnesium and calcium are respectively

  <Mcq>
    <Mcqc>a. 6,5</Mcqc>
    <Mcqw>b. 5,6</Mcqw>
    <Mcqw>c. 4,6</Mcqw>
    <Mcqw>d. 8,10</Mcqw>
  </Mcq>

<GoogleAds slot="1911400781" />

### 3. Choose the incorrect statement:

  <Mcq>
    <Mcqw>1. Monovalent ions of alkali metals are never found in free state in nature.</Mcqw>
    <Mcqw>2. Alkali metals and their salts impart characteristic colour to oxidizing flame.</Mcqw>
    <Mcqc>3. Water of crystallisation in Lithium chloride are 3.</Mcqc>
    <Mcqw>4. Alkali metal have largest size in a particular period.</Mcqw>
  </Mcq>

### 4. White metal is alloy of:

  <Mcq>
    <Mcqw>1. Na and Pb</Mcqw>
    <Mcqw>2. Li and Na</Mcqw>
    <Mcqc>3. Li and Pb</Mcqc>
    <Mcqw>4. Mg and Pb</Mcqw>
  </Mcq>

### 5. Alloy used to make anti knock additive is:

  <Mcq>
    <Mcqw>1. Li/Pb</Mcqw>
    <Mcqc>2. Na/Pb</Mcqc>
    <Mcqw>3. Mg/Pb</Mcqw>
    <Mcqw>4. Ba/Pb</Mcqw>
  </Mcq>

### 6. Choose the incorrect statement:

  <Mcq>
    <Mcqw>1. Alkali metals form salts with all oxoacids.</Mcqw>
    <Mcqw>2. The superoxide ion is stable only in presence of large cations.</Mcqw>
    <Mcqc>3. Silver (Ag) is better reducing agent than iodide ion (I⁻).</Mcqc>
    <Mcqw>4. In most cases hydrogencarbonates are highly stable to heat.</Mcqw>
  </Mcq>

### 7. Identify the correct statement with respect to lithium.

  <Mcq>
    <Mcqw>1. It forms ethynide on reaction with ethyne.</Mcqw>
    <Mcqw>2. It is most reactive among all alkali metals.</Mcqw>
    <Mcqc>3. It is strongest reducing agent.</Mcqc>
    <Mcqw>4. Its hydride is covalent in nature.</Mcqw>
  </Mcq>

### 8. Both Mg and Li are:

  <Mcq>
    <Mcqc>1. Harder and lighter than other elements in respective groups.</Mcqc>
    <Mcqw>2. Softer and lighter than other elements in respective groups.</Mcqw>
    <Mcqw>3. Harder and heavier than other elements in respective groups.</Mcqw>
    <Mcqw>4. Softer and dense than other elements in respective groups.</Mcqw>
  </Mcq>

### 9. How many of the following sodium compounds are used in textile industry?

| NaCl, NaOH, Na₂CO₃, NaHCO₃ |
| -------------------------- |


  <Mcq>
    <Mcqw>a. 4</Mcqw>
    <Mcqw>b. 1</Mcqw>
    <Mcqc>c. 2</Mcqc>
    <Mcqw>d. 3</Mcqw>
  </Mcq>

### 10. Compound of sodium used for mercerizing of cotton fabrics and for preparation of pure fat and oils.

  <Mcq>
    <Mcqw>1. NaCl</Mcqw>
    <Mcqw>2. Na₂CO₃</Mcqw>
    <Mcqw>3. NaHCO₃</Mcqw>
    <Mcqc>4. NaOH</Mcqc>
  </Mcq>

### 11. Mark the correct order of amount of elements found in human body.

  <Mcq>
    <Mcqw>1. Ca&gt;Na&gt;Mg&gt;Cu&gt;Fe</Mcqw>
    <Mcqw>2. Ca&gt;Mg&gt;Na&gt;Cu&gt;Fe</Mcqw>
    <Mcqc>3. Ca&gt;Na&gt;Mg&gt;Fe&gt;Cu</Mcqc>
    <Mcqw>4. Ca&gt;Na&gt;Fe&gt;Mg&gt;Cu</Mcqw>
  </Mcq>

### 12. Choose the incorrect statement:

  <Mcq>
    <Mcqw>1. Sodium hydroxide reacts with CO₂ in the atmosphere to form Na₂CO₃.</Mcqw>
    <Mcqw>2. Calcium and magnesium chloride are more soluble than NaCl.</Mcqw>
    <Mcqc>3. Washing soda turns to soda ash by heating at 373K.</Mcqc>
    <Mcqw>4. Beryllium and magnesium appear to be somewhat greyish.</Mcqw>
  </Mcq>

### 13. Odd one out with respect to rock salt structure.

  <Mcq>
    <Mcqw>1. MgO</Mcqw>
    <Mcqw>2. BaO</Mcqw>
    <Mcqw>3. CaO</Mcqw>
    <Mcqc>4. BeO</Mcqc>
  </Mcq>

### 14. Element used for making windows of X-ray tubes.

  <Mcq>
    <Mcqc>1. Be</Mcqc>
    <Mcqw>2. Ba</Mcqw>
    <Mcqw>3. Ca</Mcqw>
    <Mcqw>4. Mg</Mcqw>
  </Mcq>

### 15. Select the incorrect match with respect to elements and their uses.

  <Mcq>
    <Mcqw>1. Ra – Radiotherapy</Mcqw>
    <Mcqc>2. Be – Remove air from vaccum tubes</Mcqc>
    <Mcqw>3. Ca – Extraction of metals</Mcqw>
    <Mcqw>4. Mg – Incendiary bombs</Mcqw>
  </Mcq>

### 16. Correct order of solubility of nitrates.

  <Mcq>
    <Mcqc>1. Be(NO₃)₂&gt;Mg(NO₃)₂&gt;Ca(NO₃)₂&gt;Sr(NO₃)₂</Mcqc>
    <Mcqw>2. Be(NO₃)₂&lt;Mg(NO₃)₂&lt;Ca(NO₃)₂&lt;Sr(NO₃)₂</Mcqw>
    <Mcqw>3. Be(NO₃)₂&lt;Mg(NO₃)₂&gt;Ca(NO₃)₂&lt;Sr(NO&lt;₃)₂</Mcqw>
    <Mcqw>4. Be(NO₃)₂&gt;Mg(NO₃)₂&gt;Ca(NO₃)₂&lt;Sr(NO₃)₂</Mcqw>
  </Mcq>

### 17.Compound of alkaline earth metal that is/are stable to heat.

  <Mcq>
    <Mcqw>1. Nitrates</Mcqw>
    <Mcqc>2. Sulphates</Mcqc>
    <Mcqw>3. Carbonates</Mcqw>
    <Mcqw>4. All of these</Mcqw>
  </Mcq>

### 18. From solution of which metal in ammonia, [M(NH₃)₆]⁺² can be recovered.

  <Mcq>
    <Mcqw>1. Na&nbsp;</Mcqw>
    <Mcqw>2. Be</Mcqw>
    <Mcqc>3. Mg</Mcqc>
    <Mcqw>4. Li</Mcqw>
  </Mcq>

### 19. Sodium hydrogencarbonate and calcium hydroxide are respectively:

  <Mcq>
    <Mcqw>1. Disinfectant, Antiseptic</Mcqw>
    <Mcqw>2. Both Disinfectant</Mcqw>
    <Mcqc>3. Antiseptic, Disinfectant</Mcqc>
    <Mcqw>4. Both Antiseptic</Mcqw>
  </Mcq>

### 20. Choose the correct statement:

A. Sodium-potassium pump consumes more than one third of the ATP used by resting animal.  
B. CaCO₃ is extensively used in manufacture of high quality paper.

  <Mcq>
    <Mcqw>1. Both A and B are incorrect</Mcqw>
    <Mcqc>2. Both A and B are correct</Mcqc>
    <Mcqw>3. Only A is correct</Mcqw>
    <Mcqw>4. Only B is correct</Mcqw>
  </Mcq>

### 21. Which of the following is cheapest form of alkali?

  <Mcq>
    <Mcqc>1. CaO</Mcqc>
    <Mcqw>2. BaO</Mcqw>
    <Mcqw>3. NaOH</Mcqw>
    <Mcqw>4. CaCO₃</Mcqw>
  </Mcq>

### 22. Choose the correct one:

  <Mcq>
    <Mcqw>1. NaOH&gt;NaCl (melting point).</Mcqw>
    <Mcqc>2. E⁰ for M²⁺ + 2e-→M(s) for Ca, Sr, Ba is nearly constant.</Mcqc>
    <Mcqw>3. Potassium found to be more useful than sodium.</Mcqw>
    <Mcqw>4. LiCl is insoluble in water but soluble in acetone.</Mcqw>
  </Mcq>

### 23. State true or false.

A. Fire caused by saline hydride can be extinguished by CO₂.  
B. Calcium hydroxide is used for purification of sugar.  
C. CaO on exposure to atmosphere absorbs CO₂ and moisture.

  <Mcq>
    <Mcqw>1. T, F, T</Mcqw>
    <Mcqw>2. F, F, T</Mcqw>
    <Mcqc>3. F, T, T</Mcqc>
    <Mcqw>4. F, F, F</Mcqw>
  </Mcq>

### 24. Beryllium chloride has which hybridization at 1200K?

  <Mcq>
    <Mcqw>1. sp²</Mcqw>
    <Mcqc>2. sp</Mcqc>
    <Mcqw>3. sp³</Mcqw>
    <Mcqw>4. Unhybridized</Mcqw>
  </Mcq>

### 25. Upto what extent bone is solubilied and redeposited daily?

  <Mcq>
    <Mcqw>1. 30 mg</Mcqw>
    <Mcqc>2. 400 mg</Mcqc>
    <Mcqw>3. 40 gm</Mcqw>
    <Mcqw>4. 800 gm</Mcqw>
  </Mcq>

### 26. Portland cement has which of the following ingredients as major component?

  <Mcq>
    <Mcqw>1. Gypsum</Mcqw>
    <Mcqc>2. Tricalcium silicate</Mcqc>
    <Mcqw>3. Dicalcium silicate</Mcqw>
    <Mcqw>4. Tricalcium aluminate</Mcqw>
  </Mcq>

### 27. Which element’s oxide is not used in cement preparation?

  <Mcq>
    <Mcqw>1. Sulphur</Mcqw>
    <Mcqw>2. Iron</Mcqw>
    <Mcqw>3. Magnesium</Mcqw>
    <Mcqc>4. Copper</Mcqc>
  </Mcq>

### 28. The atomic size of Potassium is bigger than that of

  <Mcq>
    <Mcqw>1. Barium</Mcqw>
    <Mcqw>2. Calcium</Mcqw>
    <Mcqw>3. Strontium</Mcqw>
    <Mcqc>4. All of these</Mcqc>
  </Mcq>

### 29. Which element’s hydride is synthesised above 1000K temperature?

  <Mcq>
    <Mcqw>1. Cs</Mcqw>
    <Mcqc>2. Li</Mcqc>
    <Mcqw>3. Na</Mcqw>
    <Mcqw>4. K</Mcqw>
  </Mcq>
