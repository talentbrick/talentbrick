---
sidebar_position: 9
title: MCQ Questions Hydrogen Class 11 Inorganic Chemistry Chapter 9
description: Hydrogen Inorganic Chemistry Chapter 9 NCERT MCQ questions for Class 11. Latest questions to expect in Jee Mains | NEET | School Exams.
image: https://icdn.talentbrick.com/mcq/Hydrogen-MCQ-Talentbrick.png
sidebar_label: Chapter 9
---

import Mcq, {Mcqinfo, Mcqw, Mcqc, Mcqans} from "@site/src/components/Mcq";
import LoadGoogleAds from "@site/src/components/LoadGoogleAds";
import GoogleAds from "@site/src/components/GoogleAds";

**NCERT** *Inorganic Chemistry* Hydrogen Class 11 Chapter 9, **Important MCQ Questions** for Jee Mains | NEET | Class 11.

Prepare these important Questions of Hydrogen in Inorganic Chemistry, Latest questions to expect in Jee Mains | NEET | School Exams. 

<Mcqinfo/>
<LoadGoogleAds/>
<GoogleAds slot="1911400781" />

### 1. Hydrogen is placed in which place in the periodic table?
  <Mcq>
    <Mcqw>1. Group 18</Mcqw>
    <Mcqc>2. Separately in the Periodic Table</Mcqc>
    <Mcqw>3. Group 17</Mcqw>
    <Mcqw>4. Group 1</Mcqw>
  </Mcq>

### 2. Catalyst used in the reaction of CO with steam is
  <Mcq>
    <Mcqw>1. Copper Chromate</Mcqw>
    <Mcqw>2. Cadmium Oxide</Mcqw>
    <Mcqw>3. Nickel</Mcqw>
    <Mcqc>4. Iron Chromate</Mcqc>
  </Mcq>

### 3. Internuclear distance between H2 and D2 is:
  <Mcq>
    <Mcqw>1. More for H2</Mcqw>
    <Mcqw>2. More for D2</Mcqw>
    <Mcqc>3. Equal for both</Mcqc>
    <Mcqw>4. Vary with Temperature</Mcqw>
  </Mcq>

### 4. Arrange the following in their increasing order of bond energy.
  <Mcq>
    <Mcqw>1. H2, F2, D2</Mcqw>
    <Mcqc>2. F2, H2, D2</Mcqc>
    <Mcqw>3. D2, F2, H2</Mcqw>
    <Mcqw>4. H2, D2, F2</Mcqw>
  </Mcq>

### 5. Presently most of the industrial dihydrogen gas is prepared from __
  <Mcq>
    <Mcqw>1. Electrolysis of water</Mcqw>
    <Mcqw>2. Coal</Mcqw>
    <Mcqc>3. Petrochemicals</Mcqc>
    <Mcqw>4. Electrolysis of brine</Mcqw>
  </Mcq>

<GoogleAds slot="1911400781" />

### 6. Single Bond energy is highest for which molecule?
  <Mcq>
    <Mcqc>1. H-H Bond</Mcqc>
    <Mcqw>2. C-C Bond</Mcqw>
    <Mcqw>3. F-F Bond</Mcqw>
    <Mcqw>4. Cl-Cl Bond</Mcqw>
  </Mcq>

### 7. The ionization energy of hydrogen is similar to which of the following halogen?
  <Mcq>
    <Mcqw>1. Fluorine</Mcqw>
    <Mcqc>2. Chlorine</Mcqc>
    <Mcqw>3. Iodine</Mcqw>
    <Mcqw>4. Bromine</Mcqw>
  </Mcq>

### 8. Choose the Correct Statement:
  <Mcq>
    <Mcqc>1. H+ always exist in the associated form with other atoms or molecules</Mcqc>
    <Mcqw>2. Harold C. Urey separated deuterium by chemical method</Mcqw>
    <Mcqw>3. Deuterium is mostly formed in form of HDO</Mcqw>
    <Mcqw>4. Hydrogen is the second most abundant element on earth in its Combined State</Mcqw>
  </Mcq>

### 9. Lithium hydride reacts with aluminum chloride to give:
  <Mcq>
    <Mcqw>1. LiCl only</Mcqw>
    <Mcqw>2. Aluminium Hydride</Mcqw>
    <Mcqw>3. Lithium Aluminium Hydride</Mcqw>
    <Mcqc>4. Both 1 and 3</Mcqc>
  </Mcq>

### 10. Heavy hydrogen is:
  <Mcq>
    <Mcqw>1. Tritium</Mcqw>
    <Mcqw>2. Protium</Mcqw>
    <Mcqc>3. Deuterium</Mcqc>
    <Mcqw>4. All of the above</Mcqw>
  </Mcq>

### 11. Which of the following statement is incorrect?
  <Mcq>
    <Mcqc>1. Dihydrogen is used in the manufacture of Polyunsaturated vegetable oils</Mcqc>
    <Mcqw>2. The largest single of dihydrogen is in the manufacturing of Ammonia</Mcqw>
    <Mcqw>3. Many metals combine at high temperature with dihydrogen</Mcqw>
    <Mcqw>4. Hydroformylation of alkene leads to an increase in no. of carbon atoms in the chain</Mcqw>
  </Mcq>

### 12. Select the correct Statement:
  <Mcq>
    <Mcqw>1. A mixture of CO and H2 is used for the preparation of methanoic acid</Mcqw>
    <Mcqw>2. Methane and steam react to give carbon dioxide and hydrogen</Mcqw>
    <Mcqw>3. Isotopes of hydrogen have almost the same physical property</Mcqw>
    <Mcqc>4. Isotopes have a difference in their rate of reactions</Mcqc>
  </Mcq>

### 13. Which of the following statement is correct?<
  <Mcq>
    <Mcqw>1. All group 13 elements form electron-deficient compounds except Aluminum</Mcqw>
    <Mcqc>2. Dihydrogen does not produce any Pollution</Mcqc>
    <Mcqw>3. It produces greater energy per mole than gasoline and other fuels</Mcqw>
    <Mcqw>4. Lithium hydride reacts with Cl2 at moderate temperature to form Hydrochloric acid</Mcqw>
  </Mcq>

### 14. Which of the following makes the least contribution to the total world’s water supply?
  <Mcq>
    <Mcqc>1. River</Mcqc>
    <Mcqw>2. Lakes</Mcqw>
    <Mcqw>3. Soil moisture</Mcqw>
    <Mcqw>4. Atmospheric Water Vapor</Mcqw>
  </Mcq>

### 15. Incorrect with respect to metallic hydrides:
  <Mcq>
    <Mcqw>1. Metallic hydrides are hydrogen deficient.</Mcqw>
    <Mcqw>2. Metallic hydride retain their metallic conductivity</Mcqw>
    <Mcqw>3. Nickel and Actinium have different lattices from that of parent metal</Mcqw>
    <Mcqc>4. These hydrides conduct as efficiently as their parent metals</Mcqc>
  </Mcq>

### 16. The human Body consists of how much water percentage?
  <Mcq>
    <Mcqw>1. 85%</Mcqw>
    <Mcqw>2. 40%</Mcqw>
    <Mcqc>3. 65%</Mcqc>
    <Mcqw>4. 95%</Mcqw>
  </Mcq>

 ### 17. Due to _____ with polar molecules, even covalent compounds like alcohol and carbohydrates dissolve in water. Fill in the blank:
  <Mcq>
    <Mcqw>1. Dipole induced dipole interaction</Mcqw>
    <Mcqc>2. H-Bonding</Mcqc>
    <Mcqw>3. Van der Waal interactions</Mcqw>
    <Mcqw>4. London Force</Mcqw>
  </Mcq>

### 18. At Atmospheric pressure ice crystallizes in:
  <Mcq>
    <Mcqc>1. Hexagonal form</Mcqc>
    <Mcqw>2. Trigonal form</Mcqw>
    <Mcqw>3. Tetragonal form</Mcqw>
    <Mcqw>4. Cubic form</Mcqw>
  </Mcq>

### 19. Magnesium ion forms which ppt on treatment with washing soda and calcium hydroxide respectively?
  <Mcq>
    <Mcqw>1. In both Mg(OH)2</Mcqw>
    <Mcqw>2. In both MgCO3</Mcqw>
    <Mcqc>3. MgCO3 and Mg(OH)2</Mcqc>
    <Mcqw>4. Mg(OH)2 and MgCO3</Mcqw>
  </Mcq>

### 20. In ice crystal structure distance between two oxygen atoms is about:
  <Mcq>
    <Mcqw>1. Approximately equal to O-H bond length</Mcqw>
    <Mcqc>2. More than the double of O-H bond length</Mcqc>
    <Mcqw>3. Less than O-H bond length</Mcqw>
    <Mcqw>4. More than triple of O-H bond length</Mcqw>
  </Mcq>

### 21. Cation exchange resins contain ____ molecule.
  <Mcq>
    <Mcqw>1. Large, inorganic, water-insoluble</Mcqw>
    <Mcqw>2. Small, inorganic, water-soluble</Mcqw>
    <Mcqc>3. Large, organic, water-insoluble</Mcqc>
    <Mcqw>4. Small, organic, water-soluble</Mcqw>
  </Mcq>

### 22. A commercially marketed sample of H₂O₂ is:
  <Mcq>
    <Mcqw>1. 2 V</Mcqw>
    <Mcqc>2. 10 V</Mcqc>
    <Mcqw>3. 50 V</Mcqw>
    <Mcqw>4. 100 V</Mcqw>
  </Mcq>

### 23. Industrially Hydrogen peroxide is prepared by which method?
  <Mcq>
    <Mcqw>1. Electrolytic oxidation of acidified sulphate solutions</Mcqw>
    <Mcqw>2. Exhaustive electrolysis of water</Mcqw>
    <Mcqw>3. Acidifying barium peroxide</Mcqw>
    <Mcqc>4. Auto oxidation of 2-alkyl anthraquinols</Mcqc>
  </Mcq>

### 24. BaCl2. xH2O is which type of hydrate and x is?
  <Mcq>
    <Mcqw>1. H-bonded, 4</Mcqw>
    <Mcqc>2. Interstitial,2</Mcqc>
    <Mcqw>3. Interstitial,4</Mcqw>
    <Mcqw>4. Coordinated,2</Mcqw>
  </Mcq>

### 25. On Mass basis Dihydrogen can release about how much times more energy than petrol?
  <Mcq>
    <Mcqw>1. 4 times</Mcqw>
    <Mcqw>2. 10 times</Mcqw>
    <Mcqc>3. 3 times</Mcqc>
    <Mcqw>4. 30 times</Mcqw>
  </Mcq>

### 26. The basic Principle of hydrogen economy is:
  <Mcq>
    <Mcqw>1. Transportation of gas in liquid form</Mcqw>
    <Mcqw>2. Storage in liquid form</Mcqw>
    <Mcqw>3. Transport in liquid form</Mcqw>
    <Mcqc>4. All of these</Mcqc>
  </Mcq>

### 27. Arrange the following as per the order of energy released per gram:
  <Mcq>
    <Mcqc>1. H₂ (g) &gt; CH4 &gt; LPG &gt; Octane</Mcqc>
    <Mcqw>2. LPG &gt; CH4 &gt; Octane &gt; H₂ (g)</Mcqw>
    <Mcqw>3. LPG &gt; H₂ (g) &gt; CH4 &gt; Octane</Mcqw>
    <Mcqw>4. H₂ (g) &gt; LPG &gt; CH4 &gt; Octane</Mcqw>
  </Mcq>

### 28. Arrange in order of increasing Electrical conductance:
  <Mcq>
    <Mcqw>1. BeH₂ &gt; TiH₂ &gt; CaH₂ </Mcqw>
    <Mcqw>2. LPG &gt; CH4 &gt; Octane &gt; H₂ (g)</Mcqw>
    <Mcqw>3. CaH₂ &gt; TiH₂ &gt; BeH₂</Mcqw>
    <Mcqc>4. TiH₂ &gt; CaH₂ &gt; BeH₂</Mcqc>
  </Mcq>

### 29. H₂O₂ as an antiseptic is sold in the market as:
  <Mcq>
    <Mcqw>1. Per-carbonate</Mcqw>
    <Mcqw>2. Cephalosporin</Mcqw>
    <Mcqc>3. Perhydrol</Mcqc>
    <Mcqw>4. Sodium perborate</Mcqw>
  </Mcq>

### 30. ___ is added as Stabilizer in H₂O₂.
  <Mcq>
    <Mcqw>1. Dust</Mcqw>
    <Mcqc>2. Urea</Mcqc>
    <Mcqw>3. Zn Powder</Mcqw>
    <Mcqw>4. Carbamate</Mcqw>
  </Mcq>