---
sidebar_position: 3
title: MCQ Classification of Elements and Periodicity in Properties Class 11 Ch.3
description: Classification of Elements and Periodicity MCQ Questions Class 11, These are the latest questions to expect in NEET | JEE | School Exams | Competitive Exams.
image: https://1.bp.blogspot.com/-5ZTuFHZioOc/YIVBiyARyqI/AAAAAAAAAvs/sy-4pG_4ooUceJkIR6wBi1Rgq-gCIDBngCLcBGAsYHQ/s0/MCQ%2BClassification%2Bof%2BElements%2Band%2BPeriodicity%2Bin%2BProperties%2BClass%2B11%2BCh.3.png
sidebar_label: Chapter 3
---

import Mcq, {Mcqinfo, Mcqw, Mcqc, Mcqans} from "@site/src/components/Mcq";
import LoadGoogleAds from "@site/src/components/LoadGoogleAds";
import GoogleAds from "@site/src/components/GoogleAds";

**NCERT** Inorganic Chemistry Classification of Elements and Periodicity in properties Class 11 Chemistry MCQ Questions for *Jee Mains | NEET | Class 11 Examination*.

Prepare these **Important Questions** of Classification of Elements and Periodicity in properties in **Inorganic Chemistry**, Latest questions to expect in Jee Mains | NEET | School Exams.

<Mcqinfo/>
<LoadGoogleAds/>
<GoogleAds slot="1911400781" />

### 1. Scientist first to consider the idea of trends among properties of elements.
  <Mcq>
    <Mcqw>1. Mendeleev</Mcqw>
    <Mcqc>2. Dobereiner</Mcqc>
    <Mcqw>3. Seaborg</Mcqw>
    <Mcqw>4. Moseley</Mcqw>
  </Mcq>

### 2. Cylindrical table of elements was given by
  <Mcq>
    <Mcqw>1. Dobereiner</Mcqw>
    <Mcqw>2. Mendeleev</Mcqw>
    <Mcqw>3. Lothar Meyer</Mcqw>
    <Mcqc>4. A.E.B Chancourtois</Mcqc>
  </Mcq>

### 3. Davy medal was awarded to
  <Mcq>
    <Mcqw>1. Mendeleev</Mcqw>
    <Mcqw>2. Meyer</Mcqw>
    <Mcqc>3. Newland</Mcqc>
    <Mcqw>4. A.E.B Chancourtois</Mcqw>
  </Mcq>

### 4. Lothar Meyer plotted which of the following properties against atomic weight?
  <Mcq>
    <Mcqw>1. Melting point</Mcqw>
    <Mcqw>2. Boiling point</Mcqw>
    <Mcqw>3. Atomic volume</Mcqw>
    <Mcqc>4. All of the above</Mcqc>
  </Mcq>

### 5. Who published periodic table for the first time ?
  <Mcq>
    <Mcqw>1. Meyer</Mcqw>
    <Mcqw>2. A.E.B Chancourtois</Mcqw>
    <Mcqc>3. Mendeleev</Mcqc>
    <Mcqw>4. Newland</Mcqw>
  </Mcq>

### 6. Melting point of which of following was expected to be high?
  <Mcq>
    <Mcqw>1. Eka-aluminium</Mcqw>
    <Mcqc>2. Eka-silicon</Mcqc>
    <Mcqw>3. Eka-scandium</Mcqw>
    <Mcqw>4. Both 1 and 2</Mcqw>
  </Mcq>

<GoogleAds slot="1911400781" />

### 7. Which of the following belong to group VII?
A. Manganese  
B. Iodine  
C. Chlorine
  <Mcq>
    <Mcqw>1. Only B and C</Mcqw>
    <Mcqc>2. A, B, C</Mcqc>
    <Mcqw>3. Only C</Mcqw>
    <Mcqw>4. Only B</Mcqw>
  </Mcq>

### 8. Osmium and Platinum belong which group under Mendeleev periodic table?
  <Mcq>
    <Mcqw>1. VI</Mcqw>
    <Mcqw>2. V</Mcqw>
    <Mcqc>3. VIII</Mcqc>
    <Mcqw>4. VII</Mcqw>
  </Mcq>

### 9. Which of the following is correct according to IUPAC recommendation?
  <Mcq>
    <Mcqw>1. Zn – group IIB</Mcqw>
    <Mcqc>2. Pd – group 10</Mcqc>
    <Mcqw>3. Co – group VIA</Mcqw>
    <Mcqw>4. Ni-family 8</Mcqw>
  </Mcq>

### 10. Most widely used electronegativity scale is:
  <Mcq>
    <Mcqw>1. Mulliken Jaffe scale</Mcqw>
    <Mcqc>2. Pauling scale</Mcqc>
    <Mcqw>3. Allred Rochow scale</Mcqw>
    <Mcqw>4. Both 1 and 3</Mcqw>
  </Mcq>

### 11. Transuranium series begin from:
  <Mcq>
    <Mcqw>1. Uranium</Mcqw>
    <Mcqw>2. Rutherfordium</Mcqw>
    <Mcqc>3. Neptunium</Mcqc>
    <Mcqw>4. Lawrencium</Mcqw>
  </Mcq>

### 12. Arrange the following in increasing order of ionization energy?
  <Mcq>
    <Mcqw>1. He&lt;Li&lt;Li⁺&lt;He⁺</Mcqw>
    <Mcqc>2. Li&lt;He&lt;He⁺&lt;Li⁺</Mcqc>
    <Mcqw>3. Li&lt;Li⁺&lt;He&lt;He⁺</Mcqw>
    <Mcqw>4. Li&lt;He&lt;Li⁺&lt;He⁺</Mcqw>
  </Mcq>

### 13. Non-metals in periodic table comprise total number
  <Mcq>
    <Mcqw>1. Less than 78</Mcqw>
    <Mcqw>2. More than 78</Mcqw>
    <Mcqc>3. Less than 20</Mcqc>
    <Mcqw>4. More than 20</Mcqw>
  </Mcq>

### 14. Choose the incorrect statement.
  <Mcq>
    <Mcqc>1. Lithium is more electronegative as compared to Astatine</Mcqc>
    <Mcqw>2. Oxides of elements in center are amphoteric or neutral</Mcqw>
    <Mcqw>3. Highest positive electron gain enthalpy is of neon</Mcqw>
    <Mcqw>4. Chemical reactivity is highest at two extremes of periodic table</Mcqw>
  </Mcq>

### 15. Group having metal , non-metal, liquid as well as gas at room temperature is ___
  <Mcq>
    <Mcqw>1. Group 16</Mcqw>
    <Mcqw>2. Group 18</Mcqw>
    <Mcqw>3. Group 3</Mcqw>
    <Mcqc>4. Group 17</Mcqc>
  </Mcq>

### 16. Pitch blende is ore of ___
  <Mcq>
    <Mcqw>1. Mercury</Mcqw>
    <Mcqw>2. Cerium</Mcqw>
    <Mcqc>3. Uranium</Mcqc>
    <Mcqw>4. Neptunium</Mcqw>
  </Mcq>

### 17. The scientist who is generally credited with development of modern periodic table is
  <Mcq>
    <Mcqw>1. Moseley</Mcqw>
    <Mcqc>2. Mendeleev</Mcqc>
    <Mcqw>3. Meyer</Mcqw>
    <Mcqw>4. Newlands</Mcqw>
  </Mcq>

### 18. The symbol Uut is used for which element
  <Mcq>
    <Mcqc>1. Nihonium</Mcqc>
    <Mcqw>2. Seaborgium</Mcqw>
    <Mcqw>3. Flerovium</Mcqw>
    <Mcqw>4. Neptunium</Mcqw>
  </Mcq>

### 19. State true or false:
A. Soviets named element 104 as Kurchatovium.  
B. Metals comprise less than 78% of all known elements.
  <Mcq>
    <Mcqw>1. T T</Mcqw>
    <Mcqw>2. F T</Mcqw>
    <Mcqc>3. T F</Mcqc>
    <Mcqw>4. F T</Mcqw>
  </Mcq>