---
sidebar_position: 12
title: MCQ Organic Chemistry Some Basic Principles and Techniques Ch.12
description: Some Basic Principles and Techniques Organic Chemistry Chapter 12 NCERT MCQ questions for Class 11. Latest questions to expect in Jee Mains | NEET | School Exams.
image: https://icdn.talentbrick.com/mcq/MCQ-Organic-Chemistry-Some-Basic-Principles-and-Techniques-Ch.12.png
sidebar_label: Chapter 12
---

import Mcq, {Mcqinfo, Mcqw, Mcqc, Mcqans} from "@site/src/components/Mcq";
import LoadGoogleAds from "@site/src/components/LoadGoogleAds";
import GoogleAds from "@site/src/components/GoogleAds";

**NCERT** Chemistry Class 11 MCQ Practice Questions Organic Chemistry Some Basic Principles and Techniques Chapter 12 for JEE | NEET | Class 11.

Prepare these **important** [MCQ Questions of Chemistry Class 11](/class-11/mcq/chemistry) Some Basic Principles and Techniques in Chemistry, Latest questions to expect in NEET | JEE | School Exams.

<Mcqinfo/>
<LoadGoogleAds/>
<GoogleAds slot="1911400781" />

### 1. Incorrect match among the following.

  <Mcq>
    <Mcqw>1. Kolbe : Acetic acid</Mcqw>
    <Mcqc>2. Berzelius : Benzene</Mcqc>
    <Mcqw>3. Berthelot : Methane</Mcqw>
    <Mcqw>4. Wohler : Urea</Mcqw>
  </Mcq>

### 2. Identify a and b in the given diagram:

![Identify a](https://icdn.talentbrick.com/mcq/Qchem-a.png) ![Identify b](https://icdn.talentbrick.com/mcq/Qchem-b.png)

  <Mcq>
    <Mcqw>1. Both A and B are aromatic</Mcqw>
    <Mcqc>2. A is aromatic and B is non aromatic</Mcqc>
    <Mcqw>3. Both A and B are non aromatic</Mcqw>
    <Mcqw>4. A is anti-aromatic and B is aromatic</Mcqw>
  </Mcq>

### 3. Molecular formula for Icosane is:

  <Mcq>
    <Mcqw>1. C₁₁H₂₄</Mcqw>
    <Mcqw>2. C₃₀H₆₂</Mcqw>
    <Mcqw>3. C₁₃H₂₈</Mcqw>
    <Mcqc>4. C₂₀H₄₂</Mcqc>
  </Mcq>

### 4. State true or false

A. Unsaturated hydrocarbons contain at least one carbon carbon double bond or triple bond.  
B. Nitrometer is used in Kjeldahl’s method.  
C. Percentage of phosphorous using ammonium phophomolybdate is given by 31 x m₁ x 100 %/1877 x m.

  <Mcq>
    <Mcqw>1. F, F, T</Mcqw>
    <Mcqc>2. T, F, T</Mcqc>
    <Mcqw>3. T, T, T</Mcqw>
    <Mcqw>4. F, T, F</Mcqw>
  </Mcq>

### 5. Me₂S when undergoes heterolytic cleavage forms:

  <Mcq>
    <Mcqw>1. Carbonium ion</Mcqw>
    <Mcqw>2. Carbanion</Mcqw>
    <Mcqw>3. Carbocation</Mcqw>
    <Mcqc>4. Both 1 and 3</Mcqc>
  </Mcq>

### 6. Which effect is also called as Polarisability effect?

  <Mcq>
    <Mcqw>1. Inductive effect</Mcqw>
    <Mcqc>2. Electromeric effect</Mcqc>
    <Mcqw>3. Resonance effect</Mcqw>
    <Mcqw>4. Hyperconjugation effect</Mcqw>
  </Mcq>

### 7. Which of the following compounds have all C-C bond length equal?

A. Phenol  
B. Benzene  
C. Toluene  
D. Para ethoxy phenol

  <Mcq>
    <Mcqc>1. Only B</Mcqc>
    <Mcqw>2. A, B, C only</Mcqw>
    <Mcqw>3. A,B,C,D</Mcqw>
    <Mcqw>4. Only A and B</Mcqw>
  </Mcq>

### 8. CuSO₄ in Kjeldahl method acts as:

  <Mcq>
    <Mcqw>1. Oxidising agent</Mcqw>
    <Mcqw>2. Reducing agent</Mcqw>
    <Mcqc>3. Catalyst</Mcqc>
    <Mcqw>4. Hydrolysis agent</Mcqw>
  </Mcq>

<GoogleAds slot="1911400781" />

### 9. Which of the following groups have enhanced acidic or basic character due to identical resonating structures?

  <Mcq>
    <Mcqw>1. Carboxylic acid</Mcqw>
    <Mcqw>2. Sulphonic acids</Mcqw>
    <Mcqw>3. Guanidine</Mcqw>
    <Mcqc>4. All of the above</Mcqc>
  </Mcq>

### 10. Which of following is temporary effect?

  <Mcq>
    <Mcqc>1. Electromeric effect</Mcqc>
    <Mcqw>2. Resonance</Mcqw>
    <Mcqw>3. Hyperconjugation</Mcqw>
    <Mcqw>4. Both 1 and 3</Mcqw>
  </Mcq>

### 11. The best and latest technique for isolation and separation of organic compounds is:

  <Mcq>
    <Mcqw>1. Crystallization</Mcqw>
    <Mcqw>2. Sublimation</Mcqw>
    <Mcqc>3. Chromatography</Mcqc>
    <Mcqw>4. Fractional distillation</Mcqw>
  </Mcq>

### 12. Coordination number of blood red complex formed due to Thiocyanate ion is

  <Mcq>
    <Mcqw>a. 2</Mcqw>
    <Mcqw>b. 1</Mcqw>
    <Mcqw>c. 4</Mcqw>
    <Mcqc>d. 6</Mcqc>
  </Mcq>

### 13. Which of the following method is used for separation of glycerol from spent- lye?

  <Mcq>
    <Mcqw>1. Steam distillation</Mcqw>
    <Mcqw>2. Distillation under reduced pressure</Mcqw>
    <Mcqw>3. Fractional distillation</Mcqw>
    <Mcqc>4. Both 2 and 3</Mcqc>
  </Mcq>

### 14. Commonly used adsorbent/s used for chromatography are

  <Mcq>
    <Mcqw>1. Acetonitrile</Mcqw>
    <Mcqw>2. Alumina</Mcqw>
    <Mcqw>3. Silica gel</Mcqw>
    <Mcqc>4. Both 2 and 3</Mcqc>
  </Mcq>

### 15. Kjeldahl method is not applicable for

  <Mcq>
    <Mcqw>1. Pyridine</Mcqw>
    <Mcqw>2. Methylamine</Mcqw>
    <Mcqw>3. Quinoline</Mcqw>
    <Mcqc>4. Both 1 and 3</Mcqc>
  </Mcq>

### 16. AgF is not detected by Lassaigne’s test beacuse:

  <Mcq>
    <Mcqw>1. Fluoride gets oxidised to form fluorine gas.</Mcqw>
    <Mcqw>2. Fluoride ion forms precipitate with sodium extract.</Mcqw>
    <Mcqc>3. AgF is soluble in water.</Mcqc>
    <Mcqw>4. AgF is not stable and hence not formed.</Mcqw>
  </Mcq>

### 17. CHN elemental analyzer can detect small quantities upto:

  <Mcq>
    <Mcqc>a. 1-3 mg</Mcqc>
    <Mcqw>b. 10-30 mg</Mcqw>
    <Mcqw>c. 1-3 gm</Mcqw>
    <Mcqw>d. 0.1-0.3 mg</Mcqw>
  </Mcq>

### 18. Percentage of oxygen in organic compound is usually found by

  <Mcq>
    <Mcqc>1. Difference between the total percentage composition and sum of all percentages of all other elements.</Mcqc>
    <Mcqw>2. By converting oxygen into CO and then estimating CO using iodine pentoxide.</Mcqw>
    <Mcqw>3. By converting oxygen into iodine pentoxide and then estimating iodine pentoxide using CO.</Mcqw>
    <Mcqw>4. Using coke to directly convert oxygen into CO₂.</Mcqw>
  </Mcq>

### 19. Which acid can be used for acidification of sodium extract for testing sulphur by lead acetate test?

  <Mcq>
    <Mcqw>1. Sulphuric acid</Mcqw>
    <Mcqw>2. Nitric acid</Mcqw>
    <Mcqc>3. Acetic acid</Mcqc>
    <Mcqw>4. All of these</Mcqw>
  </Mcq>

### 20. Nitric acid converts cyanide and sulphide ions into which gases during test for halogens?

  <Mcq>
    <Mcqw>1. CO₂ and H₂S</Mcqw>
    <Mcqc>2. HCN and H₂S</Mcqc>
    <Mcqw>3. CO₂ and SO₂</Mcqw>
    <Mcqw>4. CO and SO₂</Mcqw>
  </Mcq>

### 21. Choose the odd one wrt constitutional isomers.

   <Mcq>
    <Mcqw>1. Position isomers</Mcqw>
    <Mcqw>2. Metamerism</Mcqw>
    <Mcqc>3. Geometric isomerism</Mcqc>
    <Mcqw>4. Chain isomers</Mcqw>
  </Mcq>

### 22. Nitric acid converts cyanide and sulphide ions into which gases during test for halogens?

  <Mcq>
    <Mcqw>1. More than 388K</Mcqw>
    <Mcqc>2. Less than 373 K</Mcqc>
    <Mcqw>3. More than 373 K</Mcqw>
    <Mcqw>4. Less than 388 but more than 373 K</Mcqw>
  </Mcq>
