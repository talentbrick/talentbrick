---
title: Class 11 Did You Know Facts | NCERT Bites
description: Revise NCERT concepts in a minute with Did You Know facts, It's very useful for competitive exam preparation like NEET JEE.
hide_table_of_contents: true
image: https://icdn.talentbrick.com/main/dyk.png
---
<head>
  <link rel="stylesheet" href="/assets/DidYouKnow.css" />
</head>

They say NCERT is THE book for NEET preparation. But how do you remember the great number of lines with their respective contexts for your exam? Well, we have you covered with NCERT Bites! 

Here we provide you with lines from your very own books which you might have missed, along with the pagewise references so you can go read them up if that's what you want.

import DocCardList from '@theme/DocCardList';
import {useCurrentSidebarCategory} from '@docusaurus/theme-common';

<DocCardList items={useCurrentSidebarCategory().items}/>