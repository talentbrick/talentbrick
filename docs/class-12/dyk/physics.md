---
title: Class 12 Physics Did You Know Facts
sidebar_label: Physics
description: Revise Class 12 Physics NCERT concepts in a minute with Did You Know facts, It's very useful for competitive exam preparation like NEET JEE.
hide_table_of_contents: true
image: https://icdn.talentbrick.com/main/dyk.png
---
<head>
  <link rel="stylesheet" href="/assets/DidYouKnow.css" />
</head>

Revise Class 12 Physics NCERT concepts in a minute with Did You Know facts, It's very useful for competitive exam preparation like NEET JEE.

import DidYouKnow from '@site/src/components/DidYouKnow';

<DidYouKnow api="https://icdn-down.talentbrick.com/dyk-apis/class12/phy.json"/>