---
sidebar_position: 14
title: MCQ Questions Biomolecules Organic Chemistry Chapter 14 Class 12
description: Biomolecules Chapter 14 NCERT MCQ questions for Class 12. Latest questions to expect in Jee Mains | NEET | School Exams.
image: https://icdn.talentbrick.com/mcq/class-12-chem-chapter-14.png
sidebar_label: Chapter 14
---

import Mcq, {Mcqinfo, Mcqw, Mcqc, Mcqans} from "@site/src/components/Mcq";

NCERT Organic Chemistry Class 12 MCQ Practice Questions for Jee Mains | NEET | School Exams.

Prepare these important MCQ Questions of Biomolecules in organic chemistry, Latest questions to expect in Jee Mains | NEET | School Exams.

<Mcqinfo/>

### 1. Most common oligosaccharides are:
  <Mcq>
    <Mcqw>1. Trisaccharides</Mcqw>
    <Mcqw>2. Monosaccharides</Mcqw>
    <Mcqc>3. Disaccharides</Mcqc>
    <Mcqw>4. Tetrasaccharides</Mcqw>
  </Mcq>

### 2. Find incorrect Statement.
  <Mcq>
    <Mcqw>1. Reducing sugar, reduces Fehling’s reagent as well as Tollen’s reagent.</Mcqw>
    <Mcqw>2. The most common sugar, used in our homes is sucrose.</Mcqw>
    <Mcqw>3. Glucose occurs in free as well as combined state in nature.</Mcqw>
    <Mcqc>4. Ribose is five carbon ketose sugar.</Mcqc>
  </Mcq>

### 3. Pyran and Pyrone are respectively:
  <Mcq>
    <Mcqw>1. Both Aromatic</Mcqw>
    <Mcqc>2. Non-aromatic and Aromatic</Mcqc>
    <Mcqw>3. Antiaromatic</Mcqw>
    <Mcqw>4. Aromatic and Non-aromatic</Mcqw>
  </Mcq>

### 4. Most commonly encountered carbohydrates in nature:
  <Mcq>
    <Mcqw>1. Monosaccharides</Mcqw>
    <Mcqw>2. Disaccharides</Mcqw>
    <Mcqc>3. Polysaccharides</Mcqc>
    <Mcqw>4. Trisaccharides</Mcqw>
  </Mcq>

### 5. Find the incorrect statement regarding proteins:
  <Mcq>
    <Mcqw>1. They occur in every part of our body.</Mcqw>
    <Mcqw>2. They are required for growth of body.</Mcqw>
    <Mcqw>3. All proteins are polymers of α-amino acids.</Mcqw>
    <Mcqc>4. Word ‘Protein’ is derived from latin word, ‘proteios’.</Mcqc>
  </Mcq>

### 6. State True or False (T/F):
a. Protein found in biological system with a unique 3-D structure is called native protein.  
b. Almost all enzymes are globular proteins.  
c. All amino-acids have L-configurations.
  <Mcq>
    <Mcqc>1. TTF</Mcqc>
    <Mcqw>2. FTF</Mcqw>
    <Mcqw>3. TTT</Mcqw>
    <Mcqw>4. TFT</Mcqw>
  </Mcq>

### 7. Vitamins are considered essential food factors because:
  <Mcq>
    <Mcqc>1. Our body cannot synthesize most of vitamins.</Mcqc>
    <Mcqw>2. They are vital amines which are not synthesized in our body.</Mcqw>
    <Mcqw>3. They are required by bacteria of our gut.</Mcqw>
    <Mcqw>4. Both (1) &amp; (2)</Mcqw>
  </Mcq>

### 8. Increased fragility of RBC’s is due to deficiency of which Vitamin:
  <Mcq>
    <Mcqw>a. Vit B₁₂</Mcqw>
    <Mcqc>b. Vit E</Mcqc>
    <Mcqw>c. Vit B₉</Mcqw>
    <Mcqw>d. Vit C</Mcqw>
  </Mcq>

### 9. Which of the following doesn’t form Zwitterion.
  <Mcq>
    <Mcqw>1. Sulphanilic acid</Mcqw>
    <Mcqw>2. Glutamic acid</Mcqw>
    <Mcqc>3. P-amino benzoic acid</Mcqc>
    <Mcqw>4. Glycine</Mcqw>
  </Mcq>

### 10. Read the following statements & choose the correct answer:
a. DNA fingerprinting is preffered over conventional fingerprinting.  
b. DNA is same for every cell and cannot be altered by any known treatment.
  <Mcq>
    <Mcqc>1. Both (A) &amp; (B) are correct and (B) is correct reason of (A).</Mcqc>
    <Mcqw>2. Both (A) &amp; (B) are correct and (B) is not correct reason of (A).</Mcqw>
    <Mcqw>3. Only A is correct.</Mcqw>
    <Mcqw>4. Both (A) &amp; (B) are incorrect.</Mcqw>
  </Mcq>

### 11. Select the incorrect match:
  <Mcq>
    <Mcqw>1. Glycine : Sweet taste.</Mcqw>
    <Mcqw>2. Lysine : 6 – Carbon Amino acid.</Mcqw>
    <Mcqw>3. Aspartic acid : Most acidic amino acid.</Mcqw>
    <Mcqc>4. Glutamine : Represented by ‘E’.</Mcqc>
  </Mcq>

### 12. Histidine consists of:
  <Mcq>
    <Mcqw>1. Indole ring</Mcqw>
    <Mcqw>2. Furan ring</Mcqw>
    <Mcqc>3. Imidiazole ring</Mcqc>
    <Mcqw>4. Pyran ring</Mcqw>
  </Mcq>

### 13. Positive Ceric Ammonium nitrate test is given by:
  <Mcq>
    <Mcqw>1. Serine</Mcqw>
    <Mcqw>2. Methionine</Mcqw>
    <Mcqw>3. Threonine</Mcqw>
    <Mcqc>4. Both (1) &amp; (3)</Mcqc>
  </Mcq>

### 14. Most basic amino acid is:
  <Mcq>
    <Mcqw>1. Ornithine</Mcqw>
    <Mcqw>2. Lysine</Mcqw>
    <Mcqc>3. Arginine</Mcqc>
    <Mcqw>4. Histidine</Mcqw>
  </Mcq>

### 15. Major form of glucose in solution form is:
  <Mcq>
    <Mcqc>1. β – glucose.</Mcqc>
    <Mcqw>2. α – glucose.</Mcqw>
    <Mcqw>3. Open Chain structure.</Mcqw>
    <Mcqw>4. Both α &amp; β in equal proportion.</Mcqw>
  </Mcq>

### 16. Choose the correct difference between Saccharic acid and Saccharin.
  <Mcq>
    <Mcqw>1. Both contain equal no. of carbon atoms.</Mcqw>
    <Mcqw>2. Both are acyclic compounds.</Mcqw>
    <Mcqw>3. Both are derived from D – glucose.</Mcqw>
    <Mcqc>4. Both of them have at least 2 oxygen atoms.</Mcqc>
  </Mcq>

### 17. Rhamnose, a carbohydrate has formula:
  <Mcq>
    <Mcqw>1. C₆H₁₀O₆</Mcqw>
    <Mcqw>2. C₆H₁₂O₆</Mcqw>
    <Mcqc>3. C₆H₁₂O₅</Mcqc>
    <Mcqw>4. C₅H₁₂O₆</Mcqw>
  </Mcq>

### 18. What is the product P in the reaction:
[![](https://1.bp.blogspot.com/-W9z-VjkRGko/X4Acja8KEfI/AAAAAAAAAYU/zC6Ex6xsEwMuwcuBSTAqzarSHQGotI4bQCLcBGAsYHQ/s320/Organic%2BChemistry%2BBiomolecules%2BQ18.png)](https://1.bp.blogspot.com/-W9z-VjkRGko/X4Acja8KEfI/AAAAAAAAAYU/zC6Ex6xsEwMuwcuBSTAqzarSHQGotI4bQCLcBGAsYHQ/Organic%2BChemistry%2BBiomolecules%2BQ18.png)
  <Mcq>
    <Mcqw>1. Xanthine</Mcqw>
    <Mcqw>2. Thymine</Mcqw>
    <Mcqw>3. Cytosine</Mcqw>
    <Mcqc>4. Uracil</Mcqc>
  </Mcq>

### 19. Which of the following sugar is found in RNA.
  <Mcq>
    <Mcqw>1. α – Ribose</Mcqw>
    <Mcqc>2. β – Ribose</Mcqc>
    <Mcqw>3. α – Deoxyribose</Mcqw>
    <Mcqw>4. Openchain Ribose</Mcqw>
  </Mcq>

### 20. How many of the following amino-acids have more than one nitrogen atom?
  <Mcq>
<table><tbody><tr><td>Alanine, Histidine, Proline, Tyrosine, Tryptophan, Lysine.</td></tr></tbody></table>
    <Mcqw>a. 2</Mcqw>
    <Mcqw>b. 5</Mcqw>
    <Mcqw>c. 4</Mcqw>
    <Mcqc>d. 3</Mcqc>
  </Mcq>