---
sidebar_position: 7
title: MCQ Questions P Block Elements Inorganic Chemistry Chapter 7 Class 12
description: P Block Elements Inorganic Chemistry | NCERT Physics MCQ questions for Class 12. Latest questions to expect in Jee Mains | NEET | School Exams.
image: https://icdn.talentbrick.com/mcq/class-12-chem-chapter-7.png
sidebar_label: Chapter 7
---

import Mcq, {Mcqinfo, Mcqw, Mcqc, Mcqans} from "@site/src/components/Mcq";

NCERT Inorganic Chemistry Class – 12 MCQ Practice Questions for Jee Mains | NEET | Class 12

Prepare these Important MCQ Questions of Chapter 7 p-Block Elements in Inorganic Chemistry, Latest questions to expect in Jee Mains | NEET | School Exams.

<Mcqinfo/>

### 1. N-N bond is weaker than P-P bond due to repulsion of __
  <Mcq>
    <Mcqw>1. Anti bonding electrons</Mcqw>
    <Mcqc>2. Non bonding electrons</Mcqc>
    <Mcqw>3. Bonding electrons</Mcqw>
    <Mcqw>4. None</Mcqw>
  </Mcq>

### 2. Which among the following has smallest ionic radius?
  <Mcq>
    <Mcqw>1. P</Mcqw>
    <Mcqw>2. As</Mcqw>
    <Mcqw>3. N</Mcqw>
    <Mcqc>4. Sb</Mcqc>
  </Mcq>

### 3. At very high temperature dinitrogen combines with dioxygen to form __
  <Mcq>
    <Mcqw>1. NO₂</Mcqw>
    <Mcqw>2. N₂O₅</Mcqw>
    <Mcqc>3. NO</Mcqc>
    <Mcqw>4. N₂O₄</Mcqw>
  </Mcq>

### 4. Which of the following is ionic fluoride?
  <Mcq>
    <Mcqw>1. SbF₃</Mcqw>
    <Mcqw>2. AsF₃</Mcqw>
    <Mcqc>3. BiF₃</Mcqc>
    <Mcqw>4. Both 1 and 3</Mcqw>
  </Mcq>

### 5. How many group 15 hydrides have enthalpy of formation negative?
  <Mcq>
    <Mcqw>a. 2</Mcqw>
    <Mcqw>b. 3</Mcqw>
    <Mcqc>c. 1</Mcqc>
    <Mcqw>d. 0</Mcqw>
  </Mcq>

### 6. Order of third ionisation of group 15 elements is
  <Mcq>
    <Mcqc>1. N &gt; P &gt; As &gt; Bi &gt; Sb</Mcqc>
    <Mcqw>2. N &gt; P &gt; As &gt; Sb &gt; Bi</Mcqw>
    <Mcqw>3. N &gt; As &gt; Sb &gt; Bi &gt; P</Mcqw>
    <Mcqw>4. P &gt; As &gt; Sb &gt; Bi &gt; N</Mcqw>
  </Mcq>

### 7. The correct order of melting point is:
  <Mcq>
    <Mcqw>1. Water &gt; Hydrogen Fluoride &gt; Ammonia</Mcqw>
    <Mcqc>2. Water &gt; Ammonia &gt; Hydrogen Fluoride</Mcqc>
    <Mcqw>3. Hydrogen Fluoride &gt; Water &gt; Ammonia</Mcqw>
    <Mcqw>4. Ammonia &gt; Water &gt; Hydrogen Fluoride</Mcqw>
  </Mcq>

### 8. The Maximum covalency of oxygen can be __
  <Mcq>
    <Mcqw>a. 2</Mcqw>
    <Mcqw>b. 3</Mcqw>
    <Mcqc>c. 4</Mcqc>
    <Mcqw>d. 1</Mcqw>
  </Mcq>

### 9. Choose the correct statement among the following:
  <Mcq>
    <Mcqw>1. All group 16 elements except oxygen show allotropy.</Mcqw>
    <Mcqw>2. All group 15 elements show allotropy.</Mcqw>
    <Mcqw>3. All halogens except Astatine are colored. </Mcqw>
    <Mcqc>4. All group 15 elements are polymorphic in nature.</Mcqc>
  </Mcq>

### 10. Dinitrogen pentoxide is prepared by treating:
  <Mcq>
    <Mcqw>1. Nitric acid with NO.</Mcqw>
    <Mcqw>2. Thermal decomposition of Lead nitrate.</Mcqw>
    <Mcqc>3. Nitric acid with P4O10.</Mcqc>
    <Mcqw>4. All of the above</Mcqw>
  </Mcq>

### 11. Which of the following acid is used in manufacture of glusose from corn starch?
  <Mcq>
    <Mcqw>1. HNO₃</Mcqw>
    <Mcqc>2. HCl</Mcqc>
    <Mcqw>3. H₂SO₄</Mcqw>
    <Mcqw>4. H₃PO₄</Mcqw>
  </Mcq>

### 12. Select the Correct Statement:
  <Mcq>
    <Mcqc>1. Mustard gas have 4 carbon atoms present in its structure.</Mcqc>
    <Mcqw>2. Platinum forms coordination number 4 compound, when dissolved in aqua regia.</Mcqw>
    <Mcqw>3. At high temperature(823K) NaCl reacts with sulphuric acid to form sodium hydrogen sulphate. </Mcqw>
    <Mcqw>4. All of these.</Mcqw>
  </Mcq>

### 13. Which among the following property of fluorine is less than expected?
  <Mcq>
    <Mcqw>1. Ionisation Enthalpy</Mcqw>
    <Mcqc>2. Melting point and Boiling point</Mcqc>
    <Mcqw>3. Melting point and Electronegativity</Mcqw>
    <Mcqw>4. Covalent radius and Electrode potential.</Mcqw>
  </Mcq>

### 14. S8 reacts with chlorine to form X. Which of the following is correct for X.
  <Mcq>
    <Mcqw>1. It has distorted tetrahedron shape.</Mcqw>
    <Mcqc>2. Sulphur has +1 oxidation state in X.</Mcqc>
    <Mcqw>3. X is cyclic structure similar to benzene.</Mcqw>
    <Mcqw>4. There is no S-S bond present in X.</Mcqw>
  </Mcq>

### 15. Bromine reacts with excess flourine to form compound Y. Y has shape of _
  <Mcq>
    <Mcqw>1. Square Planar</Mcqw>
    <Mcqw>2. Bent T</Mcqw>
    <Mcqc>3. Square Pyramidal</Mcqc>
    <Mcqw>4. Pentagonal Bipyramidal</Mcqw>
  </Mcq>

### 16. Arrange the following in order of their electron gain enthalpy.
  <Mcq>
    <Mcqw>1. He &gt; Ne &gt; Ar &gt; Kr &gt; Xe &gt; Rn</Mcqw>
    <Mcqw>2. He &gt; Kr &gt; Xe &gt; Ne &gt; Ar &gt; Rn</Mcqw>
    <Mcqw>3. Ne &gt; Ar &gt; Kr &gt; Xe &gt; He &gt; Rn</Mcqw>
    <Mcqc>4. Ne &gt; Ar &gt; Kr &gt; Xe &gt; Rn &gt; He</Mcqc>
  </Mcq>

### 17. Choose the correct statements.
  <Mcq>
    <Mcqw>1. HCl is used for extracting glue from bones.</Mcqw>
    <Mcqw>2. Oxygen is used in manufacture of many metals, particularly steel.</Mcqw>
    <Mcqw>3. Potassium Permanganate is used in production of ozone.</Mcqw>
    <Mcqc>4. Both 1 and 2.</Mcqc>
  </Mcq>

### 18. Iodine + excess chlorine reacts to give compound __
  <Mcq>
    <Mcqc>1. Orange Solid</Mcqc>
    <Mcqw>2. Black Solid </Mcqw>
    <Mcqw>3. Colorless Gas</Mcqw>
    <Mcqw>4. Ruby Red Solid</Mcqw>
  </Mcq>

### 19. Which one is most abundant noble gas in atmosphere?
  <Mcq>
    <Mcqw>1. Ne</Mcqw>
    <Mcqw>2. He</Mcqw>
    <Mcqc>3. Ar</Mcqc>
    <Mcqw>4. Kr</Mcqw>
  </Mcq>

### 20. What is color of O₂PtF₆ and XePtF₆ respectively?
  <Mcq>
    <Mcqw>1. Red, Blue</Mcqw>
    <Mcqc>2. Both Red</Mcqc>
    <Mcqw>3. Red, Colorless</Mcqw>
    <Mcqw>4. Colorless, Red</Mcqw>
  </Mcq>

### 21. Choose the odd one out with respect to state of compound (solid, liquid, gas)
  <Mcq>
    <Mcqw>1. XeF₂</Mcqw>
    <Mcqw>2. XeO₃</Mcqw>
    <Mcqc>3. XeOF₄</Mcqc>
    <Mcqw>4. XeF₆</Mcqw>
  </Mcq>

### 22. A sequence of reactions of phosphorous (P₄) is given below, the correct set of products (Q, R, S and T) among the following is
![](https://icdn.talentbrick.com/mcq/p-Block-MCQ-q22.png)
  <Mcq>
    <Mcqw>1. Q = PCl₃; R = POCl₃; S = P₂O₃; T = H₃PO₃</Mcqw>
    <Mcqw>2. Q = PCl₅; R = P₂O₅; S = P₄O₆; T = H₃PO₃</Mcqw>
    <Mcqc>3. Q = PCl₃; R = POCl₃; S = P₄O₁₀; T = H₃PO₄</Mcqc>
    <Mcqw>4. Q = PCl₅; R = P₄O₁₀; S = P₄O₁₀; T = H₃PO₄</Mcqw>
  </Mcq>

### 23. Choose the correct statement.
  <Mcq>
    <Mcqc>1. All group 16 elements form hydrides of type H₂E.</Mcqc>
    <Mcqw>2. All hexafluorides of group 16 elements are in liquid state.</Mcqw>
    <Mcqw>3. DIchloride of selenium disproportionate to form compounds of +6 and -2 oxidation state. </Mcqw>
    <Mcqw>4. SeO₂ is found in liquid state.</Mcqw>
  </Mcq>

### 24. Which of the following metals occurs in nature s decay product of uranium and thorium?
  <Mcq>
    <Mcqw>1. Livermorium</Mcqw>
    <Mcqc>2. Polonium</Mcqc>
    <Mcqw>3. Moscovium</Mcqw>
    <Mcqw>4. Oganesson</Mcqw>
  </Mcq>

### 25. Bond angle in ozone is:
  <Mcq>
    <Mcqw>a. 135</Mcqw>
    <Mcqw>b. 120</Mcqw>
    <Mcqw>c. 107</Mcqw>
    <Mcqc>d. 117</Mcqc>
  </Mcq>

### 26. Cyclo-S6 ring adopts which form
  <Mcq>
    <Mcqw>1. Half chair form</Mcqw>
    <Mcqw>2. Twisted form</Mcqw>
    <Mcqw>3. Boat form</Mcqw>
    <Mcqc>4. Chair form</Mcqc>
  </Mcq>

### 27. Choose the incorrect statement.
  <Mcq>
    <Mcqc>1. Bond length in cyclo-S6 form is less than that of S8.</Mcqc>
    <Mcqw>2. Monoclinic sulhphur has higher MP than that of rhombic sulphur.</Mcqw>
    <Mcqw>3. Monoclinic sulhur has lower specific gravity than that of rhombic sulphur.</Mcqw>
    <Mcqw>4. Bond angle is greater in S8 than in cyclo-S6.</Mcqw>
  </Mcq>

### 28. Chalcogen is greek word for
  <Mcq>
    <Mcqw>1. Copper</Mcqw>
    <Mcqw>2. Oxygen</Mcqw>
    <Mcqw>3. Sulphur</Mcqw>
    <Mcqc>4. Brass</Mcqc>
  </Mcq>

### 29. Which Interhalogen is so unstable that it has been just spectroscopically detected?
  <Mcq>
    <Mcqc>1. IF</Mcqc>
    <Mcqw>2. IBr</Mcqw>
    <Mcqw>3. ClF₃</Mcqw>
    <Mcqw>4. BrF₅</Mcqw>
  </Mcq>

### 30. Identify the nitrogen compounds A, B, C, D, and E.
![](https://icdn.talentbrick.com/mcq/p-Block-MCQ-q30.png)
  <Mcq>
    <Mcqw>1. A = NO₂ and C = NO₂</Mcqw>
    <Mcqw>2. B = N₂O₄ and D = N₂O₃</Mcqw>
    <Mcqc>3. C = NO and E = NO₂</Mcqc>
    <Mcqw>4. A = NO₂, B = N₂O₃</Mcqw>
  </Mcq>