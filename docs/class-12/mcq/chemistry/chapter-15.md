---
sidebar_position: 15
title: MCQ Questions for Class 12 Chemistry Chapter 15 Polymers with Answers
description: Polymers Chapter 15 NCERT MCQ questions for Class 12. Latest questions to expect in Jee Mains | NEET | School Exams.
image: https://icdn.talentbrick.com/mcq/class-12-chem-chapter-15.png
sidebar_label: Chapter 15
---

import Mcq, {Mcqinfo, Mcqw, Mcqc, Mcqans} from "@site/src/components/Mcq";

MCQ Questions for Class 12 Chemistry Chapter 15 Polymers for NEET |  School Exams | Class 12

Prepare these important MCQ Questions of Class 12 Chemistry Chapter 15 Polymers with Answers, These are Latest questions to expect in NEET | School Exams.

<Mcqinfo/>

### 1. Which one of the following is natural polymer:
  <Mcq>
    <Mcqw>1. Cellulose acetate</Mcqw>
    <Mcqw>2. Glyptal</Mcqw>
    <Mcqw>3. Cellulose nitrate</Mcqw>
    <Mcqc>4. Resins</Mcqc>
  </Mcq>

### 2. Choose the correct statement.
  <Mcq>
    <Mcqw>1. Dacron is synthesized by using acid or basic catalysis.</Mcqw>
    <Mcqc>2. Novolac requires heating to form Bakelite.</Mcqc>
    <Mcqw>3. Copolymers can be made step growth polymerization only.</Mcqw>
    <Mcqw>4. Melamine formaldehyde is used making combs.</Mcqw>
  </Mcq>

### 3. Which of the following has the superior resistance to vegetable and mineral oils?
  <Mcq>
    <Mcqw>1. Buna-N</Mcqw>
    <Mcqw>2. Buna-S</Mcqw>
    <Mcqw>3. Natural rubber</Mcqw>
    <Mcqc>4. Neoprene</Mcqc>
  </Mcq>

### 4. Which of the following polymer is used as Insulator?
  <Mcq>
    <Mcqc>1. Polystyrene</Mcqc>
    <Mcqw>2. Polypropene</Mcqw>
    <Mcqw>3. Polyethene&nbsp;</Mcqw>
    <Mcqw>4. Bakelite</Mcqw>
  </Mcq>

### 5. Which is the most important class of biodegradable polymer?
  <Mcq>
    <Mcqw>1. Aromatic</Mcqw>
    <Mcqc>2. Aliphatic</Mcqc>
    <Mcqw>3. Cyclic</Mcqw>
    <Mcqw>4. Oxolanes</Mcqw>
  </Mcq>

### 6. Which of the following is fiber forming solid?
  <Mcq>
    <Mcqw>1. Nylon – 2,6</Mcqw>
    <Mcqc>2. Nylon – 6</Mcqc>
    <Mcqw>3. PHBV</Mcqw>
    <Mcqw>4. Dacron</Mcqw>
  </Mcq>

### 7. Match the polymers given in Column I with their main applications given in Column II.
[![](https://1.bp.blogspot.com/-U_ozQd09bk8/YGBxgxNPjvI/AAAAAAAAArk/o0qsFJxLcpE4-rYJ2Vk9WXF4UE6ZgpSYwCLcBGAsYHQ/s0/NCERT%2BQUESTION.png)](https://1.bp.blogspot.com/-U_ozQd09bk8/YGBxgxNPjvI/AAAAAAAAArk/o0qsFJxLcpE4-rYJ2Vk9WXF4UE6ZgpSYwCLcBGAsYHQ/s0/NCERT%2BQUESTION.png)
  <Mcq>
    <Mcqw>1. i = a, ii = c, iii = d, iv = e, v = b, vi = f</Mcqw>
    <Mcqw>2. i = a, ii = c, iii = f, iv = b, v = b, vi = f</Mcqw>
    <Mcqc>3. i = d, ii = e, iii = a, iv = f, v = b, vi = c</Mcqc>
    <Mcqw>4. i = e, ii = e, iii = c, iv = f, v = b, vi = a</Mcqw>
  </Mcq>

### 8. Polymers property is closely related to
  <Mcq>
    <Mcqw>1. Size</Mcqw>
    <Mcqw>2. Molecular mass</Mcqw>
    <Mcqw>3. Structure</Mcqw>
    <Mcqc>4. All of these</Mcqc>
  </Mcq>

### 9. For which type of rubber 5% of Sulphur is used at crosslinking agent?
  <Mcq>
    <Mcqw>1. Cable insulation rubber</Mcqw>
    <Mcqw>2. Pipes rubber</Mcqw>
    <Mcqw>3. Glass rubber</Mcqw>
    <Mcqc>4. Tyre rubber</Mcqc>
  </Mcq>

### 10. Which of the following is not true about low density polythene?
  <Mcq>
    <Mcqw>1. Tough</Mcqw>
    <Mcqc>2. Hard</Mcqc>
    <Mcqw>3. Poor conductor of Electricity</Mcqw>
    <Mcqw>4. Highly branched structure</Mcqw>
  </Mcq>

### 11. Which of the following is not a semisynthetic polymer?
  <Mcq>
    <Mcqc>1. cis-polyisoprene</Mcqc>
    <Mcqw>2. Cellulose nitrate</Mcqw>
    <Mcqw>3. Cellulose accetate</Mcqw>
    <Mcqw>4. Vulcanised rubber</Mcqw>
  </Mcq>

### 12. Oldest synthetic polymers.
  <Mcq>
    <Mcqc>a. Phenol formaldehyde</Mcqc>
    <Mcqw>b. Urea formaldehyde</Mcqw>
    <Mcqw>c. Nylon – 6</Mcqw>
    <Mcqw>d. Rayon</Mcqw>
  </Mcq>

### 13. Polymers used glass reinforcing material.
  <Mcq>
    <Mcqw>1. Nylon – 6</Mcqw>
    <Mcqw>2. Phenol formaldehyde</Mcqw>
    <Mcqw>3. Rayon</Mcqw>
    <Mcqc>4. Dacron</Mcqc>
  </Mcq>

### 14. Zinc acetate antimony oxide is used as catalyst for.
  <Mcq>
    <Mcqw>1. Nylon-6</Mcqw>
    <Mcqc>2. Dacron</Mcqc>
    <Mcqw>3. Nylon-6,6</Mcqw>
    <Mcqw>4. Buna-N</Mcqw>
  </Mcq>