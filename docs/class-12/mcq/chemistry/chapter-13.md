---
sidebar_position: 13
title: MCQ Questions Amines Class 12 Chemistry Chapter 13
description: Amines Chemistry Chapter 13 NCERT MCQ questions for Class 12. Latest questions to expect in Jee Mains | NEET | School Exams.
image: https://icdn.talentbrick.com/mcq/class-12-chem-chapter-13.png
sidebar_label: Chapter 13
---

import Mcq, {Mcqinfo, Mcqw, Mcqc, Mcqans} from "@site/src/components/Mcq";

NCERT Chemistry Class 12 MCQ Practice Questions Organic Chemistry Amines Chapter 13 for JEE | NEET | Class 12. Prepare these important [MCQ Questions of Chemistry Class 12](/class-12/mcq/chemistry) Amines Chapter 13, Latest questions to expect in NEET | JEE | School Exams.

<Mcqinfo/>

### 1. Choose the correct IUPAC name of Ph-NH₂

  <Mcq>
    <Mcqw>1. Aniline</Mcqw>
    <Mcqw>2. Benzenamine</Mcqw>
    <Mcqw>3. Amino Benzene</Mcqw>
    <Mcqc>4. Both 1 and 2</Mcqc>
  </Mcq>

### 2. How many of the following contains secondary amino groups?

| Adrenaline, Surfactants, Benadryl, Ephedrine |
| -------------------------------------------- |


  <Mcq>
    <Mcqw>a. 1</Mcqw>
    <Mcqc>b. 2</Mcqc>
    <Mcqw>c. 3</Mcqw>
    <Mcqw>d. 4</Mcqw>
  </Mcq>

### 3. The major products A and B for following reactions are respectively:

![](https://icdn.talentbrick.com/mcq/eq365.png)

  <Mcq>
    <Mcqw>1.<img src="https://icdn.talentbrick.com/mcq/B-r.png" /></Mcqw>
    <Mcqc>2.<img src="https://icdn.talentbrick.com/mcq/A-r.png" /></Mcqc>
    <Mcqw>3.<img src="https://icdn.talentbrick.com/mcq/C-r.png" /></Mcqw>
    <Mcqw>4.<img src="https://icdn.talentbrick.com/mcq/D-r.png" /></Mcqw>
  </Mcq>

### 4. What is the name of the H₂N–CH₂–CH₂–NH₂

  <Mcq>
    <Mcqc>1. ethane-1, 2-diamine</Mcqc>
    <Mcqw>2. ethana-1, 2-diamine</Mcqw>
    <Mcqw>3. ethylene diamine</Mcqw>
    <Mcqw>4. ethylidene amine</Mcqw>
  </Mcq>

### 5. Reaction of Aniline with sulphuric acid at room temperature gives

  <Mcq>
    <Mcqw>1. Ortho-Subsituted Zwitterion</Mcqw>
    <Mcqc>2. Anilinium Salt</Mcqc>
    <Mcqw>3. Para-Substituted Zwitterion</Mcqw>
    <Mcqw>4. Meta-Subsituted Zwitterion</Mcqw>
  </Mcq>

### 6. With which of the following reagents reduction of nitro group is preffered?

  <Mcq>
    <Mcqw>1. SnCl₄</Mcqw>
    <Mcqc>2. FeCl₂</Mcqc>
    <Mcqw>3. FeCl₃</Mcqw>
    <Mcqw>4. Sn</Mcqw>
  </Mcq>

### 7. Primary amines with more than \_\_ carbons are liquid.

  <Mcq>
    <Mcqw>a. 9</Mcqw>
    <Mcqw>b. 2</Mcqw>
    <Mcqc>c. 3</Mcqc>
    <Mcqw>d. 4</Mcqw>
  </Mcq>

### 8. Choose the correct statement.

  <Mcq>
    <Mcqw>1. Arylamines are usually colored but get decolorises due to reduction.</Mcqw>
    <Mcqw>2. Arylamines are usually colorless but get colored due to reduction.</Mcqw>
    <Mcqw>3. Arylamines are usually colored but get decolorises due to oxidation.</Mcqw>
    <Mcqc>4. Arylamines are usually colorless but get colored due to oxidation.</Mcqc>
  </Mcq>

### 9. Arrange the compounds of similar molecular mass on the basis of decreasing boiling point.

  <Mcq>
    <Mcqw>1. Tertiarry Amine, Alcohol, Primary Amine, Secondary Amine</Mcqw>
    <Mcqw>2. Primary Amine, Secondary Amine, Tertiarry Amine, Alcohol</Mcqw>
    <Mcqw>3. Primary Amine, Tertiarry Amine, Alcohol, Secondary Amine</Mcqw>
    <Mcqc>4. Alcohol, Primary Amine, Secondary Amine, Tertiarry Amine</Mcqc>
  </Mcq>

### 10. Aliphatic Amines have the pKb value range.

  <Mcq>
    <Mcqw>1. 8 – 9.2</Mcqw>
    <Mcqc>2. 3 – 4.2</Mcqc>
    <Mcqw>3. 5 – 6.2</Mcqw>
    <Mcqw>4. 1.2 – 2</Mcqw>
  </Mcq>

### 11. Ammonia on reaction with carboxylic acid at room temperature forms.

  <Mcq>
    <Mcqc>1. Salt</Mcqc>
    <Mcqw>2. Amide</Mcqw>
    <Mcqw>3. Imine</Mcqw>
    <Mcqw>4. Nitrile</Mcqw>
  </Mcq>

### 12. Arrange the following in increasing order of solubility in water (C₂H₅)₂NH, C₂H₅NH₂, C₆H₅NH₂

  <Mcq>
    <Mcqc>1. C₆H₅NH₂, (C₂H₅)₂NH, C₂H₅NH₂</Mcqc>
    <Mcqw>2. (C₂H₅)₂NH, C₂H₅NH₂, C₆H₅NH₂</Mcqw>
    <Mcqw>3. (C₂H₅)₂NH, C₆H₅NH₂, C₂H₅NH₂</Mcqw>
    <Mcqw>4. C₂H₅NH₂, (C₂H₅)₂NH, C₆H₅NH₂</Mcqw>
  </Mcq>

### 13. Which of the following statement is true for diazonium fluoroborate?

  <Mcq>
    <Mcqw>1. Soluble in water</Mcqw>
    <Mcqw>2. On heating gives inorganic graphite</Mcqw>
    <Mcqw>3. On reaction with NaNO₂ to give fluorobenzene</Mcqw>
    <Mcqc>4. Stable at room temperature</Mcqc>
  </Mcq>

### 14. Arrange in the increasing order of Basic nature in water.

  <Mcq>
    <Mcqw>1. Methanamine, N,N-Diethylethanamine, Ethanamine</Mcqw>
    <Mcqc>2. Methanamine, Ethanamine, N,N-Diethylethanamine</Mcqc>
    <Mcqw>3. Ethanamine, N,N-Diethylethanamine, Methanamine</Mcqw>
    <Mcqw>4. N,N-Diethylethanamine, Methanamine, Ethanamine</Mcqw>
  </Mcq>

### 15. The major product of the following reaction is

  <Mcq>
    <div className="separator" style={{"clear":"both","margin-bottom":"12px"}}><a href="https://1.bp.blogspot.com/-7egyd9AOsWo/YIa_m91fbbI/AAAAAAAAAwU/6O83jzL_afY3qyRd5IieCsR3vCOI8dWOwCLcBGAsYHQ/s0/Talentbrick.com%2Bmajor%2Bquestion.png" style={{"display":"block","padding":"1em 0","-webkit-text-align":"center","text-align":"center", "background":"white"}}><img alt="The major product of the following reaction is" data-original-height={169} data-original-width={541} src="https://1.bp.blogspot.com/-7egyd9AOsWo/YIa_m91fbbI/AAAAAAAAAwU/6O83jzL_afY3qyRd5IieCsR3vCOI8dWOwCLcBGAsYHQ/s0/Talentbrick.com%2Bmajor%2Bquestion.png" border={0} /></a>
    </div>
    <img alt data-original-height={253} data-original-width={240} style={{"background":"white"}} src="https://1.bp.blogspot.com/-bB0VcFNJ8NU/YIbA3xzO2GI/AAAAAAAAAwc/1co143uMgcwlbB_yf_KFcvCBoKAH4nihgCLcBGAsYHQ/s0/question-a.png" border="2px" /><img style={{"background":"white"}} alt data-original-height={253} data-original-width={146} src="https://1.bp.blogspot.com/-C9z5OmnDgqw/YIbA3-5qTvI/AAAAAAAAAwk/J3YpO3t77LYWgEwZjNgsW9cRwKtAzczSgCLcBGAsYHQ/s0/question-b.png" style={{"background":"white"}} border="2px" /><img alt data-original-height={253} data-original-width={145} src="https://1.bp.blogspot.com/-FE1ENVpn1Rw/YIbA30sz1gI/AAAAAAAAAwg/X3l6pTwHLAYqOn_L7AEICKOkZVijzDE2wCLcBGAsYHQ/s0/question-c.png" style={{"background":"white"}} border="2px" /><img alt data-original-height={253} data-original-width={208} src="https://1.bp.blogspot.com/-NNi9561_8Ao/YIbA4YaB4mI/AAAAAAAAAwo/Wiiini02ntMhqxR_Us4n2Kv-sD6zLrF5QCLcBGAsYHQ/s0/question-d.png" style={{"background":"white"}} border="2px" />
    <Mcqw>1. A</Mcqw>
    <Mcqw>2. B</Mcqw>
    <Mcqc>3. C</Mcqc>
    <Mcqw>4. D</Mcqw>
  </Mcq>
