---
sidebar_position: 8
title: MCQ Questions D and F Block Elements Inorganic Chemistry Chapter 8 Class 12
description: D-and-F Block Elements Inorganic Chemistry NCERT MCQ questions for Class 12. Latest questions to expect in Jee Mains | NEET | School Exams.
image: https://icdn.talentbrick.com/mcq/class-12-chem-chapter-8.png
sidebar_label: Chapter 8
---

import Mcq, {Mcqinfo, Mcqw, Mcqc, Mcqans} from "@site/src/components/Mcq";

NCERT Inorganic Chemistry D-and-F Block Elements Class 12th Chemistry MCQ Practice Questions for Jee Mains | NEET | Class 12.

Prepare these important Questions of D-and-F Block Elements in Inorganic Chemistry, Latest questions to expect in Jee Mains | NEET | School Exams.

<Mcqinfo/>

### 1. Copernicium has electronic configuration as:
  <Mcq>
    <Mcqw>a. 7s² 6d²</Mcqw>
    <Mcqw>b. 7s² 6d⁴</Mcqw>
    <Mcqw>c. 7s² 6d⁶</Mcqw>
    <Mcqc>d. 7s² 6d¹⁰</Mcqc>
  </Mcq>

### 2. Arrange according to melting point order:
  <Mcq>
    <Mcqc>1. W &gt; Re &gt; Ru &gt; Rh</Mcqc>
    <Mcqw>2. Re &gt; Ru &gt; Rh &gt; W</Mcqw>
    <Mcqw>3. Re &gt; W &gt;Ru &gt; Rh</Mcqw>
    <Mcqw>4.W &gt; Rh &gt; Ru &gt; Re</Mcqw>
  </Mcq>

### 3. Enthalpy of atomisation is maximum for:
  <Mcq>
    <Mcqw>1. Titanium</Mcqw>
    <Mcqw>2. Cobalt</Mcqw>
    <Mcqc>3. Vanadium</Mcqc>
    <Mcqw>4. Scandium</Mcqw>
  </Mcq>

### 4. Among Ti, V, Cr, Mn which metal has minimum metallic radius:
  <Mcq>
    <Mcqw>1. Ti</Mcqw>
    <Mcqc>2. Cr</Mcqc>
    <Mcqw>3. Mn</Mcqw>
    <Mcqw>4. V</Mcqw>
  </Mcq>

### 5. Which of the following element have highest oxidising potential in +3 oxidation state?
  <Mcq>
    <Mcqw>1. Mn</Mcqw>
    <Mcqc>2. Co</Mcqc>
    <Mcqw>3. Fe</Mcqw>
    <Mcqw>4. V</Mcqw>
  </Mcq>

### 6. +5 oxidation state is not possesed by:
  <Mcq>
    <Mcqw>1. Mn</Mcqw>
    <Mcqc>2. Fe</Mcqc>
    <Mcqw>3. Cr</Mcqw>
    <Mcqw>4. V</Mcqw>
  </Mcq>

### 7. The value of E⁰ is more negative than expected for:
  <Mcq>
    <Mcqw>1. Zn</Mcqw>
    <Mcqw>2. Ni</Mcqw>
    <Mcqw>3. Mn</Mcqw>
    <Mcqc>4. All of these</Mcqc>
  </Mcq>

### 8. High value of E⁰ of Nickel is contributed to:
  <Mcq>
    <Mcqw>1.Low ionisation enthalpy I</Mcqw>
    <Mcqw>2. Low atomisation energy </Mcqw>
    <Mcqw>3. Low ionisation enthalpy II</Mcqw>
    <Mcqc>4. High hydration energy</Mcqc>
  </Mcq>

### 9.In VX₂ and CuX type halide X cannot be:
  <Mcq>
    <Mcqc>1. F</Mcqc>
    <Mcqw>2. Cl</Mcqw>
    <Mcqw>3. Br</Mcqw>
    <Mcqw>4. I</Mcqw>
  </Mcq>

### 10. Ferrates (VI), are formed in:
  <Mcq>
    <Mcqw>1. Acidic medium</Mcqw>
    <Mcqw>2.Neutral medium</Mcqw>
    <Mcqc>3. Alkaline medium</Mcqc>
    <Mcqw>4. Both 1 and 2</Mcqw>
  </Mcq>

### 11. Which among the following will release hydrogen gas on reaction with dilute acid?
  <Mcq>
    <Mcqw>1. Mn⁺³</Mcqw>
    <Mcqw>2. Co⁺³</Mcqw>
    <Mcqw>3. Ti</Mcqw>
    <Mcqc>4. Cr⁺²</Mcqc>
  </Mcq>

### 12. Wilkinson’s catalyst is:
  <Mcq>
    <Mcqc>1. Homogeneous catalyst</Mcqc>
    <Mcqw>2. Heterogeneous catalyst</Mcqw>
    <Mcqw>3. Dehydrogenating catalyst</Mcqw>
    <Mcqw>4. Oxidising catalyst</Mcqw>
  </Mcq>

### 13.Which metal/ion catalyses reaction between iodine and persulphate ions?
  <Mcq>
    <Mcqc>1. Fe(III)</Mcqc>
    <Mcqw>2. Co(II)</Mcqw>
    <Mcqw>3.Fe(II)</Mcqw>
    <Mcqw>4. Ni(II)</Mcqw>
  </Mcq>

### 14. Alloys are formed by atoms with metallic radii that are how much percent of each other?
  <Mcq>
    <Mcqw>a. 20%</Mcqw>
    <Mcqw>b. 5%</Mcqw>
    <Mcqw>c. 50%</Mcqw>
    <Mcqc>d. 15%</Mcqc>
  </Mcq>

### 15. Mn₂O₇ is a:
  <Mcq>
    <Mcqw>1. Ionic green solid </Mcqw>
    <Mcqw>2. Ionic yellow solid </Mcqw>
    <Mcqc>3. Covalent green oil</Mcqc>
    <Mcqw>4. Covalent yellow gas</Mcqw>
  </Mcq>

### 16.Potasium dichromate is used as —– in volumetric analysis
  <Mcq>
    <Mcqc>1. Primary standard</Mcqc>
    <Mcqw>2. Secondary standard</Mcqw>
    <Mcqw>3. Indicator&nbsp;</Mcqw>
    <Mcqw>4. Catalyst</Mcqw>
  </Mcq>

### 17. In neutral medium thiosulphate is oxidised to which species by permanganate
  <Mcq>
    <Mcqw>1. Peroxodisulphuric acid</Mcqw>
    <Mcqw>2. Caro acid</Mcqw>
    <Mcqc>3.Sulphate</Mcqc>
    <Mcqw>4. Dithionate</Mcqw>
  </Mcq>

### 18.Lanthanum belongs to which block:
  <Mcq>
    <Mcqw>1. p </Mcqw>
    <Mcqc>2. d</Mcqc>
    <Mcqw>3. s</Mcqw>
    <Mcqw>4. f</Mcqw>
  </Mcq>

### 19. Which of the following carbide is not formed by lanthanoids?
  <Mcq>
    <Mcqw>a. Ln₃C</Mcqw>
    <Mcqw>b. Ln₂C₃</Mcqw>
    <Mcqc>c. Ln₄C₃</Mcqc>
    <Mcqw>d. All of these</Mcqw>
  </Mcq>

### 20. What is configuration of Gd²⁺
  <Mcq>
    <Mcqw>1. 4f⁸</Mcqw>
    <Mcqc>2. 4f⁷5d¹</Mcqc>
    <Mcqw>3. 4f⁵5d³</Mcqw>
    <Mcqw>4. 4f⁷6s¹</Mcqw>
  </Mcq>

### 21. Which one of the following is coinage metal:
  <Mcq>
    <Mcqw>1. Ni</Mcqw>
    <Mcqw>2. Fe</Mcqw>
    <Mcqc>3. Cu</Mcqc>
    <Mcqw>4. Zn</Mcqw>
  </Mcq>

### 22. Which of the following lanthanoid is steal hard?
  <Mcq>
    <Mcqw>1. Dy</Mcqw>
    <Mcqc>2. Sm</Mcqc>
    <Mcqw>3. Ho</Mcqw>
    <Mcqw>4. Yb</Mcqw>
  </Mcq>

### 23. Which element is not present in misch metal:
  <Mcq>
    <Mcqw>1. Carbon</Mcqw>
    <Mcqc>2. Magnesium</Mcqc>
    <Mcqw>3. Calcium</Mcqw>
    <Mcqw>4. Sulphur</Mcqw>
  </Mcq>

### 24. State True or False:
a. Hydroxides of lanthanoids are just hydrated oxides.  
b. Trivalent lanthanoids ions are colored in both solid state and aqueous solution.
  <Mcq>
    <Mcqw>1. F,F</Mcqw>
    <Mcqw>2. T,F</Mcqw>
    <Mcqw>3. T,T</Mcqw>
    <Mcqc>4. F,T</Mcqc>
  </Mcq>

### 25. Actinoids on reaction with boiling water forms:
  <Mcq>
    <Mcqw>1. Oxides only</Mcqw>
    <Mcqw>2. Hydride only</Mcqw>
    <Mcqw>3. Hydroxides</Mcqw>
    <Mcqc>4. Both Oxide and Hydride</Mcqc>
  </Mcq>

### 26. Choose the correct statement:
  <Mcq>
    <Mcqw>1. V₂O₄ do not dissolve in acids.</Mcqw>
    <Mcqw>2. V₂O₄ dissolve in acids and form VO⁺ salts.</Mcqw>
    <Mcqc>3. V₂ O₄ dissolve in acids and form VO⁺² salts.</Mcqc>
    <Mcqw>4. V₂O₄ dissolve in acids and form VO₂⁺ salts.</Mcqw>
  </Mcq>

### 27. Choose the incorrect option with respect to uses:
  <Mcq>
    <Mcqw>1. MnO₂ – Dry Cells</Mcqw>
    <Mcqw>2. Ln₂O₃ – Petroleum Cracking</Mcqw>
    <Mcqw>3. AgBr – Photographic industry</Mcqw>
    <Mcqc>4. V₂O₅ – Pigment industry</Mcqc>
  </Mcq>

### 28. Which of the following do not exhibit +4 oxidation states?
  <Mcq>
    <Mcqw>1. Pr</Mcqw>
    <Mcqc>2. Sm</Mcqc>
    <Mcqw>3. Nd</Mcqw>
    <Mcqw>4. Tb</Mcqw>
  </Mcq>

### 29. In the reaction of Fe⁺² ions with potassium permanganate, color of solution changes from:
  <Mcq>
    <Mcqw>1. Yellow to Green</Mcqw>
    <Mcqw>2. Red to Yellow</Mcqw>
    <Mcqw>3. Violet to Green</Mcqw>
    <Mcqc>4. Green to Yellow</Mcqc>
  </Mcq>

### 30. Which of the following for manganate ion is true:
  <Mcq>
    <Mcqw>1. Diamagnetism only</Mcqw>
    <Mcqw>2. Diamagnetism along with temperature dependent strong paramagnetism</Mcqw>
    <Mcqw>3. Diamagnetism along with temperature independent weak paramagnetism</Mcqw>
    <Mcqc>4. Diamagnetism along with temperature dependent weak paramagnetism</Mcqc>
  </Mcq>