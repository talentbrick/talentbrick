---
sidebar_position: 11
title: MCQ Questions Dual Nature of Radiation and Matter Physics Class 12 Chapter 11
description: Dual Nature of Radiation and Matter | NCERT Physics MCQ questions for Class 12. Latest questions to expect in Jee Mains | NEET | School Exams.
image: https://icdn.talentbrick.com/mcq/class-12-phy-chapter-11.png
sidebar_label: Chapter 11
---

import Mcq, {Mcqinfo, Mcqw, Mcqc, Mcqans} from "@site/src/components/Mcq";

NCERT Physics – Dual Nature of Radiation and Matter Class 12 Practice Questions for Jee Mains | NEET | School Exams.

Prepare these important Questions of Nature of Matter and Radiation, Latest questions to expect in Jee Mains | NEET | School Exams.

<Mcqinfo/>

### 1. Cathode Rays were discovered by:
  <Mcq>
    <Mcqc>1. William Crookes</Mcqc>
    <Mcqw>2. J.J Thompson</Mcqw>
    <Mcqw>3. Roentgen</Mcqw>
    <Mcqw>4. R.A Mulliken</Mcqw>
  </Mcq>

### 2. Choose the correct one:
  <Mcq>
    <Mcqw>1. Work function is generally measured in joules.</Mcqw>
    <Mcqw>2. Work function is not much sensitive to surface impurities.</Mcqw>
    <Mcqc>3. Work function is maximum for Al among Na, Ca, Al.</Mcqc>
    <Mcqw>4. Alkali metals responds only to UV light.</Mcqw>
  </Mcq>

### 3. When charged zinc plate is illuminated by UV light, then leaves of electroscope will:
  <Mcq>
    <Mcqw>1. Repel only.</Mcqw>
    <Mcqc>2. Collapse and then further repel.</Mcqc>
    <Mcqw>3. Collapse and then attract.</Mcqw>
    <Mcqw>4. Attract and repel continiously.</Mcqw>
  </Mcq>

### 4. Who studied photo current variation with collector plate potential?
  <Mcq>
    <Mcqw>1. Albert Einstein.</Mcqw>
    <Mcqc>2. Hallwachs and Lenard.</Mcqc>
    <Mcqw>3. Henrich Hertz.</Mcqw>
    <Mcqw>4. Davisson &amp; Germer.</Mcqw>
  </Mcq>

### 5. Quartz window permits which light spectrum to pass through:
  <Mcq>
    <Mcqw>1. Infrared only</Mcqw>
    <Mcqw>2. Visible and infrared only</Mcqw>
    <Mcqw>3. Microwaves and radiowaves only</Mcqw>
    <Mcqc>4. UV rays</Mcqc>
  </Mcq>

### 6. Photoelectric current is measured by:
  <Mcq>
    <Mcqc>1. Microammeter.</Mcqc>
    <Mcqw>2. Galvanometer with high resistance in series.</Mcqw>
    <Mcqw>3. Ammeter</Mcqw>
    <Mcqw>4. Milliammeter</Mcqw>
  </Mcq>

### 7. Estimate the speed with which electrons emitted from the heated emitter of an evacuated tube impinge on collector maintained at a potential difference of 20 MV. Ignore the small initial speed of electrons.
  <Mcq>
    <Mcqw>a. 2.65 x 10⁹ m/sec</Mcqw>
    <Mcqw>b. 2.65 x 10⁸ m/sec</Mcqw>
    <Mcqc>c. 2.99 x 10⁸ m/sec</Mcqc>
    <Mcqw>d. 2.96 x 10⁷ m/sec</Mcqw>
  </Mcq>

### 8. Why gases start conducting at low pressure?
  <Mcq>
    <Mcqw>1. They ionise rapidly due to decreased internal energy.</Mcqw>
    <Mcqc>2. Ions have higher chance to reach electrode, as chance of recombination decreases.</Mcqc>
    <Mcqw>3. Gas behavior becomes near to ideal gas behavior.</Mcqw>
    <Mcqw>4. Gas expand at low pressure.</Mcqw>
  </Mcq>

### 9. Compute the typical de-Broglie wavelength for electron in metal at 300K and compare it with mean seperation between two electrons which is about 2 Å. then
  <Mcq>
    <Mcqc>1. de-Broglie wavelength is about 6 nm and there is strong overlap of electron wave packets.</Mcqc>
    <Mcqw>2. de-Broglie wavelength is about 6 Å and there is strong overlap of electron wave packets.</Mcqw>
    <Mcqw>3. de-Broglie wavelength is about 6 μm and there is no overlap of electron wave packets.</Mcqw>
    <Mcqw>4. de-Broglie wavelength is about 60 Å and there is no overlap of electron wave packets.</Mcqw>
  </Mcq>

### 10. Observable charges in nature are
  <Mcq>
    <Mcqw>1. +2/3 e</Mcqw>
    <Mcqc>2. Integral multiple of e</Mcqc>
    <Mcqw>3. -1/3 e</Mcqw>
    <Mcqw>4. All of these</Mcqw>
  </Mcq>

### 11. Choose the correct statement with respect to wavelength, velocity and frequency associated with matter wave.
  <Mcq>
    <Mcqc>1. Both wavelength and group velocity are of physical significance.</Mcqc>
    <Mcqw>2. Both phase velocity and group velocity are of physical significance.</Mcqw>
    <Mcqw>3. Both phase velocity and particle velocity are of physical significance.</Mcqw>
    <Mcqw>4. Both wavelength and phase velocity are of physical significance.</Mcqw>
  </Mcq>

### 12. Which of the following is incorrect with respect to photoelectric effect?
![](https://1.bp.blogspot.com/-AcolMi_Akl0/X7FkD1G_NCI/AAAAAAAAAPg/NpfSpikHHP42DuRN5-e40V30tCNrIX0mwCLcBGAsYHQ/opt1.png)![](https://1.bp.blogspot.com/-NRopDfHckFI/X7FkJQQFAUI/AAAAAAAAAPk/vl7jROADfRQkvzoYiR-4-bCL34fTMm0FgCLcBGAsYHQ/opt2.png)
  <Mcq>
    <Mcqw>a. 1)</Mcqw>
    <Mcqw>b. 2)</Mcqw>
    <Mcqc>c. 3)</Mcqc>
    <Mcqw>d. 4)</Mcqw>
  </Mcq>

### 13. Photoelectric current do not depends on:
  <Mcq>
    <Mcqw>1. Intensity of light</Mcqw>
    <Mcqw>2. Potential difference applied between electrode</Mcqw>
    <Mcqc>3. Stopping potential</Mcqc>
    <Mcqw>4. Nature of emitter</Mcqw>
  </Mcq>

### 14. Interference fringes are observed for:
  <Mcq>
    <Mcqw>1. Iodine molecules</Mcqw>
    <Mcqw>2. Electrons</Mcqw>
    <Mcqw>3. Neutrons</Mcqw>
    <Mcqc>4. All of these</Mcqc>
  </Mcq>

### 15. Which scientist obtained accurate value of Planck’s constant h?
  <Mcq>
    <Mcqw>1. Max Planck</Mcqw>
    <Mcqw>2. Max Born</Mcqw>
    <Mcqc>3. Mullikan</Mcqc>
    <Mcqw>4. Thomson</Mcqw>
  </Mcq>

### 16. Choose the correct statement.
  <Mcq>
    <Mcqw>1. A wave of definite wavelength is not extended all over space.</Mcqw>
    <Mcqw>2. Matter wave is made of single wavelength</Mcqw>
    <Mcqc>3. de-Broglie relation and Born’s probability interpretation reproduce the Heisenberg principle exactly.</Mcqc>
    <Mcqw>4. de-Broglie hypothesis does not support Bohr’s concept of stationary orbit.</Mcqw>
  </Mcq>

### 17. Matter waves are
  <Mcq>
    <Mcqw>1. Electromagnetic waves</Mcqw>
    <Mcqw>2. Mechanical waves</Mcqw>
    <Mcqw>3. Both 1 and 2</Mcqw>
    <Mcqc>4. Neither 1 nor 2</Mcqc>
  </Mcq>

### 18. Electric eye converts:
  <Mcq>
    <Mcqw>1. Change in frequency of illumination into change in photocurrent.</Mcqw>
    <Mcqc>2. Change in intensity of illumination into change in photocurrent.</Mcqc>
    <Mcqw>3. Change in frequency of illumination into change in intensity.</Mcqw>
    <Mcqw>4. Change in photocurrent of illumination into change in frequency.</Mcqw>
  </Mcq>

### 19. State true or false:
A. Absorption by rods and cones require wave picture of light.  
B. Gathering of light by eye-lens require wave picture of light.
  <Mcq>
    <Mcqw>1. T T</Mcqw>
    <Mcqw>2. F F</Mcqw>
    <Mcqw>3. T F</Mcqw>
    <Mcqc>4. F T</Mcqc>
  </Mcq>

### 20. Which of the following cannot be explained by wave model.
  <Mcq>
    <Mcqw>1. Rectilinear propagation.</Mcqw>
    <Mcqc>2. Low temperature specific heat of solids.</Mcqc>
    <Mcqw>3. Diffraction.</Mcqw>
    <Mcqw>4. Reflection.</Mcqw>
  </Mcq>