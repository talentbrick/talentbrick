---
sidebar_position: 8
title: MCQ Questions Human Health and Disease Biology Class 12 Chapter 8
description: Human Health and Disease Chapter 8 Biology Class 12, Boost your performance and score great marks in NEET and Class 11 Exam by practicing these MCQs.
image: https://icdn.talentbrick.com/mcq/class-12-bio-chapter-8.png
sidebar_label: Chapter 8
---

import Mcq, {Mcqinfo, Mcqw, Mcqc, Mcqans} from "@site/src/components/Mcq";
import GoogleAds from "@site/src/components/GoogleAds";

MCQ Questions Human Health and Disease Biology Class 12 Chapter 8 for NEET |  School Exams | Class 12 Reproduction in Plants, MCQ Practice Questions.

Prepare these important MCQ Questions of Biology Class 12 Human Health and Disease Ch.8, These are Latest questions to expect in NEET | School Exams.

<Mcqinfo/>
<GoogleAds slot="1911400781" />

### 1. Non-infectious disease, major cause of death is
  <Mcq>
    <Mcqw>1. Diabetes</Mcqw>
    <Mcqw>2. HIV</Mcqw>
    <Mcqc>3. Cancer</Mcqc>
    <Mcqw>4. Anemia</Mcqw>
  </Mcq>

### 2. Choose the correct statement.
  <Mcq>
    <Mcqw>1. All parasites are pathogen.</Mcqw>
    <Mcqc>2. Constipation is one of the symptoms of amoebic dysentery.</Mcqc>
    <Mcqw>3. In severe cases in common cold lips may turn blue or gray.</Mcqw>
    <Mcqw>4. Common cold lasts for 3-7 weeks.</Mcqw>
  </Mcq>

### 3. Malarial parasite reproduces __ in RBCs and __ in Liver.
  <Mcq>
    <Mcqw>1. Sexually, Sexually</Mcqw>
    <Mcqc>2. Asexually, Asexually</Mcqc>
    <Mcqw>3. Sexually, Asexually</Mcqw>
    <Mcqw>4. Asexually, Sexually</Mcqw>
  </Mcq>

### 4. Choose the correct statement with respect to stage that develops in RBCs:
  <Mcq>
    <Mcqw>1. Asexual stage, Gametocytes</Mcqw>
    <Mcqc>2. Sexual stage, Gametocytes</Mcqc>
    <Mcqw>3. Sexual stage, Sporozoites</Mcqw>
    <Mcqw>4. Asexual stage, Sporozoites</Mcqw>
  </Mcq>

### 5. Stools with excess mucous and blood clots is symptoms of:
  <Mcq>
    <Mcqw>1. Cholera</Mcqw>
    <Mcqw>2. Diphtheria</Mcqw>
    <Mcqc>3. Amoebiasis</Mcqc>
    <Mcqw>4. Dysentery</Mcqw>
  </Mcq>

### 6. Internal bleeding and blockage of intestinal passage are symptoms of:
  <Mcq>
    <Mcqw>1. Typhoid</Mcqw>
    <Mcqw>2. Tuberculosis</Mcqw>
    <Mcqc>3. Ascariasis</Mcqc>
    <Mcqw>4. Amoebiasis</Mcqw>
  </Mcq>

<GoogleAds slot="1911400781" />

### 7. Which of the following are responsible for causing ringworms?
A. Trichoderma  
B. Trichophyton  
C. Microsporum  
D. Pin worm
  <Mcq>
    <Mcqw>1. B, C, D</Mcqw>
    <Mcqw>2. A, B, C</Mcqw>
    <Mcqc>3. Only B and C</Mcqc>
    <Mcqw>4. Only A and B</Mcqw>
  </Mcq>

### 8. Most common infectious diseases are:
  <Mcq>
    <Mcqw>1. AIDS and Common cold</Mcqw>
    <Mcqc>2. Ringworm and Common cold</Mcqc>
    <Mcqw>3. Cancer and AIDS</Mcqw>
    <Mcqw>4. Common cold and Malaria</Mcqw>
  </Mcq>

### 9. Elephantiasis results in gross deformities when it affects:
  <Mcq>
    <Mcqw>1. Heart</Mcqw>
    <Mcqc>2. Gonads</Mcqc>
    <Mcqw>3. Lower limbs</Mcqw>
    <Mcqw>4. Blood vessels</Mcqw>
  </Mcq>

### 10. Acquired immunity is found in:
  <Mcq>
    <Mcqw>1. Insects, vertebrates</Mcqw>
    <Mcqw>2. Insects only</Mcqw>
    <Mcqw>3. Both plants and animals</Mcqw>
    <Mcqc>4. Vertebrates only</Mcqc>
  </Mcq>

### 11. Choose odd one out with respect to phagocytic cells:
  <Mcq>
    <Mcqw>1. Neutrophils</Mcqw>
    <Mcqw>2. Macrophages</Mcqw>
    <Mcqc>3. Natural killer cells</Mcqc>
    <Mcqw>4. Dendritic cells</Mcqw>
  </Mcq>

### 12. How many interdisulphide linkage are found between two heavy chains?
  <Mcq>
    <Mcqw>a. 3</Mcqw>
    <Mcqw>b. 4</Mcqw>
    <Mcqc>c. 2</Mcqc>
    <Mcqw>d. 1</Mcqw>
  </Mcq>

### 13. Choose the incorrect statement.
  <Mcq>
    <Mcqw>1. Everyone of us suffers from infectious diseases.</Mcqw>
    <Mcqc>2. In typhoid, there is high fever recurring every 2-3 days.</Mcqc>
    <Mcqw>3. Common cold infects respiratory passage.</Mcqw>
    <Mcqw>4. Plague is bacterial disease.</Mcqw>
  </Mcq>

### 14. Dysentery is a:
  <Mcq>
    <Mcqc>1. Bacterial disease</Mcqc>
    <Mcqw>2. Viral disease</Mcqw>
    <Mcqw>3. Protozoan disease</Mcqw>
    <Mcqw>4. Fungal disease</Mcqw>
  </Mcq>

### 15. A disease that man has been fighting since years is:
  <Mcq>
    <Mcqw>1. Cancer</Mcqw>
    <Mcqc>2. Malaria</Mcqc>
    <Mcqw>3. Dengue</Mcqw>
    <Mcqw>4. AIDS</Mcqw>
  </Mcq>

### 16. Select the correct statements among the following.
A. Each antibody molecule has four peptide chains.  
B. Anopheles mosquito is parasite on human being.  
C. Sporozoites escape from gut and migrate to salivary glands of mosquitoes.
  <Mcq>
    <Mcqw>1. Only C</Mcqw>
    <Mcqc>2. Both A and C</Mcqc>
    <Mcqw>3. Both A and B</Mcqw>
    <Mcqw>4. All A, B, C</Mcqw>
  </Mcq>

### 17. Infectious agents transmitted through food and water such as:
  <Mcq>
    <Mcqw>1. Typhoid</Mcqw>
    <Mcqw>2. Amoebiasis</Mcqw>
    <Mcqw>3. Ascariasis</Mcqw>
    <Mcqc>4. All of these</Mcqc>
  </Mcq>

### 18. Most important method to control vectors is:
  <Mcq>
    <Mcqw>1. Avoiding contact with infected person</Mcqw>
    <Mcqc>2. Avoiding stagnation of water</Mcqc>
    <Mcqw>3. Consumption of clean drinking water</Mcqw>
    <Mcqw>4. Maintaining personal hygiene</Mcqw>
  </Mcq>

### 19. State True or False
A. The discovery of blood by Harvey by pure reflective thought.  
B. Using thermometer Harvey proved the ‘good humor’ hypothesis.
  <Mcq>
    <Mcqw>1. T,F</Mcqw>
    <Mcqw>2. F,T</Mcqw>
    <Mcqw>3. T,T</Mcqw>
    <Mcqc>4. F,F</Mcqc>
  </Mcq>

### 20. Main barrier which prevents entry of microbes is
  <Mcq>
    <Mcqw>1. Mucus coating</Mcqw>
    <Mcqc>2. Skin</Mcqc>
    <Mcqw>3. Macrophage</Mcqw>
    <Mcqw>4. Neutrophils</Mcqw>
  </Mcq>

### 21. Choose the incorrect statement.
  <Mcq>
    <Mcqw>1. There is always a time lag between the infection and appearance of AIDS symptoms.</Mcqw>
    <Mcqw>2. HIV spreads only through body fluids.</Mcqw>
    <Mcqc>3. After getting into body of person, virus enters into helper T cells where it replicates its RNA genome.</Mcqc>
    <Mcqw>4. Anti-retroviral drugs cannot prevent death.</Mcqw>
  </Mcq>

### 22. Choose the odd one with respect to cancer of body part caused by tobacco smoking.
  <Mcq>
    <Mcqw>1. Urinary bladder</Mcqw>
    <Mcqw>2. Lung</Mcqw>
    <Mcqc>3. Oral cavity </Mcqc>
    <Mcqw>4. Throat</Mcqw>
  </Mcq>

### 23. Choose the odd one with respect to side effect caused by anabolic steroids in females.
  <Mcq>
    <Mcqw>1. Increased aggressiveness</Mcqw>
    <Mcqc>2. Breast enlargement</Mcqc>
    <Mcqw>3. Masculinisation</Mcqw>
    <Mcqw>4. Excessive hairs on face</Mcqw>
  </Mcq>

### 24. Identify the given figure, flower responsible for hallucinogenic properties.
![Identify the given figure, flower responsible for hallucinogenic properties.](https://icdn.talentbrick.com/mcq/q5a6s.png)
  <Mcq>
    <Mcqw>1. Atropa bellodona</Mcqw>
    <Mcqc>2. Datura</Mcqc>
    <Mcqw>3. Crack</Mcqw>
    <Mcqw>4. Morphine</Mcqw>
  </Mcq>

### 25. Majority of abused drugs are obtained from:
  <Mcq>
    <Mcqw>1. Fungi</Mcqw>
    <Mcqc>2. Flowering plants</Mcqc>
    <Mcqw>3. Animals</Mcqw>
    <Mcqw>4. Bacteria</Mcqw>
  </Mcq>

### 26. Most cancers are treated by:
  <Mcq>
    <Mcqw>1. Surgery</Mcqw>
    <Mcqw>2. Radiotherapy</Mcqw>
    <Mcqw>3. Chemotherapy</Mcqw>
    <Mcqc>4. All of these</Mcqc>
  </Mcq>

### 27. Heroin acts as:
  <Mcq>
    <Mcqw>1. Sedative</Mcqw>
    <Mcqc>2. Depressant</Mcqc>
    <Mcqw>3. Hallucinogen</Mcqw>
    <Mcqw>4. Stimulant</Mcqw>
  </Mcq>

### 28. Choose the incorrect match:
  <Mcq>
    <Mcqw>1. Benign cancer – causes little damage</Mcqw>
    <Mcqc>2. Viral genes – c-onc</Mcqc>
    <Mcqw>3. MRI – uses strong magnetic fields</Mcqw>
    <Mcqw>4. Cancer drug – side effect is Anemia</Mcqw>
  </Mcq>