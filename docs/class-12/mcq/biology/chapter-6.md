---
sidebar_position: 6
title: MCQ Questions Molecular Basis of Inheritance Class 12 Biology Chapter 6 with Answers
description: Molecular Basis of Inheritance Chapter 6 Biology Class 12, Boost your performance and score great marks in NEET and Class 11 Exam by practicing these MCQs.
image: https://icdn.talentbrick.com/mcq/class-12-bio-chapter-6.png
sidebar_label: Chapter 6
---

import Mcq, {Mcqinfo, Mcqw, Mcqc, Mcqans} from "@site/src/components/Mcq";
import GoogleAds from "@site/src/components/GoogleAds";

NCERT Biology Class 12th Bio Ch.6 Molecular Basis of Inheritance MCQ Practice Questions for NEET | Class 11

Prepare these important [MCQ Questions of Biology Class 12](/class-12/mcq/biology) Molecular Basis of Inheritance Chapter 6, Latest questions to expected to come in NEET | School Exams.

<Mcqinfo/>
<GoogleAds slot="1911400781" />

### 1. For terminating process of translation release factor binds to

  <Mcq>
    <Mcqw>1. Ribosome subunit</Mcqw>
    <Mcqc>2. Stop codon</Mcqc>
    <Mcqw>3. UTR at downstream</Mcqw>
    <Mcqw>4. tRNA</Mcqw>
  </Mcq>

### 2. Choose the incorrect statement.

  <Mcq>
    <Mcqw>1. Alec Jeffrey used satellite DNA as probe.</Mcqw>
    <Mcqw>2. β gal stands for beta-galactosidase.</Mcqw>
    <Mcqc>3. DNA from single cell is not enough to perform fingerprinting analysis.</Mcqc>
    <Mcqw>4. Satellite DNA could be classified on basis of different base composition.</Mcqw>
  </Mcq>

### 3. Choose the incorrect match with respect to codon and amino acid coded by them.

  <Mcq>
    <Mcqw>1. UUC – Phe</Mcqw>
    <Mcqc>2. AAU – Asp</Mcqc>
    <Mcqw>3. GAG – Glu</Mcqw>
    <Mcqw>4. UGU – Cys</Mcqw>
  </Mcq>

### 4. How different proteins does the ribosome consists of?

  <Mcq>
    <Mcqc>1. 80</Mcqc>
    <Mcqw>2. 40</Mcqw>
    <Mcqw>3. 25</Mcqw>
    <Mcqw>4. 400</Mcqw>
  </Mcq>

<GoogleAds slot="1911400781" />

### 5. Process of translation begins when

  <Mcq>
    <Mcqw>1. Charging of tRNA takes place in presence of ATP.</Mcqw>
    <Mcqw>2. Large subunit of ribosome encounters mRNA</Mcqw>
    <Mcqc>3. Small subunit of ribosome encounters mRNA</Mcqc>
    <Mcqw>4. tRNA binds UAC codon.</Mcqw>
  </Mcq>

### 6. Amino acid found attached to tRNA having anticodon UCA is

  <Mcq>
    <Mcqw>1. Tyrosine at 5′ end.</Mcqw>
    <Mcqc>2. Serine at 3′ end.</Mcqc>
    <Mcqw>3. Serine at 5′ end.</Mcqw>
    <Mcqw>4. Tyrosine at 3′ end.</Mcqw>
  </Mcq>

### 7. The actual structure of tRNA looks like:

  <Mcq>
    <Mcqw>1. Clover leaf</Mcqw>
    <Mcqw>2. Bean shaped</Mcqw>
    <Mcqc>3. Inverted L</Mcqc>
    <Mcqw>4. Double helix</Mcqw>
  </Mcq>

### 8. Choose the incorrect option:

  <Mcq>
    <Mcqw>1. Lac operon is under both positive and negative regulation.</Mcqw>
    <Mcqc>2. Control of rate of translation is predominant site for control of gene expression in prokaryotes.</Mcqc>
    <Mcqw>3. In most cases the sequences of operator bind a repressor protein.</Mcqw>
    <Mcqw>4. In presence of lactose or allolactose, repressor is inactivated.</Mcqw>
  </Mcq>

### 9. There are **i** sites in **ii** for amino acids to bind and be close enough for bond formation. i and ii are

  <Mcq>
    <Mcqw>1. 3, large subunit</Mcqw>
    <Mcqc>2. 2, large subunit</Mcqc>
    <Mcqw>3. 2, small subunit</Mcqw>
    <Mcqw>4. 3, small subunit</Mcqw>
  </Mcq>

### 10. All the following amino acids are coded by six different codons except

  <Mcq>
    <Mcqw>1. Arginine</Mcqw>
    <Mcqc>2. Lysine</Mcqc>
    <Mcqw>3. Leucine</Mcqw>
    <Mcqw>4. Serine</Mcqw>
  </Mcq>

### 11. Chemical method developed by which scientist was instrumental in synthesis of RNA molecules with defined combination of bases?

  <Mcq>
    <Mcqw>1. Jacob Monod</Mcqw>
    <Mcqw>2. M.Nirenberg</Mcqw>
    <Mcqw>3. G.Gamow</Mcqw>
    <Mcqc>4. HG Khorana</Mcqc>
  </Mcq>

### 12. We face difficulty in predicting the codon sequence from the given amino acid sequence because of which property of genetic code

  <Mcq>
    <Mcqw>1. Universal</Mcqw>
    <Mcqw>2. Non ambiguous</Mcqw>
    <Mcqc>3. Degeneracy</Mcqc>
    <Mcqw>4. Comaless natture</Mcqw>
  </Mcq>

### 13. Whic of the following,finally the helped the genetic code code to deciphered?

  <Mcq>
    <Mcqw>1. Severa ochoa</Mcqw>
    <Mcqc>2. Cell free system for protein sythesis</Mcqc>
    <Mcqw>3. Chemical method to for synthesis of RNA with defined combination of bases.</Mcqw>
    <Mcqw>4. Lac operon elucidation</Mcqw>
  </Mcq>

### 14. Vectors used in human genome project include

  <Mcq>
    <Mcqw>1. BAC</Mcqw>
    <Mcqw>2. YAC</Mcqw>
    <Mcqw>3. Ti plasmid</Mcqw>
    <Mcqc>4. Both 1 and 2</Mcqc>
  </Mcq>

### 15. Choose the incorrect statement.

  <Mcq>
    <Mcqw>1. Y chromosome has 231 genes.</Mcqw>
    <Mcqw>2. The two alleles of a chromosome contain different copy numbers of VNTR. </Mcqw>
    <Mcqw>3. DNA from every tissue from and individual show same degree of polymorphism.</Mcqw>
    <Mcqc>4. Satellite DNA on density gradient centrifugation forms major peak.</Mcqc>
  </Mcq>

### 16. QB bacteriophage has genetic material as

  <Mcq>
    <Mcqc>1. RNA</Mcqc>
    <Mcqw>2. ss DNA</Mcqw>
    <Mcqw>3. ds DNA</Mcqw>
    <Mcqw>4. Both RNA and DNA</Mcqw>
  </Mcq>

### 17. ΦX174 phage has<

  <Mcq>
    <Mcqw>1. 5386 base pairs</Mcqw>
    <Mcqc>2. 5386 nucleotides</Mcqc>
    <Mcqw>3. 48502 bp</Mcqw>
    <Mcqw>4. about 4 billion bp</Mcqw>
  </Mcq>

### 18. Unequivocal proof of DNA comes from study on

  <Mcq>
    <Mcqw>1. Bacteria</Mcqw>
    <Mcqw>2. Neurospora</Mcqw>
    <Mcqw>3. Humans</Mcqw>
    <Mcqc>4. Bacterial virus</Mcqc>
  </Mcq>

### 19. State True or False:

A. The chains in B-DNA have parallel polarity.  
B. RNA has ability to duplicate direct their duplication.

  <Mcq>
    <Mcqw>1. T, F</Mcqw>
    <Mcqc>2. F, T</Mcqc>
    <Mcqw>3. T, T</Mcqw>
    <Mcqw>4. F, F</Mcqw>
  </Mcq>

### 20. Blending is performed in Hershey chase experiment to

  <Mcq>
    <Mcqw>1.Seperate virus particles from bacteria </Mcqw>
    <Mcqc>2. Remove viral coats from bacteria</Mcqc>
    <Mcqw>3. Infect the bactria with virus</Mcqw>
    <Mcqw>4. Making DNA and protein radioactive</Mcqw>
  </Mcq>
