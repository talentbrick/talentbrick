---
sidebar_position: 2
title: MCQ Questions Sexual Reproduction in Flowering Plants Biology Chapter 2 Class 12
description: Sexual Reproduction in Flowering Plants Chapter 2 Biology Class 12, Boost your performance and score great marks in NEET and Class 11 Exam by practicing these MCQs.
image: https://icdn.talentbrick.com/mcq/class-12-bio-chapter-2.png
sidebar_label: Chapter 2
---

import Mcq, {Mcqinfo, Mcqw, Mcqc, Mcqans} from "@site/src/components/Mcq";
import {Adsense} from '@ctrl/react-adsense';

MCQ Quiz Reproduction in Flowering Plants Class – 12th MCQ Questions for NEET |  School Exams | Class 12 Reproduction in Plants, Practice Questions.Prepare these important Questions of Reproduction in Plants, These are Latest questions to expect in NEET | School Exams.

<Mcqinfo/>
<Adsense
  client="ca-pub-1520766169486436"
  slot="1911400781"
  style={{ display: 'block' }}
  layout="in-article"
  format="fluid"
/>

### 1. Find the incorrect statement:
  <Mcq>
    <Mcqw>1. Seeds of large no. of species live for several years.</Mcqw>
    <Mcqc>2. Oldest seed is of date palm 10,000 years of dormancy.</Mcqc>
    <Mcqw>3. Fleshy fruit include guava.</Mcqw>
    <Mcqw>4. As seeds matures, water content is reduced by 10-15%</Mcqw>
  </Mcq>

### 2. Majority of plants produce:
  <Mcq>
    <Mcqw>1. Parthenocarpic Fruit</Mcqw>
    <Mcqw>2. Unisexual Flowers</Mcqw>
    <Mcqw>3. False Fruit</Mcqw>
    <Mcqc>4. Bisexual Flowers</Mcqc>
  </Mcq>

### 3. State True or False:
a. Only Sexual mode of reproduction is present in most of animals.  
b. Plant, fungi, animals differ greatly in External Morphology, internal structure, physiology but have similar pattern of sexual reproduction.  
c. In animals, juvenile phase is followed by change in physiology only.
  <Mcq>
    <Mcqw>1. FTF</Mcqw>
    <Mcqw>2. TTT</Mcqw>
    <Mcqc>3. TTF</Mcqc>
    <Mcqw>4. FFT</Mcqw>
  </Mcq>

### 4. Most crucial and Most vital event of sexual reproduction are:
  <Mcq>
    <Mcqw>1. Fertilisation</Mcqw>
    <Mcqc>2. Syngamy</Mcqc>
    <Mcqw>3. Gamete Transfer</Mcqw>
    <Mcqw>4. Copulation</Mcqw>
  </Mcq>

### 5. How many of following are Common pollinating agent?
  <Mcq>
<table><tbody><tr><td>Bees, Gecko, Rodents, Sun Birds, Moths, ants, flies, wasps.</td></tr></tbody></table>
    <Mcqw>a. 5</Mcqw>
    <Mcqw>b. 8</Mcqw>
    <Mcqc>c. 6</Mcqc>
    <Mcqw>d. 7</Mcqw>
  </Mcq>

### 6. Plants use __ biotic and ___ abiotic agents.
  <Mcq>
    <Mcqc>a. 1,2</Mcqc>
    <Mcqw>b. 2,1</Mcqw>
    <Mcqw>c. 1,1</Mcqw>
    <Mcqw>d. 2,2</Mcqw>
  </Mcq>

### 7. Choose the Odd one out with respect to reward for pollination:
  <Mcq>
    <Mcqw>1. Safe Place</Mcqw>
    <Mcqc>2. Fragrance</Mcqc>
    <Mcqw>3. Nectar</Mcqw>
    <Mcqw>4. Pollen Grain</Mcqw>
  </Mcq>

### 8. Algae and Fungi shift to sexual method of reproduction ___ the onset of adverse condition.
  <Mcq>
    <Mcqc>1. Just before</Mcqc>
    <Mcqw>2. Just after</Mcqw>
    <Mcqw>3. After reproducing asexual once.</Mcqw>
    <Mcqw>4. May be (a) &amp; (b)</Mcqw>
  </Mcq>

### 9. Flowers are object of:
  <Mcq>
    <Mcqw>1. Ornamental</Mcqw>
    <Mcqw>2. Religious</Mcqw>
    <Mcqw>3. Social</Mcqw>
    <Mcqc>4. All of these</Mcqc>
  </Mcq>

### 10. In how many among following male gamete is Transported through water?
  <Mcq>
<table><tbody><tr><td>Vallisneria, Zostera, Maize, Marchantia, Chara, Pinus, Hydrilla.</td></tr></tbody></table>
    <Mcqw>a. 5</Mcqw>
    <Mcqw>b. 3</Mcqw>
    <Mcqc>c. 2</Mcqc>
    <Mcqw>d. 4</Mcqw>
  </Mcq>

### 11. Radius of Pollen grain is about:
  <Mcq>
    <Mcqc>a. 12.5-25u</Mcqc>
    <Mcqw>b. 25-50u</Mcqw>
    <Mcqw>c. 70u</Mcqw>
    <Mcqw>d. 100u</Mcqw>
  </Mcq>

### 12. Identify the Incorrect statement:
  <Mcq>
    <Mcqw>1. Vegetative reproduction is type of asexual reproduction.</Mcqw>
    <Mcqw>2. Fungi and Algae reproduce through special asexual reproductive structures.</Mcqw>
    <Mcqw>3. In yeast, the division is unequal</Mcqw>
    <Mcqc>4. Water Hyacinth was introduced in India because of its flowers &amp; fruits.</Mcqc>
  </Mcq>

### 13. Which of the following statements are correct?
I : Much before actual flower is seen, the decision that plant is going to flower has taken place.  
II : Special cellular thickening in synergids help guiding pollen tube.
  <Mcq>
    <Mcqw>a. I only</Mcqw>
    <Mcqc>b. I &amp; II Both</Mcqc>
    <Mcqw>c. II only</Mcqw>
    <Mcqw>d. None</Mcqw>
  </Mcq>

### 14. State True or False (T/F):
- End products of sexual reproduction are fruits and seeds.
- In Angiosperms, there’s indirect pollination.
- Pollination in Yuca is brought by moth larvae.


  <Mcq>
    <Mcqc>1. TTF</Mcqc>
    <Mcqw>2. FFF</Mcqw>
    <Mcqw>3. FTT</Mcqw>
    <Mcqw>4. TTT</Mcqw>
  </Mcq>

### 15. Tassels in corn cob are:
  <Mcq>
    <Mcqw>1. Style</Mcqw>
    <Mcqw>2. Stigma</Mcqw>
    <Mcqw>3. Calyx</Mcqw>
    <Mcqc>4. Both a &amp; b</Mcqc>
  </Mcq>