---
title: Drunk Without Drinking?
authors: priyanshu
description: Can you think of the condition when you didn't drink the alcohol and got intoxicated?
slug: drunk-without-drinking
tags: [alcohol, toxication, science, drinking, disease, rare, biology]
image: https://icdn.talentbrick.com/main/second_proj.png
---

import GoogleAds from "@site/src/components/GoogleAds";

<GoogleAds slot="1911400781" />

# Auto-Brewery Syndrome

Have you ever accused your friend of drinking, seeing their condition, and lab tests and your friend do not admit it? There is a few percent chance that he or she might be true. There is a syndrome called Auto-Brewery Syndrome.  
In this syndrome, ethanol is literally produced inside your body, either your urinary tract, oral cavity, or any other part of the gastrointestinal tract. As to some of you, it might seem a boon, as the internal factory is set for alcohol, but it has serious consequences.

<!--truncate-->
![](https://icdn.talentbrick.com/main/second_proj.png)
It is caused by certain rare types of fungi or bacteria say species of _Klebsiella_ or _Sacchromyces_. With the intake of a high carbohydrate diet, ethanol generated may go as high as 400 mg/dl, which can cause symptoms of alcohol intoxication such as:
- Vomiting
- Belching
- Dizziness
- Ulceration of the GI tract
- Mood changes

Treatment for this condition includes dietary changes involving a high protein and low carbohydrate diet to avoid fermentation.
Also, the use of certain antibiotics for specific microbes can be administered to manage the condition after diagnosis by endoscopy.  
Reading about this syndrome may seem exciting but this is undoubtedly a pathetic condition, you can see a case in web series named Good Doctor and also read an article on [NCBI](https://www.ncbi.nlm.nih.gov/books/NBK513346/) to know more about this.