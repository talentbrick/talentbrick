---
title: Vampire Hormone and Sleep
hide_table_of_contents: true
authors: priyanshu
description: Did you ever visited a foreign country and had some issues with sleep, or after returning to your home you got a sleepy very early, and chaotic schedule? If yes, this is the article for you.
slug: vampire-sleep
tags: [scientific, sleep, jet lag, analogy, biology, medicine, travel]
image: https://icdn.talentbrick.com/main/vampire-hormone.png
---

import GoogleAds from "@site/src/components/GoogleAds";


**Melatonin** has got the name vampire hormone, not because it is sinister but simply because is released at night. Melatonin is the chemical through the suprachiasmatic nucleus (a part of the brain which receives the light input) and communicates to the body.

<!--truncate-->

<img src="https://icdn.talentbrick.com/main/vampire-article.png" style={{maxHeight: '370px'}} />

<GoogleAds slot="1911400781" />

Each of us got our intrinsic rhthym called the circadian rhythm. To know more about sleep and vampire hormone, let's get to know about circadian rhythm.  
Actually, our circadian rhythm is little more than 24 hours (about 15-20 min more to be exact). What this actually means is, if you are put in a very dark cave you will experience sleep/wake, and temperature alternation at regular intervals of time little more than 24h. It is amazing to know that **sunlight acts like a manipulating finger on the side dial of an imprecise watch**( here refers to our circadian rhythm). This means sunlight resets your clock every 24h, but it is not necessary to maintain the rhythm, that's why blinds also have the circadian rhythm. The body just uses sunlight as its most trusted and repeating signal that we have in our environment. Sun has always risen in the morning and set in the evening.

Now back to our vampire hormone, this hormone level rises at night and gives the message to the body that it's time for sleep, and says "it's dark, it's dark".And then another part of the brain generates sleep. It is important to note that melatonin just gives the message of sleep and does not participate in sleep generation. It's like melatonin is train official, waving the flag for the train(sleep) to depart but do not actually run the train. So melatonin is not powerful sleeping aid for non-jet-lagged individuals.

## What is Jet lag?
The invention of Jet engines made us move faster than our biological clock through time zones. To begin the discussion about jet lag, let's consider certain facts. Sydney (eastwards) time runs 5.5 hours ahead of India and London(westwards) time runs 5.5 hours behind of India. This means if in India it is 17:30 then in Sydney it would be 23:00 and in London, it would be 12:00.

Suppose you are traveling to Sydney for a conference of about 6 days. Now you would be attending the meeting at 8:30 am in the morning, but your clock knows that in India it is sleeping time! that is it is 3 am for the biological clock and you are sleepy and drowsy, as your melatonin levels are high. Now when it is sleeping time there in Sydney that is at 11 pm, for your clock it's time for tea in India(5:30 pm), as melatonin is not enough. You will now face difficulty in sleeping.  
However your clock adapts, but it's slow it adapts for 1 hour per day. So after about 6 days your clock has adapted and you are ready to sleep soundly, there's **bad news**. You have to return to India and again your poor brain needs to adjust to the Indian time zone. This is what is called jet lag.

In the above example, your jet lag can be cured by taking melatonin pills during the night in Syndey which would increase the melatonin and help aid sleep.  
If you travel to London, 8:30 am in London would correspond to 14:00 am in India, which is easier to be awake here. Also at night, 10:30 pm in London corresponds to 4 am morning in India.

<GoogleAds slot="1911400781" />

**So, it is harder to acclimatize to time zones in the east as compared to the west.**  
This is because it is easier to stay up later than to fall asleep and our biological clock is little more than 24 h, when you travel westward, the "day" is longer for you, and a little easier to stretch the day than to shrink it. Eastward travel involves a "day" shorter than 24h which goes against your internal clock and is rather harder to adapt.  
So traveling to the west is better for Indians, but you gonna face issues while returning home!
