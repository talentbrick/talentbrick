---
title: "Brain: Thinking Machine"
# hide_table_of_contents: true
authors: khushboo
description: This article revolves around our core engine which is the brain. Our brain says much more than we listen. For listening more we need to realize more than what this article is about realizing and evoking all that u already know is the prime target. Through realization, we will do an amazing journey where we would embrace our brain much more than any other materialistic thing.
slug: thinking-machine
tags: [scientific, brain, eating, analogy, biology, medicine, thinking]
image: https://icdn.talentbrick.com/main/thinking-machine.png
---

import GoogleAds from "@site/src/components/GoogleAds";

![](https://icdn.talentbrick.com/main/thinking-machine.png)

<!--truncate-->

We all are very aware of our Brainwork 24/7 but what we are doing for it? Our Brain deserves much more than it. Maybe many of us could get acknowledged about it if we could see our brain out of that opaque skull box. We all are very eager to make our skin beautiful whenever it gets damaged whyn't our brain? We neglect the most important part of our body. Whenever our Brain screams out in discomfort, we react to it in less than optimal ways: whenever it hurts just to take a pill, when it's sleepy as it requires pseudo hibernation, we soak it in caffeine. When it can't work according to the requirement as it lacks the necessary information, we just don't admit this fact but rather consider it to be inherently weak. 

<GoogleAds slot="1911400781" />

What we need to do right now is to embrace our Brain as much as we can. Just fall in love with this powerful weapon. Don't wait for background music to add another to the day so-called as World's Brain appreciation day. You and your brain stand at the end of a long line of Brains that lived and thought across many thousands of years. Many years ago,*Homo erectus* crafted stone tolls and tamed fire. Everything accomplished in our civilization shares a direct connection with the early achievements of human brains. Thus our brain is equivalent to a civilization. Thanks to the brains that came before you, you belong to a very exclusive club today.  **Its the reason you are able to look up at the night sky and have some realistic idea of what you see and where you are it's the reason you are something very special on this planet**.  
Now since the brain is a working engine it needs constant fuel so to feed well your body is of utmost importance. Many kinds of research have shown that poor nutrition detoreates your brain health. Unfortunately, we lose an immeasurable amount of intellectual potential every moment of every day because poor people, children, and particulars don't get enough good food. If you are dehydrated, calorie deficient, or protein deficient, your brain will struggle. It will not think, create, solve problems, or be as alert as it can be. It will be more prone to bad moods. All this matters to the excellent skeptic to anyone who thinks well and thinks often. Don't just really about a second piece of cheesecake having eventual repercussions on your thighs or butt, know that poor food choices show up in your brain too.  

There are many compelling scientific studies about a specific food that seem to support both short-term and long-term brain health. However, it is accepted to be more productive for most people to start by adopting an overall philosophy of eating well for general body fitness and brain health rather than chasing behind every new study that hints at the discovery of fresh powerful wonder food. Your habit and pattern most of the time needs to be smart eating like, if you like meat, cut down on red meat, if you like snacks, try to eat more nuts than candies. Blueberries and blackberries appear to be good for the brain. 

Eating well is not enough for the brain, exercise must accompany it. Exercise boosts brain performance for all ages, from the very old to the very young. The brain benefits from physical activity because of increased blood flow. It simply means if you are spending all of your waking hours sitting on couches, car seats, and office chairs, then your brain is not functioning at optimal levels. The solution is clear you need to be active and you need to sweat. Inactive and sweatless is a state better left to die. According to the report, there is now enough evidence that physical activity can help improve academic achievement and classroom behavior. If you want to be a good skeptic with a sharp mind, then don't just sit there, get moving. We evolved to be upright and active not planted in chairs 12 hours a day.   
Ending this, I would like to conclude this with these lines;

> Let's introspect today to Know our core  
Instead of being lethargic, let's think more  
Exercise, sleep, and food are our new friends   
And together we change these gloomy trends...    

Reference: [Think: Why You Should Question Everything](https://amzn.to/3KGyLst) by Guy P. Harrison 
