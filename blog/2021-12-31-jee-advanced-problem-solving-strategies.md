---
title: Jee Advanced Subject Wise Problem Solving Strategies
authors: owais
tags: [strategy, tips, subject-wise, tricks, JEE]
slug: jee-advanced-problem-solving-strategies
image: https://icdn.talentbrick.com/main/article-owais.png
---

import GoogleAds from "@site/src/components/GoogleAds";


> Stay with them and the concept will stay with you.

Hi there! JEE Adv. The examination is about solving a good/excellent problem related to a concept within a given time limit. Cracking JEE ADV requires excellent problem-solving skills more than anything else. So today I want to tell you about some of them that can help you crack problems easily apart from books that you will need.

<!--truncate-->

<GoogleAds slot="1911400781" />

## Physics
Physics is my favorite subject. I love doing physics, solving problems, and analyzing various real-life situations with the help of the equation written in notes.  
Understanding the concepts in Physics is very important in strengthening your foundation and as you proceed, problem-solving especially in mighty Exams like JEE Adv. **For studying and understanding the concepts I believe [H.C Verma](https://amzn.to/3FYg3aB) is the Bible.** I strongly recommend reading and re-reading it from time to time for a better understanding of physics. Also do solve its problems, Although most of them might seem simple as compared to what is asked in JEE Adv. but they are worth giving a try. Once your concepts seem strong enough in a particular chapter, go on to solve your coaching modules and if you don’t go to coaching, I suggest doing [D.C Pandey](https://amzn.to/3EHz5QZ) for further problems related to that concept or chapter. [D.C Pandey](https://amzn.to/3EHz5QZ) or any other reliable book for that matter has different levels of problems and it is important to go through easy problems as is to go through the toughest even though the former does not appear in your main Examination.

This is for a reason. I believe that a so-called tough problem is nothing but a combination of various so-called simple problems, and it is these simple ones that carry a conceptual understanding in them. So do the simple ones patiently, in fact, try to frame more simple problems from a simple problem. Stay with them and the concept will stay with you.

![Physics Diagram](https://icdn.talentbrick.com/main/phy-diagram.png)

Imagine the regular Inclined plane problem as shown above. The wedge is fixed and the surfaces are smooth. Given their masses as m and M solve the acceleration of m. Now go on to think about the improvisation in the same problem and what assumptions distance it from a real-life system., now imagine that the upper surface of the wedge is not smooth. Then you might go one to take the surface below wedge as rough. At last, assume that wedge ain’t fixed. Once you have got the whole thing you can play with it, like then think about the system in an elevator or a pulley binding the mass m with other mass, and so on. A simple problem led you into a complex problem through simple changes made by you. strange, isn't it?🙂  
Once your concepts are strong enough, I suggest [I.E Irodov](https://www.amazon.in/Problems-GENERAL-PHYSICS-I-Irodov/dp/9351762564), it is a very good book for gaining confidence to solve tough problems within the given time and also tests your concepts rigorously. Especially the mechanics and Electromagnetism(up to Electromagnetic Induction) part must be done patiently and with clear visualization of every problem.  
Moreover many problems, in JEE Adv. have the same format of solution as do the problems in this book have. I don't suggest doing Modern physics, optics, or Waves from this book. Modern physics as asked in the Exam is pretty simple as doesn't need any special preparation apart from NCERT, [H.C Verma](https://amzn.to/3FYg3aB), and your Coaching assignment.

<GoogleAds slot="1911400781" />

Your Confidence is a very decisive factor in an Exam like JEE Adv. and you should always try to gain confidence over a topic so that it stays with you for a long time. Confidence comes from a clear understanding of a problem which comes from extensive problem-solving. I also did [S.S Krotov](https://www.amazon.in/Science-Everyone-Aptitude-Problem-Physics/dp/9350941449) which is more about olympiads but you can try it if you are a physics fan.

## Chemistry 
Chemistry is a very interesting subject. Let’s talk about its subparts separately:

### Physical Chemistry
Physical chemistry is mostly based on formulae except for the chapter on surface chemistry. Reading NCERT with full concentration is a must for excellence in chemistry. So do read it, especially the thermodynamics portion with dedication. Then I suggest going with [N. Awasthi](https://amzn.to/3HozzNx) JEE for practice, it contains problems of all levels for your all-round practice. For practicing calculative problems you can also find [R.C Mukerjee](https://amzn.to/3eDFgeo) useful, however, I did not complete it but did its most important and relevant ones. Formulae must be revised in every chapter prior to a test, making a sheet for each chapter will be ideal.

### Organic Chemistry
Organic is a very tricky part of JEE syllabus, most students have no
prior knowledge in this area. GOC is the most important chapter to me, so understand it
very well from the NCERT and books are given below and then move on to other ones. I recommend [M.S Chouhan](https://amzn.to/32RE18o) and your coaching Assignment for problems. Initially, for concept building, you can study [L.G Wade](https://amzn.to/3zlPKsk) (concepts are explained very patiently plus has a myriad number of problems and case studies) and once you have completed the chapter and done all its assignments give it a read from [Peter Sykes](https://amzn.to/3pF0hvp), it will make the already strong concepts
permanent and then you can solve all problems of that chapter, no matter what the level.
Make notes of mechanisms only after you get them. I believe only self notes work in Organic Chemistry. Practice Mechanisms by yourself multiple times. Ask questions about them. Keep in mind the conditions of the reaction and understand how it affects the products
obtained. A good problem solver always thinks about all the conditions to play safe.

### Inorganic Chemistry
Believe me, it isn't boring!😎 . Studying NCERT, with full honesty is the key to mastery in inorganic. Read all the chapters, regularly and with focus. Don't try to mug up things. Try to get the concept, try to relate the concepts, their examples, and their exceptions. The books I read were [INORGANIC CHEMISTRY BY J.D Lee Foreign Edition](https://amzn.to/3eGqPpE), but only the relevant parts of it, [Vogel’s qualitative analysis](https://amzn.to/3pDv4J0), and of course NCERT. For problems, I referred to [V.K Jaiswal JEE](https://amzn.to/3pFGVWU) and [NCERT EXEMPLAR](https://amzn.to/3qx4KiW) apart from the coaching modules.

## Mathematics
Maths is all about practice. The more you practice, the more efficient and fluent you are in problem-solving. But first, you need to make your concepts clear especially in differential and integral calculus, for which I think [I.A Maron problem in the calculus of one variable](https://www.amazon.in/Problems-CALCULUS-ONE-VARIABLE-Maron/dp/9351762599) is the best book. For coordinate geometry, I think [S.L looney](https://amzn.to/3EIkTXN) is the Ideal book.

Maths requires a lot of patience, hence keep trying until you don't land on the solution yourselves. don't look for solutions in problem books unless you are just months away from the JEE Exam when time is very precious, but ask your teacher and learn it. A good problem solver is also a patient thinker. Try to solve a problem in multiple ways and then stick to one which is most efficient for your final exam. Our teacher always told us to make a sheet for every chapter where we take note of types of problems framed in exams and general methods of doing that problem. I did and it worked out very much for me in saving time.  

For more problems, you can solve your coaching assignment, PYQs, and various DPPs available online.  
Finally, try to apply these techniques and you will definitely see an increase in your problem-solving ability.
JEE is not an Exam but an experience that transforms one’s self. The Journey makes you bold, adventurous and explorative as never before. It made me believe in my thoughts and my way of doing things.
Good Luck! With problems.🙂