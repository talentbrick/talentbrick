---
title: Brain Waves and Cricket Match
authors: priyanshu
description: Can you imagine any way to link the famous cricket match between India and Pakistan to brain waves? Here is the fantastic analogy between the two.
slug: brain-cricket
tags: [scientific, sleep, cricket, analogy, biology, medicine]
image: https://icdn.talentbrick.com/main/brain-cricket.jpg
---

import GoogleAds from "@site/src/components/GoogleAds";

<GoogleAds slot="1911400781" />

One might expect to have highly synchronous brain waves while wakefulness, matching the ordered pattern of your (mostly) logical thought during waking consciousness. But this is not true as seen in the wave pattern. This chaotic waveform is explained by the different functioning of the different brain parts at different times in different ways.
<!--truncate-->
Think it of as a cricket match of **INDIA vs PAKISTAN**, the fans present in the stadium represents the brain cells, spread in different parts of the stadium like different parts of the brain. And suppose the electrode placed for recording is a microphone in the middle of the stadium.  
Before the game starts there is chaotic chattering among the people, different people talk about different things at different times as a result voice heard is unclear and chaotic.

![](https://icdn.talentbrick.com/main/waves.jpg)

Now things change after the toss, as a probably famous player comes to play(say, Virat Kohli) now the crowd which was chattering chaotic noises has switched to a unified chant (say, cheering for Team INDIA). All the shout at once, creating a spike in brain activity and then a silence for while. This will produce a rhythmic pattern in the microphone.  
This represents the deeper phase of NREM sleep where the waves are synchronized. This slow-wave NREM sleep has a great role in saving memories.  
Think of wakefulness as a fast muscular sprinter (fast-frequency waves) which can carry loads of information but gets tired easily, now think of a slow long-distance runner, slow wave frequency is one in a deep phase of NREM and can carry information to long distances.  
This slow-wave transfers the memory packets short-term storage to the long-term regions in the brain.