---
title: How enjoy reading First Year MBBS
# hide_table_of_contents: true
authors: priyanshu
description: Here are the 1st yearbooks to refer to. I have tried to cover all the tastes of the books. For exams, for extra knowledge, for fun.
slug: reading-1st-mbbs
tags: [scientific, physiology, anatomy, biochemistry, MBBS, medicine, books]
image: https://icdn.talentbrick.com/mbbs-how-to.png
---

import GoogleAds from "@site/src/components/GoogleAds";

<!--truncate-->

![](https://icdn.talentbrick.com/mbbs-how-to.png)
In the first year of the medical courses, there are a lot of opportunities for one to explore. Here I have written about my exploration journey about all books and resources for three subjects. Some of the books I explored myself and some are suggested by my friends and senior. **We never read all of them or to be honest, any single book to its fullest.**
> There are books for each taste, for passing exams, for enjoying a subject, for loving MBBS, etc.

## Anatomy

> The Whole purpose of this project is to make you imagine the structure of the human body with ease, for the professor would spend hours of lectures, Dissection hall, and models for you to imagine and think.

For imagination purposes attending DH contributes to a great factor in learning especially in topics like upper & lower limbs, abdomen, and thorax. Some cases like neuroanatomy are very troublesome to be imagined and analyzed in DH. Other than DH, you can use apps like **AnatomyLearning**( cracked version readily available) or **Elsevier app**( it is the best app, but hard to get for free).

If you can manage your time then the book for you is [Gray's Anatomy(Student edition)](https://amzn.to/3OFAHBi). It is a complete book with great diagrams and easy-to-understand text. This is the book if you want feel for the subject. For **exam purposes,** books like [BDC](https://amzn.to/3EE17il) and [Vishram Singh](https://amzn.to/3V7UhIO) would be more than sufficient. Among these two, [Vishram Singh](https://amzn.to/3V7UhIO) is better and easy to read.

#### For applied aspect:
1. Clinical Cases at the end of the [Gray's anatomy](https://amzn.to/3OFAHBi) are jackpots, they link all of your anatomy with biochemistry and physiology to some extent.
2. [Moore's Clinical Anatomy](https://amzn.to/3ik0zX9) blue boxes are also amazing along with [Vishram Singh](https://amzn.to/3V7UhIO) blue boxes which sometimes have clinical you will not find anywhere else 😁.
3. You can also refer to [First-Aid USMLE book](https://amzn.to/3VdWKkU).

For imaginative purposes; You can refer to any of the atlas you find suitable for yourself. My suggestion: Netter's atlas is too complex to be analyzed at the beginning, instead try Gray's atlas comes with simple pictures. If you want your Dissection Hall in form of an atlas then you can try [Abrahams' and McMinn's Clinical Atlas of Human Anatomy](https://amzn.to/3u5nkAW).

### Neuroanatomy
For exam purposes, the best book is again [Vishram singh](https://amzn.to/3VtXEd0). For having fun reading Neuroanatomy and if you want to cover most of your CNS of physiology [Snell Clinical Neuroanatomy](https://amzn.to/3u6cr1C) is the best book. Try solving back questions of this book, especially for the chapter Blood supply of Brain🤩.

### Histology
[Ross](https://amzn.to/3F8lUMw) is a great book for histology and has got amazing information and clinical correlates, but very long to be covered just before exams.See [Defiore](https://amzn.to/3U7KAc1) for diagram and [IB Singh](https://amzn.to/3UcSGAi) for exams.

### Embryology
The Golden book for embryology as everyone says is [Langman](https://amzn.to/3VtLBfF). It is very hard, to begin with this book. Here's a tip
:::tip

Start with the summary first and then go back to see the diagrams of the Langman, in this way you could easily develop interest and save time.

:::

### Radiology
Not much is required at this time. Gray's student edition diagram is sufficient to know and analyze and you can also find some good stuff on google.

# Biochemistry 
This is my favorite subject. It is not just about learning enzymes if try to understand the chemistry part of Biochemistry.  
If you are just aiming for exams go for [Lipincott](https://amzn.to/3gDbIBZ). Many here at my college refer to [SK Gupta](https://amzn.to/3gDbNWj), which is a derived book from Harper. But Harper and [Lipincott](https://amzn.to/3gDbIBZ) will not make Biochemistry boring at least.  
Now if you want to enjoy reading biochemistry here we go:
1. [Lehninger](https://amzn.to/3gzcjox): one the best books for knowing the organic mechanisms. Must read for the kinetics of enzymes🤩.
2. [Stryer](https://amzn.to/3GPSaoA): This book gives you an evolutionary touch of biochemistry along with some clinical.
3. [Bruce alberts](https://amzn.to/3EFQQCk): Amazing book with great diagrams, evolutionary links, and explanation. Try reading cancer from this book.

For the practical part: the AIIMS biochemistry Lab manual is amazing and got a lot of clinical stuff.  
For **Immunology**, if you want to enjoy reading then [IMMUNE](https://amzn.to/3EKGyAE) book by Kurzgesagt is just amazing. This is a book with stories and funny analogies which makes studying a lot easier.  
Also, there are mind-blowing videos on [Nature channel](https://youtu.be/5AXApBbj1ps) on Youtube regarding immunology.  
Now for theory/exam part you can refer )[Levinson](https://amzn.to/3APzTUM)(immunology section), [Immunology for MBBS](https://amzn.to/3u2Dxqm).

# Physiology
> Physiology is the stepchild of medicine. That is why Cinderella often turns out queen.

This subject will decide your further interest in MBBS. All clinical cases and pharmacology have their roots in physiology. Don't study this subject just for exams, you will regret it in the end. This subject answers the question **"Why"** for each curiosity you have.  
The most preferred book is [Ganong](https://amzn.to/3ALc800), which has got good stuff but to unlock fun reading this book you need some story at the back of your mind.

To begin with, physiology try referring [Guyton](https://amzn.to/3ARHOky) (especially CNS) as a supplement, then go for [Ganong](https://amzn.to/3ALc800).  
There are various books that are awesome for understanding purposes:
1. [Vander's Human physiology](https://amzn.to/3Ur24Ax): This is short, crisp, and easy to read and understand and in a few lines gives big concepts. Must try Respiratory physiology.
2. [Sherwood](https://amzn.to/3GUbqkW): This books also got great diagrams, flowcharts and mind-blowing questions at the end of the chapter.Try reading Nerve-muscle physiology from here. Other chapters are also good.
3. [Silvethorn](https://amzn.to/3EE3eCN): It has got very elaborate diagrams and flowcharts, use it as a physiology atlas 😁.
4. [Bijlani](https://amzn.to/3VsU96u): It is a book by an Indian professor, very easy to understand language and with Indian examples. (Remember HC Verma's book?)


[Berne and Levy](https://amzn.to/3XzJDMG) is a nice book, if you like it you can follow it for every chapter, especially since the renal part is good.  
[Boron Medical physiology](https://amzn.to/3EzoPw4): It is a very detailed book, that will explain every aspect of physiology, and involve you in the calculation. Just use this book as a reference.  
Try calculating parts of physiology from this book like in Ventilation-Perfusion topic and Acid-Base balance.

### Practical
The clinical examination and exam part refers to the book which the college suggests to you.  
For knowing more about the clinical aspect, [Hutchinson](https://amzn.to/3EEQ1JL) has got good stuff, which helps you at several places like viva, quizzes, and clinics.

Try seeing some sort of medical drama also, if you can manage 🙃. This would make you "feel" about your MBBS career and you would explore more. I watched the Good Doctor series.

## Conclusion
It is your choice and taste, what you prefer. Above are my suggestion that you should try to have fun studying in the first year.
