---
title: How to prepare for Biology Olympiad
authors: dhiren
slug: inbo-air-1-preparation-strategy
tags: [experience, story, time management, topper, exclusive, INBO]
hide_table_of_contents: true
image: https://icdn.talentbrick.com/main/air-1-preparation-blog.png
---

import GoogleAds from "@site/src/components/GoogleAds";

> Don’t lose sight of your final goal no matter what happens as things don’t always go the way you need.

The first thing required for any kind of progress is pure and honest hard work. Nothing replaces this. 

<!--truncate-->

<GoogleAds slot="1911400781" />

The destination would seem easier to achieve if you enjoy the journey. (For example, if you want to do good in a biology olympiad then things would be easier for you if you enjoy knowing more and more about nature i.e. if you are curious about it) 

Second, I would advise you to keep your final goals pretty clear. It isn’t possible for your final goal to be something like - to ace some exam or do good in your mock tests or getting admission to a deemed university etc etc. If you ask me, these things seem pretty shallow. These are just short term goals or checkpoints. There can be many paths reaching your final goal each having a different set of checkpoints. Your final goal ought to include how you truly wish yourself to be , for example – becoming a good respected Doctor who keeps his family happy and who is trying his best to save as many smiles as he/she can.

> Seems like a life to really look up to right? 

Don’t lose sight of your final goal no matter what happens as things don’t always go the way you need. Stay motivated as if some of the checkpoints are missed by you (Example-You failed in an exam) you shouldn’t get demotivated and instead think of grabbing an alternate checkpoint by keeping your final goal in mind. Make sure you grab the last laugh, always seek a way out, an alternate path to reach your goal. 

<GoogleAds slot="1911400781" />

When it comes to academic preparation, first of all I would like to share the general rule which works for any exam- PREVIOUS YEAR PAPERS. Solving previous year papers is the most important step as it gives you the best estimate of the paper you are about to face. Always try to keep your surrounding conditions as close as possible to the real exam conditions.( Example-Solving paper while wearing a mask).

Also, make sure you analyse the paper well i.e. Try your best to look up for the concepts behind the questions that you couldn’t solve and in fact even for the questions you did right. Also look for as many related concepts as you can from standard books as it will help you improve your logical thinking. After properly analysing your paper, clearing all your doubts and covering a few chapters from some standard books, attempt the next paper. Make sure you analyse your previous paper well before attempting the next paper so that you improve with time. 

Let’s face the facts, biology is a vast subject. We can’t read and learn everything. But if you have read a lot of texts it will help your brain guess correctly i.e. You will be able to arrive at logics that are closer to reality, something that might actually be true. And if your logic is closer to what actually happens in nature then you would be able to solve questions easily as in olympiad most of the questions aren’t from something that you have already read or what you already know. Most of the questions require you to exercise your brain into thinking logically. So the key takeaway is if you read and learn more and more concepts your logical thinking will improve and you will be able to solve questions easily. 

Now let's discuss ways to prepare for the biology olympiad. 
## Books to prefer
**NSEB** – It requires a mix of biological knowledge and logical reasoning. It isn’t necessary to read lots of standard text to qualify for any of the stage of biology olympiad but if you read them it will surely help you a lot. According to your previous year paper analysis, you may refer to the apt books given in the book list. Ex – if you think you are weak in some particular discipline let’s say ethology, then refer to some of the ethology books given in the book list. If you don’t want to read a lot and just want the bare minimum overview [Campbell Biology](https://amzn.to/3m5uDVt) would suffice. It covers all of the disciplines in biology really nicely. It’s a really good book and a lot of people begin their biology journey with it. If you read [Campbell](https://amzn.to/3m5uDVt) and diligently think on all of its question and scientific skill exercises then you will be able to answer most of the questions of EVEN IBO . If you wish to dig more on certain topics or have some doubt or if you crave for more knowledge you may refer to some more specific book (Example – [Alcock](https://amzn.to/3m96SfA) if we talk about ethology) (Refer to book list) 

Though, let me clear one thing, reading standard texts isn’t NECESSARY, there are many people who ace Nseb just by feeding their curiosity from the Internet and referring to [Campbell](https://amzn.to/3m5uDVt) or some other detailed standard book only for their doubts or specific weak topics. If you can think logically you can definitely ace it. Standard texts are just for assisting you, clearing your doubts, to feed your curiosity, to help you think better i.e. closer to reality. I myself followed a lot of books, really A LOT. And so I can also assure you that gaining knowledge always helps and knowledge never goes to waste. So you shouldn’t stress yourself or force yourself to read standard texts nor should you completely refrain from reading them. Do Read them if you can, if you like to, if you are comfortable with it, suit yourself. 

Recommended books- [Campbell Biology](https://amzn.to/3m5uDVt) (or any other general biology book though Campbell is preferred)

**INBO** - It is quite a fun part as most of the questions are purely logical if you have a very good understanding of the basics of biology. In accordance with the general rule you need to solve and analyse all the previous INBO papers. For doing well in INBO, I would advise you to at least read as many chapters as you can from [Campbell](https://amzn.to/3m5uDVt) Biology or any other good general biology book (refer the book list). If you ask me, chances of clearing INBO and making it to OCSC without reading any of the standard texts are really slim so do try your best. Moreover, you will need to refer to more specific books for the topics you wish to dig more or for your residual doubts. Make sure you are able to clear as many doubts as you can. And also, focus on the “at least" part. [Campbell](https://amzn.to/3m5uDVt) (or any other general biology book) would be your bare minimum for INBO. If you have spare time left with you make sure you refer to the more topic specific books. Also if you read some topic from a more specific book then you may skip that part from Campbell.

Now let’s talk about the biggest challenge that INBO poses to us. It’s TIME. The questions are mostly logical but long, VERY LONG. If you have a very good reading speed then you will be able to tackle it. But if you face the issue of time management then you need to improve. To improve you need to follow the general rule. Solve previous year papers and note how much extra time it took for you to solve the whole thing and then try to solve the paper faster. It’s fine if you aren’t able to complete the paper in time, just make sure you improve with time, increase your speed little by little so that you can at least solve the next paper a bit faster. To improve my speed, I started to try my best to read faster not only while solving the paper. I tried to read everything faster for example my WhatsApp chats, books, newspaper, etc faster without compromising my quality of understanding things while reading. This slowly (about 2-4 weeks) helped me improve my speed heavily. Trust me it WORKS. Just make sure you don’t ever compromise your quality of understanding, that is , you need to improve but slowly . 

Also, Think of your own strategy. Some people tend to spend a lot of time on Section A (which awards relatively less points) and then aren’t able to give Section B the time it needs and end up making lots of silly mistakes or aren’t able to complete the paper. Some people try to not spend a long time on any single question. If a question seems lengthy - leave it and come back to it later if you get the time to. This strategy works fairly well for most students.

## Recommended Books
- [Campbell Biology](https://amzn.to/3m5uDVt) (or any other general biology book though [Campbell](https://amzn.to/3m5uDVt) is preferred) - Especially for ethology, ecology, photosynthesis- respiration, cladograms, Plant growth regulators and animal physiology unit- Animal Behaviour is really well written.

- [Life the science of biology (David Sadava)](https://amzn.to/3F1MmEW) - To me, it was just like a more detailed [Campbell](https://amzn.to/3m5uDVt) and is really an excellent book (Especially for Animal Physiology and ethology)- it too has a really good animal behaviour chapter.

### Following books aid a lot for INBO preparation-
- [Genetics - Robert J Brooker](https://amzn.to/3dXk5DC)
- Ecology - [Elements of Ecology](https://amzn.to/3sldJpV) (Smith)(Recommended) and JS Singh Ecology
- [Human Physiology - Sherwood](https://amzn.to/3q3BTCr)
- [Evolution - Douglas J Futuyma](https://amzn.to/3E2tt3n)
- For Plant Anatomy - Visit [Virtual Plant website](http://virtualplant.ru.ac.za/Main/Virtualintro.htm)

Specific books for plant biology, animal behaviour and practicals aren’t necessarily required for clearing INBO though obviously you should read them if you are interested. They are listed in the book lists given below.

## Refer Book list for a more detailed overview

**OCSC, PDC and IBO** - All three of them can be treated equally if you wish to be the part of the Indian team.

Theory questions of OCSC and PDC are mostly either exact previous year IBO questions or their modifications.
The training for Practicals is also entirely IBO inspired.

Now, you need to read. You really need to read as much as you can. IBO questions are heavily conceptual and many are solely based on data analysis. They are also EXTREMELY long (Both the Theory and Practical part).

To deal with the time problem, refer to the INBO section

Also, don’t forget the general rule. Solving Previous Year IBO Papers is the best way to prepare. Solve paper, analyse mistakes, read theory from textbooks, then attempt paper again and make sure you improve with time. Create your own strategy to study, a strategy tailored specifically for you, a strategy following which you would work most efficiently.

Also, OCSC,PDC and IBO are much more than just stages of a Biology olympiad. You get a chance to meet a huge lot of smart people from all over the world, it’s really a great opportunity only a few get to experience. Make sure you savour the fun, enjoy yourself during the journey, create lots of friends. Hurray :)

## For Practicals-
- Make sure you do all your school biology lab practicals diligently as they would help A LOT.
- Use of microscope, stem root cutting, slide identification, assaying etc are all extremely important and would help you do better in IBO.
- I recommend you to read the books given in the major book list for these stages.

## Major Book list
### General Biology- 
- [Campbell Biology](https://amzn.to/3m5uDVt)(Though you need not read campbell if you have already covered the topic from a more specific book like you may skip the evolution part if you have read Futuyma).
- [Life the Science of Biology](https://amzn.to/3F1MmEW).

<GoogleAds slot="1911400781" />

### Biochemistry-
[Lehninger](https://amzn.to/3GOLunu) - Michael and Cox 7th ed.

**Specific Recommendations:**
1. Complete Part III. Information pathways. Genes and chromosomes- Do read if you are not following any specific genetics book for molecular genetics.
2. Ch8- Nucleotides and nucleic acids 
3. Ch9- DNA-based information technologies (Work for cell biology)
4. Titration 
5. All the boxes are really awesome
6. Photosynthesis- Except Dark Reaction Part
7. Concept of uncouplers from Oxidative Phosphorylation chapter

### Cell Biology
[Molecular Biology of Cell - Bruce Alberts 6th edition](https://amzn.to/3IXdB5N)
I would specifically recommend the following -
1. Complete Part 3-Isolating Cells and Growing Them in Culture Chapter 8 (Analyzing Cells, Molecules, and Systems) and 9 (Visualising Cells).
Chapter 8 was especially very very helpful covers almost complete basic knowledge of biotechnology that would help us understand how things work. Though cell biology is almost always completely logical in IBO these chapters help understand the questions faster- Really Important.
2. Read complete Part 2 - Basic Genetic Mechanisms if you are not following any other specific book for it.

[Lehninger](https://amzn.to/3GOLunu)- Ch8 and Ch 9

### For Immune system,
- [Immune- Phillip Dettmer](https://amzn.to/3p67iVK)- It is an awesome book, one of my favourites as it is too easy to read and too easy to understand. Anyone who knows English can read and understand this book and this book covers enough concepts to tackle even any IBO Qs. You can use this book for leisure reading as well
- [Sherwood](https://amzn.to/3q3BTCr) would also work well for immune system.
- [Kuby’s Immunology(8th ed)](https://amzn.to/3p5miTF) - This book is strictly a reference book, use only and only for immune system related doubts. The Vaccines portion of this book is really nice and is worth reading(Ch 17- Vaccines part).

### Evolution
Evolution- [DJ Futuyma](https://amzn.to/3E2tt3n)(4th ed)
This is my favourite book. It’s extremely easy to read and understand and is very interesting. It's filled with fun concepts and you may read the whole book just for fun.
It helped me a lot for IBO, A LOT. Read the first fourteen chapters, chapter 16 and Human evolution from it. If you face any other topic in Evolution, do visit this book for it.
### Ecology
[Elements of Ecology](https://amzn.to/3sldJpV), 9th Global Edition by Smith, Robert Leo Smith, Thomas M.
It's truly useful for Ecology. It will be quite helpful for any concept in Ecology.
### Ethology
[Animal Behavior by John Alcock Dustin R Rubenstein](https://amzn.to/3m96SfA) 
### Genetics
Genetics- [Analysis and Principles- Robert J Brooker](https://amzn.to/3dXk5DC)
### Animal Physiology
[Sherwood - Human Physiology](https://amzn.to/3q3BTCr) From Cells to Systems.

You may refer to [Life the Science of Biology](https://amzn.to/3F1MmEW) for Animal Physio, it’s fairly helpful.

### Plant Physiology and Anatomy
- Visit [Virtual Plant website](http://virtualplant.ru.ac.za/Main/Virtualintro.htm) It is the best place to brush up your Plant Anatomy.

- [Raven’s Biology of Plants](https://amzn.to/3q7JrV8) Works well for Plant Physiology - Especially Chapter 22-30 are really important.

## Practicals
[Practical Skills in Biomolecular Sciences - Rob Reeds](https://amzn.to/33H4RAP)

For Animal Dissection (Only for OCSC and Higher stages)-
[Atlas of Animal Anatomy and Histology](https://amzn.to/32kld1G) by Péter Lőw, Kinga Molnár, György Kriska (Author)

The books listed above would more or less cover all you need.
But, if there is something you don’t find or if you don’t like any of the books given above then below I am listing an EXHAUSTIVE book list.

<GoogleAds slot="1911400781" />

## Book List
> What I mean by Strictly reference Book?

Books should be referred only for residual doubts. Reading complete chapters from these books would be really tough as they are EXTREMELY Detailed. Though the book is good and if you have a doubt you aren’t able to clear you would surely get it solved by looking into this book.

## Genetics- 
- (Recommended) [Robert J Brooker](https://amzn.to/3dXk5DC) (Easy to read and. Simple for basic concepts)(I have read this one it’s good)- Population genetics is also brilliantly explained in it.
- [Genetics - Peter Russell](https://amzn.to/3p0tAbo), Many people like it I personally found it complex to read though it is slightly more detailed.
- [Molecular biology of Gene – Watson](https://amzn.to/30wz0S1)
- [Principles of Genetics- Symmonds and Snustad](https://amzn.to/3q09Hkb) (Quite detailed but interesting)
- [Lewin’s Genes XII by Jocelyn E. Krebs, Elliott S. Goldstein, Stephen T. Kilpatrick](https://amzn.to/3Jcyvyf) - EXTREMELY detailed. Strictly a reference book.
- (Recommended)[Lehninger (7th ed❤️)](https://amzn.to/3GOLunu) - (Yes! It works for Genetics too)
- [An Introduction to Genetic Analysis by Anthony J.F. Griffiths, Susan R. Wessler, Sean B. Carroll, John Doebley](https://amzn.to/3GYADaR)
- [Primer of Genetic Analysis A Problems Approach by James N. Thompson  Jr, Jenna J. Hellack, Gerald Braver, David S. Durica](https://amzn.to/3yFUOre) 
- Questions Practice - [Solving Problems in Genetics](https://amzn.to/3DTXNgA) (Richard V Kowles)

 
## Evolution
- (Recommended) [Evolution](https://amzn.to/3E2tt3n) by Douglas J. Futuyma
- [Evolution by Mark Ridley](https://amzn.to/3yvFc9n)
- [Evolution by Stephen Stearns & Rolf Hoekstra](https://amzn.to/3oXcsTZ)
- [Evolution: Making Sense of Life by Carl Zimmer & Douglas Emlen](https://amzn.to/30wzNlX)

## Animal Behaviour
- [An Introduction to Behavioural Ecology by Nick Davies, John Krebs](https://amzn.to/3mbGQIi)
- [Animal Behaviour by John Alcock](https://amzn.to/3mBwYIn) (Recommended)
- [An Introduction to Animal Behaviour by Aubrey Manning, Marian Stamp Dawkins](https://amzn.to/3GKGdNS)

## Physiology
- [Human physiology](https://amzn.to/3q3BTCr) from cells to systems by Lauralee Sherwood Christopher Ward (Recommended)
- [Vander’s Human Physiology](https://amzn.to/32k1nne)
- [Ganong](https://amzn.to/3p5Y7EC)
- [Guyton and Hall](https://amzn.to/3Fc3Dvk)
- [Animal Physiology by Hill, Wyse, and Anderson](https://amzn.to/3GS3ayH)
- [Medical Physiology Walter F Boron, Emile L Boulpaep](https://amzn.to/3Eek4pG)- Strictly a reference book 


## Cell Biology
- [Molecular Cell Biology by Harvey Lodish](https://amzn.to/3DY6xSP)
- [Molecular Biology of The Cell by Bruce Alberts](https://amzn.to/3IXdB5N) (Ch 8 and 9 recommended 6th ed)
- [Biochemistry by Lubert Stryer](https://amzn.to/3pZ3xAz)- Strictly Reference Book
- [Lehninger](https://amzn.to/3GOLunu)- Ch8 and Ch 9(Recommended)
- [Fundamental of Biochemistry Voet](https://amzn.to/3pftOf7)- Has a good Biotechnology part
- [Principles of Gene Manipulation and Genomic](https://amzn.to/329zBdh) - S.B. Primrose (STRICTLY REFERENCE)

## For Immune system

- [Immune, Phillip Dettmer](https://amzn.to/3Fa7p8q) - It is an awesome book, one of my favourites as it is too easy to read and too easy to understand. Anyone who knows English can read and understand this book and this book covers enough concepts to tackle even any IBO Questions. You can use this book for leisure reading as well (Recommended)
- [Sherwood](https://amzn.to/3q3BTCr) would also work well for immune system (Recommended)
- [Kuby’s Immunology](https://amzn.to/3GQVE7a) - Do refer vaccines (Chapter 17 last part)(Highly eye opening) Otherwise this is STRICTLY A Reference book only

## Ecology
- [Elements of Ecology](https://amzn.to/3sldJpV) (Smith) (Recommended)-Especially helpful for sun- shade plants👾👾
- [Ecology by William D Bowman](https://amzn.to/30G3GAs)
- [Ecology Concepts and Application](https://amzn.to/3F8oeAB)- Manuel C Molles
- [JS Singh](https://amzn.to/3GQEbvQ)- Ecology
JS Singh was highly helpful when it comes to learning about stuff like Various efficiencies and production, Data, graphs, succession, resistance etc. Well and easy to read.

## Biochemistry
- [Lehninger](https://amzn.to/3GOLunu) (Recommended)
- [Fundamental of Biochemistry Voet](https://amzn.to/3pftOf7)
- [Koolman](https://amzn.to/3sh3BhO)
- [Lippincott](https://amzn.to/3q4kdqr)

## General BIOLOGY
- [Campbell biology](https://amzn.to/3m5uDVt)(Recommended)
(Very very good for most of the topics)(Honestly do all the back exercise and scientific skills exercise)
- [Life the Science of Biology- David Sadava](https://amzn.to/3F1MmEW)(Recommended)(It may be called as Extended version of Campbell it is very good along with being HIGHLY detailed for all the topics)
- [Integrated Principles of Zoology](https://amzn.to/3p7epxi)- Can be referred for doubts related to the Animal kingdom
- Biology By [Peter Raven](https://amzn.to/3z9Bgvy)

<GoogleAds slot="1911400781" />

## Plant Physiology and Anatomy
- [THIS LINK](http://virtualplant.ru.ac.za/Main/Virtualintro.htm) IS EXTREMELY IMPORTANT and Highly RECOMMENDED
- [Taiz and Zeiger](https://amzn.to/3oZaKRV) (STRICTLY reference book nothing more)
- [Ravens Biology of Plants](https://amzn.to/3q7JrV8) (Recommended Ch 22-30- Covering Angiosperm Physiology)
- Stern’s Introductory Plant Anatomy

## Practicals
- [Principles and techniques of biochemistry and molecular biology by Keith Wilson and Walked](https://amzn.to/3ma5uZY)(Extremely detailed- Reference only)
- [Rob Reeds](https://amzn.to/3GIaIDW)- Practical skills in biomolecular sciences (Recommended)
- [Atlas of Animal Anatomy and Histology](https://amzn.to/3J2vYGw) by Péter Lőw, Kinga Molnár, György Kriska (auth.)- Really Important for OCSC and above(Recommended).
- http://virtualplant.ru.ac.za/Main/Virtualintro.htm (Recommended)

## Developmental Biology
- [Developmental Biology - Scott F Gilbert](https://amzn.to/3GUCMUR)- Strictly for Reference only.
## Online Resources-
- [MIT OpenCourseware](https://ocw.mit.edu) can be referred
## Leisure Reading
- [Immune- Phillip Dettmer](https://amzn.to/3p67iVK)
- [Selfish Gene- Richard Dawkins](https://amzn.to/3F362Ix)
- Evolution- [Douglas J Futuyma](https://amzn.to/3E2tt3n)- Though it is actually a textbook, it was just too interesting.

## Conclusion
In the end, I would like to say- Always Prepare for your goals while staying honest to yourself. It is said- When you lose your excuses, you find your results. 

Also, make sure you never forget the general rule- Attempt previous year papers and analyse them. Read concepts from reliable sources.

It is best to follow your passion and hobbies so don't completely leave them during your preparation. 
Best of luck for you journey, and may the odds be ever in your favour.



